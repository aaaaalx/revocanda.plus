<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

$showsCats = [];
$showsCats['video'] = get_option('field_feedback_show_category');
$showsCats['video'] = $showsCats['video'] == 1 ? true : false;

$contentType = @ $_GET['type'];

$args = [
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'feedbacks',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
];

$posts_array = get_posts( $args );

$posts_array_fin = [
    'compare' => [],
    'reviews' => [],
    'photo' => [],
    'video' => []
];

$postsValidData = [];
function getLink ($post_id): string
{
    global $postsValidData;
    $res = '';
    $key = array_search($post_id, array_keys($postsValidData));
    if($key > -1){
        $count = count(array_slice($postsValidData, 0, $key+1, true));
        $page = ceil($count/8);
        if($page>1){
            $res = '/feedbacks?page='.$page.'#'.$post_id;
        }
        else{
            $res = '/feedbacks#'.$post_id;
        }
    }
    return $res;
}
function getValidComparePosts($posts_array): array
{
    global $postsValidData;
    $posts_data = array_column($posts_array, 'post_date', 'ID');
    if(is_array($posts_data)){
        global $wpdb;
        $query = "select *, (select meta2.meta_value 
                            from wp_postmeta meta2 
                            where meta2.meta_key = '_feedbacks_reviews_text_value_key'
                            and meta2.post_id = post.ID
                            limit 1) as text 
                  from wp_postmeta meta 
                  join wp_posts post on meta.post_id = post.ID
                  where meta.meta_key = '_feedbacks_compare_before_value_key'
                  and (meta.meta_value is not null and meta.meta_value != '') 
                  and meta.post_id in (".implode(',', array_flip($posts_data)).") order by post.post_date DESC;";
        $queryRes = $wpdb->get_results($query);
        $postsValidData = array_column($queryRes, null, 'ID');
    }
    return $postsValidData;
}

if(count($posts_array)>0){

    if($contentType === 'compare') {

        $postsValidData = getValidComparePosts($posts_array);
        foreach ($postsValidData as $item){

            //echo '<br>getLink = ';
            //print_r(getLink($item->post_id));

        }

        //print_r($postsValidData);

        //die;


        foreach($posts_array as $key => $post){
            $compare_before = get_post_meta($post->ID, '_feedbacks_compare_before_value_key', true);
            $compare_after = get_post_meta($post->ID, '_feedbacks_compare_after_value_key', true);

            $compare_before_id = get_attachment_file_id($compare_before);
            $compare_before_image = get_thumbnails_by_id($compare_before_id, 'block-medium-image');
            $compare_before_image = $compare_before_image?$compare_before_image:$compare_before;

            $compare_after_id = get_attachment_file_id($compare_after);
            $compare_after_image = get_thumbnails_by_id($compare_after_id, 'block-medium-image');
            $compare_after_image = $compare_after_image?$compare_after_image:$compare_after;

            if($compare_before && $compare_after){
                $posts_array_fin['compare'][$key]['id'] = $post->ID;
                $posts_array_fin['compare'][$key]['compare_before'] = $compare_before_image;
                $posts_array_fin['compare'][$key]['compare_after'] = $compare_after_image;
            }
        }
    } elseif ($contentType === 'photo') {
        foreach($posts_array as $key => $post){
            $photo = get_post_meta($post->ID, '_feedbacks_photo_reviews_value_key', true);
            
            if($photo){
                $posts_array_fin['photo'][$key]['photo'] = $photo;
            }
        }
    } elseif ($contentType === 'video' && $showsCats['video']) {
        foreach($posts_array as $key => $post){
            $title = get_the_title($post->ID);
            $date = get_the_date('m.d.Y', $post->ID);
            $isVideoActive = get_post_meta($post->ID, '_feedbacks_video_active_value_key', true);
            $video_type = get_post_meta($post->ID, '_feedbacks_video_type_value_key', true);
            
            if($video_type == 'video'){
                $video = get_post_meta($post->ID, '_feedbacks_video_value_key', true);
            } else{
                $video = get_post_meta($post->ID, '_feedbacks_video_link_value_key', true);
            }
            
            if($video && $isVideoActive){
                $posts_array_fin['video'][$key]['title'] = $title;
                $posts_array_fin['video'][$key]['date'] = $date;
                $posts_array_fin['video'][$key]['isVideoActive'] = $isVideoActive;
                $posts_array_fin['video'][$key]['video_type'] = $video_type;
                $posts_array_fin['video'][$key]['video'] = $video;
                $posts_array_fin['video'][$key]['video_title'] = $posts_array_fin['video'][$key]['video']?get_post_meta($post->ID, '_feedbacks_video_title_value_key', true):'';
                $posts_array_fin['video'][$key]['video_title'] = $posts_array_fin['video'][$key]['video_title']?$posts_array_fin['video'][$key]['video_title']:$posts_array_fin['video'][$key]['title'];
            }
        }
    } else {
    	foreach($posts_array as $key => $post){
            $name = get_post_meta($post->ID, '_feedbacks_reviews_name_value_key', true);
            $age = get_post_meta($post->ID, '_feedbacks_reviews_age_value_key', true);
            $text = get_post_meta($post->ID, '_feedbacks_reviews_text_value_key', true);
            
            if($text){
                $posts_array_fin['reviews'][$key]['name'] = $name;
                $posts_array_fin['reviews'][$key]['age'] = $age;
                $posts_array_fin['reviews'][$key]['text'] = $text;
            }
        }
    }
}

$pageType = in_array($contentType, ['photo', 'video', 'compare']) ? $contentType : 'reviews';

$res = getPaged($posts_array_fin[$pageType], 8);
$posts_array = $res['posts_array'];
$background = '/img/header-bg.png';

if($isMobile){
    $background = '/img/header-bg-mobile.png';
}

?>
    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri() . $background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>
									Результаты
								</span>
							</span>
                            <p>Отзывы и результаты</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-feedback-page-->
    <section class="section-feedback-page  section-photo-testimonials">
        <div class="container">
            <div class="row">
                <!--begin page-nav-wrap-->
                <div class="col-xs-12">
                    <div class="page-nav-wrap">
                        <ul class="slider-page-nav">
                            <?php
                            echo '<li class="item'.($contentType==='compare' ?' active':'').'"><a href="/feedbacks?type=compare">фото до и после</a></li>';
                            echo '<li class="item' . ($pageType === 'reviews' ? ' active' : '') . '"><a href="/feedbacks">отзывы</a></li>';
                            echo '<li class="item' . ($contentType === 'photo' ? ' active' : '') . '"><a href="/feedbacks?type=photo">фото отзывы</a></li>';
                            
                            if ($showsCats['video']) {
                                echo '<li class="item' . ($contentType === 'video' ? ' active' : '') . '"><a href="/feedbacks?type=video">видео отзывы</a></li>';
                            }

                            ?>
                        </ul>
                    </div>
                </div>
                <!--end page-nav-wrap-->

                <?php if ($contentType === 'compare'): ?>

                <!--begin before-after-items-->
                <div class="col-xs-12">
                    <div class="tab-description">
                        <p>
                            Ниже Вы видите фотографии «до» лечения. Нажмите на кнопку в правом верхнем углу, чтобы увидеть результат «после».
                        </p>
                    </div>
                    <div class="row">

                        <?php if(count($posts_array)>0): ?>
                        <?php foreach($posts_array as $index => $post): ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="flipCard">
                                <div class="card">
                                    <div class="face front">
                                        <img src="<?= $post['compare_before']; ?>" alt="">
                                        <div class="button-flip-front heartbeat"></div>
                                    </div>
                                    <div class="face back">
                                        <img src="<?= $post['compare_after']; ?>" alt="">
                                        <div class="button-flip-back heartbeat"></div>
                                    </div>
                                </div>
                            </div>
                            <?php if(isset($post['id']) && !empty($postsValidData[$post['id']]->text)): ?>
                            <span class="popupInfo show-more" data-toggle="modal" data-target="#feedbacksModal_<?= $index; ?>">Читать отзыв</span>
                                <!-- Modal -->
                                <div id="feedbacksModal_<?= $index; ?>" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><?= $postsValidData[$post['id']]->post_title; ?></h4>
                                            </div>
                                            <div class="modal-body">
                                                <p><?= $postsValidData[$post['id']]->text; ?></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">закрыть</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                Нет материалов для отображения
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <!--end before-after-items-->

                <?php elseif ($contentType === 'photo'): ?>

                <!--begin testimonial-photo-items-->
                <div class="col-xs-12">
                    <div class="tab-description">
                        <p>
                            Нажмите на изображение, чтобы увеличить его
                        </p>
                    </div>
                    <div class="row">

                        <?php if(count($posts_array)>0): ?>
                        <?php foreach($posts_array as $post): ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="list-item-box-2">
                                <img src="<?= $post['photo']; ?>" alt="">
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                Нет материалов для отображения
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <!--end testimonial-photo-items-->

                <?php elseif ($contentType === 'video'): ?>

                <!--begin tetimonials-video-items-->
                <div class="col-xs-12">
                    <div class="tab-description">
                        <p>
                            Нажмите на кнопку для просмотра видеоролика.
                        </p>
                    </div>
                    <div class="row">

                    <?php if(count($posts_array) > 0): ?>
                        <?php foreach ($posts_array as $post): ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="video-box">
                                <div class="video-wrap">
                                    <?php
                                    if($post['video_type'] == 'embed'){
                                        $videoHtml = wp_oembed_get($post['video'], [
                                            'width'=>480,
                                            'height'=>353
                                        ]);
                                    }
                                    else{
                                        $videoHtml = wp_video_shortcode([
                                            'src'=>$post['video'],
                                            'width'=>480,
                                            'height'=>350
                                        ]);
                                    }
                                    ?>
                                    <?= $videoHtml; ?>
                                    <div class="overlay-box"></div>
                                </div>
                                <div class="video-title">
                                    <h3><?= $post['video_title']; ?></h3>
                                </div>
                                <div class="divider"></div>
                                <div class="date-box">
                                    <span><?= $post['date']; ?></span>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            Нет материалов для отображения
                        </div>
                    <?php endif; ?>

                    </div>
                </div>
                <!--end tetimonials-video-items-->
            	<?php else: ?>
        		<!--begin testimonials-items-->
                <div class="col-xs-12">
                    <div class="tab-description">
                        <p>
                        </p>
                    </div>
                    <div class="row">

                        <?php if(count($posts_array)>0): ?>
                        <?php foreach($posts_array as $post): ?>
                        <div class="col-xs-12">
                            <div class="media">
                                <div class="media-left">
                                    <span><b><?= $post['name']; ?></b></span>
                                    <span><?= $post['age']; ?></span>
                                </div>
                                <div class="media-body">
                                    <p><?= $post['text']; ?></p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                Нет материалов для отображения
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <!--begin testimonials-items end-->

                <?php endif; ?>

                <?php getPaganation($res); ?>

            </div>
        </div>


    </section>
    <!--end section-feedback-page-->

<?php get_footer(); ?>