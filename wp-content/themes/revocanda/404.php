<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage revocanda
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>
    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>ошибка 404</span>
							</span>
                            <p>ошибка 404</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-page-->
    <section class="section-about-us-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Страница не найдена</h1>
                    <p>Возможно Вы неправильно набрали адрес, или такой страницы на сайте больше не существует.</p>
                    <a href="/" class="main-button blue-button" hreflang="ru"><span>на главную</span></a>
                </div>
            </div>
        </div>
    </section>
    <!--end section-page-->
<?php get_footer();
