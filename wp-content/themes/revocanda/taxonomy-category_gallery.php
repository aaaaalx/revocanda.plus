<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

$posts_array = [];

$currUrl = (isset($_SERVER["HTTPS"]) ? 'https://' : 'http://').$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$currUrlParsed = parse_url($currUrl);

$gallery = 'gallery';
if(@$_GET['type'] == 'video'){
    $gallery = 'video';
}

$args = array(
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'gallery',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
);

$selected_center = '';
if(!empty($_GET['center'])){
    $selected_center = (int)$_GET['center'];
        $args['meta_query'][] = [
                'key' => '_gallery_centers_value_key',
                'compare' => 'LIKE',
                'value' => '"'.$selected_center.'"'
            ];
}

$sections = new WP_Query($args);
$sections = $sections->posts;

$section = '';
if(!empty($_GET['section'])){
    $section = (int)$_GET['section'];
    $args['page_id']  = $section;
}

$year = '';
if(isset($_GET['y']) && $_GET['y']){
    $year = (int)$_GET['y'];
    //$args['year'] = $year;
    $args['date_query'][] = ['year' => $year];
}
$month = '';
if(isset($_GET['m']) && $_GET['m']){
    $month = (int)$_GET['m'];
    //$args['monthnum'] = $month;
    $args['date_query'][] = ['monthnum' => $month];
}
$day = '';
if(isset($_GET['d']) && $_GET['d']){
    $day = (int)$_GET['d'];
    //$args['day'] = $day;
    $args['date_query'][] = ['day' => $day];
}

$posts_array = get_posts( $args );

$galleries = [];
$playlists = [];

if(count($posts_array)>0){
    foreach ($posts_array as $post){

        $pattern = get_shortcode_regex();
        preg_match_all( '/'. $pattern .'|(<img[^>]+>)/s', $post->post_content, $matches );

        $matches = array_map('array_filter', $matches);
        $matches = array_filter($matches);

        if (is_array($matches) && isset($matches[0])) {
            foreach ($matches[0] as $k =>$shortcode){
                if(isset($matches[2]) && isset($matches[2][$k])){//if gallery
                    if($matches[2][$k] == 'gallery'){
                        $galleries[$post->ID][$k]['shortcode'] = $shortcode;
                        $galleries[$post->ID][$k]['type'] = 'gallery';
                        if(isset($matches[3][$k])){
                            $galleries[$post->ID][$k]['attrs'] = $matches[3][$k];
                        }
                    }//if videos
                    else{
                        if($matches[2][$k] == 'playlist'){
                            $playlists[$post->ID][$k]['shortcode'] = $shortcode;
                            $playlists[$post->ID][$k]['type'] = 'playlist';
                            if(isset($matches[3][$k])){
                                $playlists[$post->ID][$k]['attrs'] = $matches[3][$k];
                            }
                        }
                        elseif($matches[2][$k] == 'video'){
                            $playlists[$post->ID][$k]['shortcode'] = $shortcode;
                            $playlists[$post->ID][$k]['type'] = 'video';
                            if(isset($matches[3][$k])){
                                $playlists[$post->ID][$k]['attrs'] = $matches[3][$k];
                            }
                        }
                        elseif($matches[2][$k] == 'embed'){
                            $playlists[$post->ID][$k]['shortcode'] = $shortcode;
                            $playlists[$post->ID][$k]['type'] = 'embed';
                            if(isset($matches[5][$k])){
                                $playlists[$post->ID][$k]['attrs'] = $matches[5][$k];
                            }
                        }
                    }
                }
                elseif (isset($matches[7]) && strpos($shortcode, '<img')>-1){//if image add to gallery array
                    $galleries[$post->ID][$k]['shortcode'] = $shortcode;
                    $galleries[$post->ID][$k]['type'] = 'img';
                }
            }
        }
    }
}
$galleries = array_filter($galleries);
$playlists = array_filter($playlists);

$finalCurrentUrl = (isset($currUrlParsed['path'])?$currUrlParsed['path']:'').(isset($currUrlParsed['query'])?'?'.$currUrlParsed['query']:'');

//get centers list
$args = array(
    'numberposts' => -1,
    'offset' => 0,
    'cat'    => 0,
    'orderby'     => 0,
    'order'       => 0,
    'post_type'   => 'centers',
    'status' => 0
);

$centers = getPostsByPostType($args['post_type'], $args['cat'], $args['numberposts'], $args['offset'], $args['orderby'], $args['order'], $args['status']);

$links = [];
$res = [];

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>

<!--begin section-intro-->
	<section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="inner-box">
						<div class="section-head-box">
							<span class="section-head-back">
								<span>Медиатека</span>
							</span>
							<p>Медиатека</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--end section-intro-->

	<!--begin section-medialibrary-page-->
	<section class="section-feedback-page">
		<div class="container">
			<div class="row">

				<!--begin page-nav-wrap-->
				<div class="col-xs-12">
					<div class="page-nav-wrap">
						<ul class="slider-page-nav">
							<li class="item<?php if($gallery == 'video'){echo ' active';} ?>"><a href="/gallery/?type=video">Видео</a></li>
							<li class="item<?php if($gallery == 'gallery'){echo ' active';} ?>"><a href="/gallery/">Фото</a></li>
						</ul>
					</div>
				</div>
				<!--end page-nav-wrap-->

				<!--begin filters-->
				<div class="col-xs-12">
					<div class="list-filter-group">
						<div class="row">
                            <form id="filter" method="get">
							<div class="col-md-6 col-xs-12">
								<div class="row">
									<div class="col-sm-6">
										<select name="section" id="section_filter" class="select-1 selectListGET">
                                            <option value="">Все секции</option>
                                            <?php
                                            if(count($sections)>0){
                                                foreach($sections as $k => $item){
                                                    $checked = '';
                                                    if($section == $item->ID){
                                                        $checked = ' selected';
                                                    }
                                                    echo '<option value="'.$item->ID.'"'.$checked.'>'.$item->post_title.'</option>';
                                                }
                                            }
                                            ?>
										</select>
									</div>
									<div class="col-sm-6">
										<select name="center" id="center_filter" class="select-1 selectListGET">
                                            <option value="">Все центры</option>
                                            <?php
                                            if(count($centers)>0){
                                                foreach($centers as $k => $item){
                                                    $checked = '';
                                                    if($selected_center == $item->ID){
                                                        $checked = ' selected';
                                                    }
                                                    echo '<option value="'.$item->ID.'"'.$checked.'>'.$item->post_title.'</option>';
                                                }
                                            }
                                            ?>
										</select>
									</div>
								</div>
							</div>
                             <!--
							<div class="col-md-6 col-xs-12">
								<div class="row">
									<div class="col-md-4 col-sm-6">
										<select onChange="rewrite_days()" name="y" id="year-select" class="select-3 selectListGET">
											<option value="">Год</option>
                                            <?php
/*                                            if($year){
                                                echo '<option value="'.$year.'" selected>'.$year.'</option>';
                                            }
                                            */?>
										</select>
									</div>
									<div class="col-md-4 col-sm-6">
										<select onChange="rewrite_days()" name="m" id="month-select" class="select-3 selectListGET">
											<option value="">Месяц</option>
                                            <?php
/*                                            if($month){
                                                echo '<option value="'.$month.'" selected>'.$month.'</option>';
                                            }
                                            */?>
										</select>
									</div>
									<div class="col-md-4 col-sm-6">
										<select name="d" id="day-select" class="select-3 selectListGET">
											<option value="">День</option>
                                            <?php
/*                                            if($day){
                                                echo '<option value="'.$day.'" selected>'.$day.'</option>';
                                            }
                                            */?>
										</select>
									</div>
								</div>
							</div>
                                -->
                            </form>
						</div>
					</div>
				</div>
				<!--end filters-->

				<!--begin gallery-items-->
				<div class="col-xs-12">
				<div class="tab-description">
                    <?php if($gallery == 'gallery'): ?>
					<p>Нажмите на кнопку в правом верхнем углу, чтобы увидеть подробную информацию</p>
                    <?php elseif ($gallery == 'video'): ?>
                    <p>Нажмите на кнопку для просмотра видеоролика.</p>
                    <?php endif; ?>
				</div>
					<div class="row">
                        <?php if($gallery == 'gallery'): ?>
						<div class="lightbox-wrap gallery">
                        <?php endif; ?>

                            <?php if($gallery == 'gallery'): ?>

                            <?php
                                $pattern = '/src="([^"]*)"';//img src
                                $pattern .= '|ids="([^"]*)"/s';//gallery ids

                            ?>

                            <?php if(count($galleries)>0): ?>
                            <?php foreach($galleries as $gKey => $post_gals): ?>
                            <?php foreach($post_gals as $gal): ?>

                            <?php
                            preg_match($pattern, $gal['shortcode'], $matches);
                            $matches = array_values(array_filter($matches));
                            if($gal['type'] == 'gallery'){
                                $g = explode(',', $matches[1]);
                                if (count($g)>0){
                                    foreach ($g as $gId){

                                        $url = wp_get_attachment_url($gId);
                                        $image = pippin_get_file_id($gId, 'block-small-image');
                                        $image = $image?$image:$url;

                                        $date =  get_the_date('m.d.Y', $gKey);
                                        $location =  get_post_meta($gKey, '_gallery_location_value_key', true);
                                        $title = get_the_title($gId);
                                        $links[] = ['title'=>$title,'url'=>$url,'thumbnails'=>$image,'date'=>$date,'location'=>$location];
                                    }
                                }
                            }
                            elseif($gal['type'] == 'img'){

                                $url = $matches[1];
                                $gId = pippin_get_file_id($url);
                                $image = get_thumbnails_by_id($gId, 'block-small-image');
                                $image = $image?$image:$url;

                                if($gId){
                                    $date = get_the_date('m.d.Y', $gKey);
                                    $location = get_post_meta($gKey, '_gallery_location_value_key', true);
                                    $title = get_the_title($gId);
                                    $links[] = ['title'=>$title,'url'=>$url,'thumbnails'=>$image,'date'=>$date,'location'=>$location];
                                }
                                else{
                                    $date = get_the_date('m.d.Y', $gKey);
                                    $location = get_post_meta($gKey, '_gallery_location_value_key', true);
                                    $title = get_the_title($gKey);
                                    $links[] = ['title'=>$title,'url'=>$url,'thumbnails'=>$image,'date'=>$date,'location'=>$location];
                                }
                            }
                            ?>

                            <?php endforeach; ?>
                            <?php endforeach; ?>

                            <?php if(count($links)>0): ?>

                            <?php
                            $res = getPaged($links, 6);
                            $links = $res['posts_array'];
                            ?>

                            <?php foreach($links as $attach): ?>

							<div class="col-md-4 col-sm-6 col-xs-12 gallery-item">
								<div class="list-item-box-3">
									<a href="<?= $attach['url']; ?>">
										<img src="<?= $attach['thumbnails']; ?>" alt="">
									</a>
									<div class="info-label"></div>
									<div class="gallery-item-description">
										<div class="item-close-button"></div>
										<div class="inner-wrap">
											<div class="item-title">
												<h3><?= $attach['title']; ?></h3>
											</div>
                                            <?php if(@$attach['location']): ?>
											<div class="item-address">
												<span><?= $attach['location']; ?></span>
											</div>
                                            <?php endif; ?>
											<div class="divider"></div>
											<div class="item-date">
												<span><?= $attach['date']; ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>

                            <?php endforeach; ?>
                            <?php endif; ?>

                            <?php else: ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    Нет материалов для отображения
                                </div>
                            <?php endif; ?>

                            <?php elseif ($gallery == 'video' && count($playlists)>0): ?>

                            <?php foreach($playlists as $post_id => $videos): ?>
                            <?php foreach($videos as $gal): ?>

                            <?php
                            $pattern = '/mp4="([^"]*)"';//mp4
                            $pattern .= '|m4v="([^"]*)"';//m4v
                            $pattern .= '|webm="([^"]*)"';//webm
                            $pattern .= '|ogv="([^"]*)"';//ogv
                            $pattern .= '|wmv="([^"]*)"';//wmv
                            $pattern .= '|flv="([^"]*)"';//flv
                            $pattern .= '|mov="([^"]*)"';//mov
                            $pattern .= '|ids="([^"]*)"';//playlist ids
                            $pattern .= '|\[embed\]([^"]*)\[\/embed\]/s';//embed videos

                            preg_match($pattern, $gal['shortcode'], $matches);
                            $matches = array_values(array_filter($matches));

                            if(!$matches){
                                continue;
                            }

                            if($gal['type'] == 'playlist'){
                                $g = explode(',', $matches[1]);
                                if (count($g)>0){
                                    foreach ($g as $gId){
                                        $url = wp_get_attachment_url($gId);
                                        $date =  get_the_date('m.d.Y', $post_id);
                                        $location =  get_post_meta($post_id, '_gallery_location_value_key', true);
                                        $title = get_the_title($gId);
                                        $links[] = ['title'=>$title,'url'=>$url,'date'=>$date,'location'=>$location];
                                    }
                                }
                            }
                            elseif($gal['type'] == 'video'){
                                $url = $matches[1];
                                $gId = pippin_get_file_id($url);
                                $date =  get_the_date('m.d.Y', $post_id);
                                $location =  get_post_meta($post_id, '_gallery_location_value_key', true);
                                $title = get_the_title($gId);
                                $links[] = ['title'=>$title,'url'=>$url,'date'=>$date,'location'=>$location];
                            }
                            elseif($gal['type'] == 'embed'){
                                $url = $matches[1];
                                $date =  get_the_date('m.d.Y', $post_id);
                                $embed = $gal['type']=='embed'?true:false;
                                $location =  get_post_meta($post_id, '_gallery_location_value_key', true);
                                $title = get_the_title($post_id);
                                $links[] = ['title'=>$title,'url'=>$url,'date'=>$date, 'embed'=>$embed,'location'=>$location];
                            }
                            ?>
                            <?php endforeach; ?>
                            <?php endforeach; ?>

                            <?php if(count($links)>0): ?>

                            <?php
                                    $res = getPaged($links, 6);
                                    $links = $res['posts_array'];
                                    ?>

                            <?php foreach($links as $attach): ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="video-box">
                                    <div class="video-wrap">
                                        <?php
                                        if(@$attach['embed']){
                                            $video = wp_oembed_get($attach['url']);
                                        }
                                        else{
                                            $video =  wp_video_shortcode([
                                                    'src'=>$attach['url'],
                                                    'width'=>380,
                                                    'height'=>280
                                            ]);
                                            //$video =  do_shortcode('[video src="'.$attach['url'].'" width="380" height="280"]');
                                        }
                                        ?>
                                        <?= $video; ?>
                                        <div class="overlay-box"></div>
                                    </div>
                                    <div class="video-title">
                                        <h3><?= @$attach['title']; ?></h3>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="date-box">
                                        <span><?= $attach['date']; ?></span>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <?php endif; ?>

                            <?php else: ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    Нет материалов для отображения
                                </div>
                            <?php endif; ?>

                        <?php if($gallery == 'gallery'): ?>
						</div>
                        <?php endif; ?>
					</div>
				</div>
				<!--end gallery-items-->

                <?php getPaganation($res); ?>

			</div>
		</div>
	</section>
	<!--end section-medialibrary-page-->

<?php
get_footer();
?>