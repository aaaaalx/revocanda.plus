<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package revocanda
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

if($isMobile){
    $img = get_the_post_thumbnail_url(null, 'large');
    if($img){
        $background = ' style="background-image:url('.$img.');"';
    }else{
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg-mobile.png);"';
    }
}
else{
    $img = get_the_post_thumbnail_url();
    if($img){
        $background = ' style="background-image:url('.$img.');"';
    }
    else{
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg.png);"';
    }
}

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
);

$current = new WP_Query( $args );
$children = $current->posts;
$parent = @get_ancestors($post->ID, 'page')[0];

?>

<?php while ( have_posts() ) : the_post(); ?>

    <!--begin section-intro-->
    <section class="section-poster-intro"<?= $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>
									<?php the_title(); ?>
								</span>
							</span>
                            <p><?php the_title(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->
    <!--begin section-about-us-page-->
    <section class="section-about-us-page">
        <div class="container">
            <div class="row">

                <?php if(count($children)>0): ?>
                <!--begin page-nav-wrap-->
                <div class="col-xs-12">
                    <div class="page-nav-wrap">
                        <ul class="slider-page-nav">
                            <?php foreach($children as $child): ?>
                            <li class="item"><a href="<?= get_permalink($child->ID); ?>" hreflang="ru"><?= $child->post_title; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <!--end page-nav-wrap-->
                <?php endif; ?>

                <?php if($parent): ?>
                <a class="back-button" href="<?= get_permalink($parent); ?>" hreflang="ru"><span>Вернуться</span></a>
                <?php endif; ?>

                <div class="col-xs-12">
                    <?php the_content(); ?>
                    <?php edit_post_link( 'Редактироваь', '<span class="edit-link">', '</span>' ); ?>
                </div>
            </div>
        </div>
    </section>
    <!--end section-about-us-page-->

<?php endwhile; ?>

<?php if(function_exists('dynamic_sidebar') && is_active_sidebar('page_bottom')): ?>
    <?php dynamic_sidebar('page_bottom') ?>
<?php endif; ?>

<?php get_footer();
