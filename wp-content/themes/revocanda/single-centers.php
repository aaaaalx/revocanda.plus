<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header();

global $isMobile;

//get page header background
$background = get_post_meta($post->ID, '_centers_settings_bg_value_key', true);
if($background){
    if($isMobile){
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'mobile-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
    else{
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'desktop-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
}
else{
    if($isMobile){
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg-mobile.png);"';
    }
    else{
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg.png);"';
    }
}
?>
<!--begin section-intro-->
<section class="section-poster-intro"<?= $background; ?>>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-box">
                    <div class="section-head-box">
							<span class="section-head-back">
								<span>
									Наши центры
								</span>
							</span>
                        <p><?= get_the_title(); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-intro-->

<!--begin section-item-page-->
<div class="section-center-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a class="back-button" href="/centers" hreflang="ru"><span>Все центры</span></a>
            </div>
            <div class="col-xs-12">

                <?php
                // Start the loop.
                while ( have_posts() ) : the_post();
                ?>
                <?php do_shortcode(the_content()); ?>
                <?php
                // End the loop.
                endwhile;
                ?>

            </div>
        </div>
    </div>
</div>
<!--end section-item-page-->

<?php
$specialistsOn = get_post_meta($post->ID, '_centers_settings_specialists_on_value_key', true);

$specialists = [];
if($specialistsOn){
    $args = [
        'posts_per_page'   => 10,
        'offset'           => 0,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'specialists',
        'post_status'      => 'publish',
        'meta_query' => [
            [
                'key' => '_specialists_centers_value_key',
                'compare' => 'LIKE',
                'value' => '"'.$post->ID.'"'
            ]
        ]
    ];
    $specialists = new WP_Query($args);
    $specialists = $specialists->posts;
}

?>

<?php if(count($specialists)>0): ?>
<!--begin section-specialist-->
<section class="section-specialist section-center-page blue-bg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="slider-specialist">

                    <?php foreach($specialists as $specialist): ?>

                    <?php
                    $position = get_post_meta($specialist->ID, '_specialists_position_value_key', true);
                    $image = get_the_post_thumbnail_url($specialist->ID, 'large');
                    ?>

                    <div class="item">
                        <?php if($image): ?>
                            <img src="<?= $image; ?>" alt="">
                        <?php endif; ?>
                        <div class="description-box">
                            <div class="inner-wrap">
                                <span><?= $specialist->post_title; ?></span>
                                <p><?= $position; ?></p>
                                <a href="<?= get_permalink($specialist->ID); ?>" hreflang="ru" class="more-info"><span>подробнее</span></a>
                            </div>
                        </div>
                    </div>

                    <?php endforeach; ?>


                </div>
                <a href="/specialists" class="main-button red-button to-right"><span>все специалисты</span></a>
            </div>
        </div>
    </div>
</section>
<!--end section-specialist-->
<?php endif; ?>

<?php
$citiesAndCountries = getCities(true);

$contactsOn = get_post_meta($post->ID, '_centers_settings_contacts_on_value_key', true);
$contacts = [];
$coordinates = null;
$phones = '';
if($contactsOn){

    $address = get_post_meta($post->ID, '_centers_address_value_key', true);
    $addressAdditionalInfo = get_post_meta($post->ID, '_centers_addressAdditionalInfo_value_key', true);
    $country = get_post_meta($post->ID, '_centers_country_value_key', true);
    $country = isset($citiesAndCountries['countries'][$country])?$citiesAndCountries['countries'][$country]['country']:'';
    $city = get_post_meta($post->ID, '_centers_city_value_key', true);
    $city = isset($citiesAndCountries['cities'][$city])?$citiesAndCountries['cities'][$city]['city']:'';
    $full_address = '';
    if($country){
        $full_address = $country;
    }
    if($city){
        $full_address .= $full_address?', ':'';
        $full_address .= $city;
    }
    if($address){
        $full_address .= $full_address?', ':'';
        $full_address .= $address;
    }
    if($full_address && $addressAdditionalInfo){
        $full_address = $full_address.', '.$addressAdditionalInfo;
    }
    $contacts['address'] = $full_address;

    $coordinates = get_post_meta($post->ID, '_centers_coordinates_value_key', true);

    $contacts['phones'] = get_post_meta($post->ID, '_centers_phones_value_key', true);
    $contacts['phones'] = $contacts['phones']?json_decode($contacts['phones'],true):[];

    $contacts['emails'] = get_post_meta($post->ID, '_centers_emails_value_key', true);
    $contacts['emails'] = $contacts['emails']?json_decode($contacts['emails'],true):[];

    $contacts['contacts'] = get_post_meta($post->ID, '_centers_contacts_value_key', true);
    $contacts['contacts'] = $contacts['contacts']?json_decode($contacts['contacts'],true):[];
}

?>

<?php if(isset($contacts['address']) && $contacts['address']): ?>
<!--begin section center-contacts-->
<div class="section-center-contacts">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-head-box">
						<span class="section-head-back">
							<span>Контакты</span>
						</span>
                    <h2 class="section-head-title">
                        <span>Контакты центра</span>
                    </h2>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">

                    <?php if(isset($contacts['address'])): ?>
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="address-box">
                            <?= $contacts['address']; ?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(is_array($contacts['phones']) && count($contacts['phones'])>0): ?>
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="phone-box">
                            <?php foreach($contacts['phones'] as $item): ?>
                            <?php if(!$item['phone_link']){
                                $item['phone_link'] = 'tel:'.str_replace(' ', '', $item['phone']);
                            }
                            ?>
                            <a href="<?= $item['phone_link']; ?>" hreflang="ru"><?= $item['phone']; ?></a>
                            <?php
                                $phones .= '<p>'.$item['phone'].'</p>';
                            ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(is_array($contacts['emails']) && count($contacts['emails'])>0): ?>
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="email-box">
                            <?php foreach($contacts['emails'] as $item): ?>
                                <?php if(!$item['email_link']){
                                    $item['email_link'] = 'mailto:'.str_replace(' ', '', $item['email']);
                                }
                                ?>
                                <a href="<?= $item['email_link']; ?>" hreflang="ru"><?= $item['email']; ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(is_array($contacts['contacts']) && count($contacts['contacts'])>0): ?>
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <?php foreach($contacts['contacts'] as $item): ?>
                        <div class="contact-box <?= $item['icon']; ?>">

                            <?php
                            $str = '';
                            if(isset($item['contacts_link'])){
                                $str .= '<a href="'.$item['contacts_link'].'>" hreflang="ru">';
                                $str .= $item['contacts'];
                                $str .= '</a>';
                            }
                            else{
                                $str .= '<span>'.$item['contacts'].'</span>';
                            }
                            echo $str;
                            ?>

                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>

                </div>
            </div>
            <?php if($coordinates): ?>
            <div class="col-xs-12">
                <div class="map-container">
                    <div id="map-canvas-center">

                    </div>
                </div>
                <?php
                $coordinates = explode(',', $coordinates);
                $address = preg_replace('/(\>)\s*(\<)/m', '$1$2', $contacts['address']);
                ?>
                <script>
                    if (document.getElementById('map-canvas-center')) {
                        function initMap() {
                            var uluru = {
                                lat: <?= @$coordinates[0]?$coordinates[0]:0; ?>,
                                lng: <?= @$coordinates[1]?$coordinates[1]:0; ?>
                            };
                            var map = new google.maps.Map(document.getElementById('map-canvas-center'), {
                                zoom: 17,
                                center: uluru
                            });

                            var contentString = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h3 id="firstHeading" class="firstHeading"><?= $post->post_title; ?></h3>' +
                                '<div id="bodyContent">' +
                                '<p><?= $address; ?></p>' +
                                '<?= $phones; ?>' +
                                '</div>' +
                                '</div>';

                            var infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });

                            var marker = new google.maps.Marker({
                                position: uluru,
                                map: map

                            });

                            function mapDescription() {
                                if ($(window).width() > 768) {
                                    infowindow.open(map, marker);
                                }
                            };
                            mapDescription();
                            $(window).resize(mapDescription);
                        }
                    }
                </script>
                <?php
                $key = get_option('field_mapKey');
                wp_register_script('user_map_script', 'https://maps.googleapis.com/maps/api/js?key='.$key.'&callback=initMap', '', '', true);
                wp_enqueue_script('user_map_script');
                ?>
            </div>
            <?php endif; ?>

        </div>
    </div>
</div>
<!--end section center-contacts-->
<?php endif; ?>

<?php
get_footer();
?>

