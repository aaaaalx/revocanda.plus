<?php
//get current template file name
global $template;
$currTemplate = substr($template, strrpos($template, '/')+1);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/Revocanda_plus_favicon_32x32px_v1-1.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/Revocanda_plus_favicon_34x34px_v1-1.ico" type="image/x-icon" sizes="64x64">

    <?php
    $title = '';
    $desc = '';
    $keywords = '';
    if($post){
        $title = get_post_meta($post->ID, '_seo_settings_title', true) ?: wp_title("", false);
        $desc = get_post_meta($post->ID, '_seo_settings_desc', true);//get custom meta box
        $keywords = get_post_meta($post->ID, '_seo_settings_keywords', true);//get custom meta box
        $title = $title ? $title : $post->post_title;
    }
    ?>

    <title><?= $title; ?></title>
    <?php if($desc): ?>
    <meta name="description" content="<?= $desc; ?>">
    <?php endif; ?>
    <?php if($keywords): ?>
    <meta name="keywords" content="<?= $keywords; ?>">
    <?php endif; ?>

    <?php if(is_singular() && pings_open(get_queried_object())): ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url') ?>">
    <?php endif; ?>
    <?php wp_head(); ?>

    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('head')): ?>
        <?php dynamic_sidebar('head') ?>
    <?php endif; ?>

</head>
<body>

<?php if(function_exists('dynamic_sidebar') && is_active_sidebar('body')): ?>
    <?php dynamic_sidebar('body') ?>
<?php endif; ?>

<!--begin header-->
<header>
    <div class="main-menu">
        <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <a href="/" class="logo-header logo-white" hreflang="ru"></a>
                </div>
                <div class="col-xs-10">
                    <button class="menu-button menu-button-white"></button>
                    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('header_email')): ?>
                    <button class="email-button email-button-grey"><?php dynamic_sidebar('header_email') ?></button>
                    <?php endif; ?>
                    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('header_phone')): ?>
                    <button class="contact-button contact-button-grey"><?php dynamic_sidebar('header_phone') ?></button>
                    <?php endif; ?>
                </div>

                <div class="contact-box submenu-box">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="item-list">
                                    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('phones')): ?>
                                        <?php dynamic_sidebar('phones') ?>
                                    <?php endif; ?>
                                </div>
                                <button class="contact-button-close menu-button-blue"></button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="nav-box submenu-box">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="item-list">
                        <!--main menu-->
                        <?php
                        $defaults = array(
                            'theme_location'  => '',
                            'menu'            => '',
                            'container'       => false,
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => '',
                            'menu_id'         => '',
                            'echo'            => true,
                            'fallback_cb'     => '',
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul>%3$s</ul>',
                            'depth'           => 0
                        );
                        wp_nav_menu($defaults);
                        ?>
                        <!--main menu end-->
                    </div>
                    <button class="menu-button-close menu-button-blue"></button>
                </div>
            </div>
        </div>
    </div>

</header>
<!--end header-->