<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

$event_types_filter = (int)@$_GET['event-type'];
$country_filter = (int)@$_GET['country'];
$city_filter = $country_filter?(int)@$_GET['city']:'';

$eventTypes = getEventsTypes();
$countries = getCountries();
$cities = [];
if($country_filter){
    $cities = getCitiesByCountry($country_filter);
    if(!isset($cities[$city_filter])){
        $city_filter = '';
    }
}

$args = [
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'events',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
];
if($event_types_filter){
    $args['meta_query'][] = [
        'key' => '_events_events_type_value_key',
        'compare' => 'LIKE',
        'value' => '"'.$event_types_filter.'"'
    ];
}
if($country_filter){
    $args['meta_query'][] = [
        'key' => '_events_country_value_key',
        'compare' => '==',
        'value' => $country_filter
    ];
}
if($city_filter){
    $args['meta_query'][] = [
        'key' => '_events_city_value_key',
        'compare' => '==',
        'value' => $city_filter
    ];
}

$posts_array = get_posts( $args );
$res = getPaged($posts_array);
$posts_array = $res['posts_array'];

$citiesAndCountries = getCities(true);

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>
    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>
									Мероприятия
								</span>
							</span>
                            <p>Мероприятия и тренинги</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-specialist-list-->
    <section class="section-item-list-2">
        <div class="container">
            <div class="row">
                <!--begin filters-->
                <div class="col-xs-12">
                    <div class="list-filter-group">
                        <div class="row">
                            <form id="filter" method="get">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select name="event-type" id="event_type_filter" class="select-2 selectListGET">
                                        <option value>Тип мероприятия</option>
                                        <?php
                                        if(count($eventTypes)>0){
                                            foreach($eventTypes as $k => $item){
                                                $checked = '';
                                                if($event_types_filter == (int)$k){
                                                    $checked = ' selected';
                                                }
                                                echo '<option value="'.$k.'"'.$checked.'>'.$item.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select name="country" id="country_filter" class="select-2 selectListGET">
                                        <option value>Страна</option>
                                        <?php
                                        if(count($countries)>0){
                                            foreach($countries as $k => $item){
                                                $checked = '';
                                                if($country_filter == $k){
                                                    $checked = ' selected';
                                                }
                                                echo '<option value="'.$k.'"'.$checked.'>'.$item.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select name="city" id="city_filter" class="select-2 selectListGET">
                                        <option value>Город</option>
                                        <?php
                                        if(count($cities)>0) {
                                            foreach ($cities as $k => $item) {
                                                $selected = '';
                                                if ($city_filter == $k) {
                                                    $selected = ' selected';
                                                }
                                                echo '<option value="' . $k . '"' . $selected . '>' . $item . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--end filters-->

                <!--begin list-item-->
                <?php if(count($posts_array)>0): ?>
                    <?php foreach($posts_array as $post): ?>
                        <?php
                        //set up address
                        $address = get_post_meta($post->ID, '_events_address_value_key', true);
                        $country = get_post_meta($post->ID, '_events_country_value_key', true);
                        $country = isset($citiesAndCountries['countries'][$country])?$citiesAndCountries['countries'][$country]['country']:'';
                        $city = get_post_meta($post->ID, '_events_city_value_key', true);
                        $city = isset($citiesAndCountries['cities'][$city])?$citiesAndCountries['cities'][$city]['city']:'';
                        $full_address = '';
                        if($country){
                            $full_address = $country;
                        }
                        if($city){
                            $full_address .= $full_address?', ':'';
                            $full_address .= $city;
                        }
                        if($address){
                            $full_address .= $full_address?', ':'';
                            $full_address .= $address;
                        }

                        $image = get_the_post_thumbnail_url($post->ID, 'block-medium-image');
                        $image = $image?$image:get_stylesheet_directory_uri().'/img/No-photo_380x290.jpg';


                        //get data
                        $date = get_post_meta($post->ID, '_events_date_value_key', true);
                        //$date =  get_the_date('m.d.Y', $post->ID);

                        $title = get_the_title($post->ID);
                        ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="list-item-box-2">
                                <a href="<?= get_permalink($post->ID); ?>">
                                    <?php if($date): ?>
                                    <div class="event-date-label">
                                        <span><?= $date; ?></span>
                                    </div>
                                    <?php endif; ?>
                                    <img src="<?= $image; ?>" alt="<?= $title; ?>">
                                </a>
                                <div class="inner-box">
                                    <div class="item-title">
                                        <a href="<?= get_permalink($post->ID); ?>">
                                            <h3><?= $title; ?></h3>
                                        </a>
                                    </div>
                                    <div class="divider"></div>
                                    <?php if($full_address): ?>
                                    <div class="item-address">
                                        <span><?= $full_address; ?></span>
                                    </div>
                                    <?php endif; ?>
                                    <div class="item-descriptions">
                                        <p><?= wp_trim_words( strip_shortcodes($post->post_content), 12, '...' ); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="list-item-box-2">
                            Нет материалов для отображения
                        </div>
                    </div>
                <?php endif; ?>
                <!--end list-item-->

                <?php getPaganation($res); ?>

            </div>
        </div>
    </section>
    <!--end section-specialist-->
<?php
get_footer();
?>