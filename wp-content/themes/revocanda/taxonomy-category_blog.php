<?php
get_header();

global $isMobile;

//get current category
$args = [
    'posts_per_page' => -1,
    'offset' => 0,
    'category' => '',
    'category_name' => '',
    'orderby' => 'date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' => '',
    'post_type' => 'blog',
    'post_mime_type' => '',
    'post_parent' => '',
    'author' => '',
    'author_name' => '',
    'post_status' => 'publish',
    'suppress_filters' => true,

];

$posts_array = get_posts($args);
$res = getPaged($posts_array, 8);
$posts_array = $res['posts_array'];

$background = '/img/header-bg.png';
if ($isMobile) {
    $background = '/img/header-bg-mobile.png';
}
?>
    <!--begin section-intro-->
    <section class="section-poster-intro"
             style="background-image: url(<?php echo get_stylesheet_directory_uri() . $background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>&nbsp;</span>
							</span>
                            <p>Блог</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-price-page-->
    <section class="section-blog-page">
        <div class="container">
            <div class="row">
                <?php if (count($posts_array) > 0): ?>
                    <!--begin price-items-->
                    <div class="col-xs-12">
                        <div class="row">
                            <?php foreach ($posts_array as $p): ?>
                                <?php
                                $image = get_the_post_thumbnail_url($p->ID, 'block-medium-image');
                                $image = $image ? $image : get_stylesheet_directory_uri() . '/img/No-photo_380x290.jpg';
                                $link = get_permalink($p->ID);
                                ?>
                                <div class="col-sm-6 col-xs-12 relative">
                                    <div class="post">
                                        <div class="post-title">
                                            <a href="<?php echo $link; ?>" class="post-title-link">
                                                <h3><?= $p->post_title; ?></h3>
                                            </a>
                                        </div>
                                        <div class="post-meta">
                                            <div class="post-meta-author">
                                                <span><?php echo human_time_diff(get_post_time('U', false, $p), current_time('timestamp')) . ' ' . __('назад') ?></span>
                                                <span><?php echo get_the_author_meta('first_name', $p->post_author) . ' ' . get_the_author_meta('last_name', $p->post_author); ?></span>
                                            </div>
                                        </div>
                                        <div class="post-media">
                                            <a href="<?php echo $link ?>">
                                                <img src="<?= $image; ?>" alt="<?= $p->post_title; ?>">
                                            </a>
                                        </div>
                                        <div class="post-description">
                                            <?= get_extended($p->post_content)['main']; ?>
                                        </div>
                                        <a href="<?= $link ?>">Подробнее</a>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <!--end price-items-->
                <?php else: ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="list-item-box-2">
                            Нет материалов для отображения
                        </div>
                    </div>
                <?php endif; ?>

                <?php getPaganation($res); ?>

            </div>
        </div>
    </section>
    <!--end section-price-page-->


<?php get_footer(); ?>