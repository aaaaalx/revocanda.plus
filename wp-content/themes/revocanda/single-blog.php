<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage revocanda
 */


get_header();

global $isMobile;

$background = get_post_meta($post->ID, '_specialists_settings_bg_value_key', true);

if ($background) {
    if ($isMobile) {
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'mobile-header');
        $background = $image ? $image : $background;
        $background = ' style="background-image:url(' . $background . ');"';
    } else {
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'desktop-header');
        $background = $image ? $image : $background;
        $background = ' style="background-image:url(' . $background . ');"';
    }
} else {
    if ($isMobile) {
        $background = ' style="background-image:url(' . get_stylesheet_directory_uri() . '/img/header-bg-mobile.png);"';
    } else {
        $background = ' style="background-image:url(' . get_stylesheet_directory_uri() . '/img/header-bg.png);"';
    }
}

$title = get_the_title();
?>
    <!--begin section-intro-->
    <section class="section-poster-intro"<?= $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>Блог</span>
							</span>
                            <p><?= get_the_title() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

<?php
$image = get_the_post_thumbnail($post->ID, 'large');
?>

    <!--begin section-specialist-list-->
    <section class="section-blog-single">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a class="back-button" href="/blog" hreflang="ru"><span>Все записи</span></a>
                </div>
                <div class="col-xs-12">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php if ($image): ?>
                            <div class="post-media">
                                <?= $image; ?>
                            </div>
                        <?php endif; ?>

                        <?= do_shortcode(get_the_content()); ?>

                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
    <!--end section-specialist-->

<?php get_footer(); ?>