 
$(document).ready(function() {
  
  /*header parallax*/
  var introHeader = $('.section-intro');
  var intro = $('.section-intro');

  function effectsModuleHeader(introHeader, scrollTopp) {
    if (introHeader.length > 0) {
      var homeSHeight = introHeader.height();
      var topScroll = $(document).scrollTop();

      if ((introHeader.hasClass('section-intro')) && ($(scrollTopp).scrollTop() <= homeSHeight)) {
        introHeader.css('top', (topScroll * 0.55));
      }

      if (introHeader.hasClass('section-intro') && ($(scrollTopp).scrollTop() <= homeSHeight)) {
        introHeader.css('opacity', (1 - topScroll / introHeader.height() * 0.8));
      }
    }
  }

  $(window).scroll(function() {
    effectsModuleHeader(introHeader, this);
  });
  
  function parallaxMarginTop(){
    $(".section-slider-services").css({"margin-top": $(".section-video").height()*1.087688}); 
    $(".section-intro").css({"height": $(".section-video").height()});
    $(".flying-video").css({"height": $(".section-video").height() * 1.2 });
  }
  parallaxMarginTop();
  $(window).resize(parallaxMarginTop);

  /*main-menu*/  

  /*logo scroll function*/
  function logoScroll(){
   if($(".main-menu").hasClass("scroll-menu")){
    $(".logo-header").removeClass("logo-white").addClass("logo-blue");
  }
  else {
    $(".logo-header").removeClass("logo-blue").addClass("logo-white");
  } 
}

/*nav-box*/
$(".menu-button").click(function(){
  $(".nav-box").addClass("show");
  $("header").addClass("white-bg");
  $(".logo-header").removeClass("logo-white").addClass("logo-blue"); 
  $(".login-box").removeClass("show")
});
$(".menu-button-close").click(function(){
  $(".nav-box").removeClass("show");
  $("header").removeClass("white-bg");
  $(".contact-box").removeClass("show");
  $(".login-box").removeClass("show")
  logoScroll();
});

/*language-box*/
$(".language-button").click(function(){
  $(".language-box").addClass("show");
  $("header").addClass("white-bg");
  $(".logo-header").removeClass("logo-white").addClass("logo-blue");
  $(".nav-box").removeClass("show");
  $(".login-box").removeClass("show");
  $(".contact-box").removeClass("show")
});
$(".language-button-close").click(function(){
  $(".language-box").removeClass("show");
  $("header").removeClass("white-bg");
  $(".nav-box").removeClass("show");
  $(".login-box").removeClass("show");
  logoScroll();
}); 

/*contact-box*/
$(".contact-button").click(function(){
  $(".contact-box").addClass("show");
  $("header").addClass("white-bg");
  $(".logo-header").removeClass("logo-white").addClass("logo-blue");
});
$(".contact-button-close").click(function(){
  $(".contact-box").removeClass("show");
  $("header").removeClass("white-bg");
  logoScroll();
}); 

/*personal-cabinet-box*/
$(".login-button").click(function(){
  $(".login-box").addClass("show");
  $("header").addClass("white-bg");
  $(".logo-header").removeClass("logo-white").addClass("logo-blue");
  $(".nav-box").removeClass("show");
  $(".language-box").removeClass("show");
});
$(".login-button-close").click(function(){
  $(".login-box").removeClass("show");
  $("header").removeClass("white-bg");
  $(".nav-box").removeClass("show");
  $(".language-box").removeClass("show");
  logoScroll();
})

/*header scroll color*/
function scrollMenu(){
  if ( $(window).scrollTop() > 0) {
    $(".logo-header").removeClass("logo-white").addClass("logo-blue");
    $(".main-menu").addClass("scroll-menu");
    $(".menu-button").removeClass("menu-button-white").addClass("menu-button-blue");
    $(".login-button").removeClass("login-button-white").addClass("login-button-blue");
    $(".language-button").removeClass("language-button-white").addClass("language-button-grey");
    $(".email-button").addClass("show");
    $(".contact-button").addClass("show");

  } else {
    $(".logo-header").removeClass("logo-blue").addClass("logo-white");
    $(".main-menu").removeClass("scroll-menu");
    $(".menu-button").removeClass("menu-button-blue").addClass("menu-button-white");
    $(".login-button").removeClass("login-button-blue").addClass("login-button-white");
    if($(".submenu-box").hasClass("show")){
      $(".logo-header").removeClass("logo-white").addClass("logo-blue");
    }
    $(".language-button").removeClass("language-button-grey").addClass("language-button-white");
    $(".email-button").removeClass("show");
    $(".contact-button").removeClass("show");

  }
}
function menuResize(){
  if($(window).width() >= 748){
    scrollMenu(false);
  } 
  else {
    scrollMenu();
  } 
};
$(window).scroll(menuResize);  
menuResize();
scrollMenu();


/*flip-card*/

$(".flipCard").click(function(){
  $(this).find(".card").toggleClass("flipped");
  return false;
});


/*blue-button-animation*/
$(".blue-button").mouseover(function(){
 $(this).addClass("animate");
 setTimeout(function(){
  $(".blue-button").css("background-color", "#1a62a7");
}, 500);
})
$(".blue-button").mouseout(function(){
 $(this).removeClass("animate");	
 $(".blue-button").css("background-color", "#1874cd");
});
/*red-button-animation*/
$(".red-button").mouseover(function(){
 $(this).addClass("animate-red");
 setTimeout(function(){
  $(".red-button").css("background-color", "#dcdcdc");
}, 500);
})
$(".red-button").mouseout(function(){
 $(this).removeClass("animate-red");	
 $(".red-button").css("background-color", "#fff");
});


/*feedback-page modal*/

/*photo-testimonials*/
$( ".section-photo-testimonials .list-item-box-2 img" ).click(function() {

  imagen = $(this).attr( "src");
  $( ".modal-body" ).html("<img src="+imagen+">");

  $('#modalTestimonialsPhoto').modal('show'); 
});

/*video-testimonials*/
var newPlayer = null;
$( ".video-box" ).click(function() {
    var elem = $(this).find("iframe, video");
    elem.removeAttr("id");
    elem.removeAttr("class");
    var wraped = '';
    if(elem.is('video')){
        wraped = '<div style="display: none;" class="wp-video">'+elem.clone()[0].outerHTML+'</div>';
        $(wraped).appendTo($("#modalVideo .modal-body-video"));
        var newElem = $("#modalVideo .modal-body-video video");
        newElem.attr('id', 'popupvideo');
        $('#modalVideo').modal('show');
        setTimeout(function(){
            newPlayer = new MediaElementPlayer('#popupvideo', {});
            $("#modalVideo .modal-body-video .wp-video").css('display', 'block');
        }, 300);
    }
    else{
        wraped = elem.clone()[0].outerHTML;
        $(wraped).appendTo($("#modalVideo .modal-body-video"));
        $('#modalVideo').modal('show');
    }
});
$("#modalVideo .close, #modalVideo").on('click', function(e){
    e.stopPropagation();
    if($(e.target).attr('id') == 'modalVideo' || $(e.target).attr('id') == 'closebutton'){
        if(newPlayer){
            newPlayer.remove();
        }
        $("#modalVideo .modal-body-video").empty();
        $('#modalVideo').modal('toggle');
    }
});


$(".slider-our-services").slick({
 infifte: true,
 fade: true,
 dots: true,
 cssEase: 'linear',
 nextArrow: '<div class="arrow-next-red draw-red"></div>',
 prevArrow: '<div class="arrow-prev-red draw-red"></div>',
 responsive: [
 {
  breakpoint: 992,
  settings: {  			
   arrows: false,  				
 }
},    
]
});

$(".slider-specialist").slick({
 infifte: true,
 dots: true,
 slidesToShow: 4,
 slidesToScroll: 1,
 nextArrow: '<div onclick="ga(\'send\', \'event\', \'button2\', \'specialists\')" class="arrow-next-red draw-red"></div>',
 prevArrow: '<div onclick="ga(\'send\', \'event\', \'button2\', \'specialists\')" class="arrow-prev-red draw-red"></div>',
 responsive: [
 {
  breakpoint: 1200,
  settings: {
   slidesToShow: 3,
 }
},
{
  breakpoint: 992,
  settings: {
   slidesToShow: 2,
 }
},
{
  breakpoint: 769,
  settings: {
   slidesToShow: 2,
   arrows: false,  		
 }
},
{
  breakpoint: 650,
  settings: {
   slidesToShow: 1,
   slidesToScroll: 1,  
   arrows: false,		
 }
}   
]
});
$(".slider-results").slick({
 infifte: true,
 slidesToShow: 1,
 slidesToScroll: 1,
 dots: true,
 fade: true,
 cssEase: 'linear',
 nextArrow: '<div class="arrow-next-white draw-red"></div>',
 prevArrow: '<div class="arrow-prev-white draw-red"></div>',
 responsive: [
 {
  breakpoint: 650,
  settings: {  			
   arrows: false,
 }
},    
]
});

$(".slider-gallery").slick({
 infifte: true,
 slidesToShow: 3,
 dots: true,
 slidesToScroll: 1,
 nextArrow: '<div onclick="ga(\'send\', \'event\', \'button3\', \'photo\')" class="arrow-next-red draw-red"></div>',
 prevArrow: '<div onclick="ga(\'send\', \'event\', \'button3\', \'photo\')" class="arrow-prev-red draw-red"></div>',
 responsive: [
 {
  breakpoint: 992,
  settings: {
   slidesToShow: 2,
   arrows: false,
 }
},  	 
{
  breakpoint: 768,
  settings: {
   slidesToShow: 1,
   arrows: false,
 }
},    
]
});
$(".slider-gallery").each(function(){
 $(this).slickLightbox();
});

$(".lightbox-wrap").each(function(){
 $(this).slickLightbox();
});
$(".slider-centers").slick({
 infifte: true,
 slidesToShow: 1,
 slidesToScroll: 1,
 fade: true,
 dots: true,
 cssEase: 'linear',
 nextArrow: '<div class="arrow-next-white draw-red"></div>',
 prevArrow: '<div class="arrow-prev-white draw-red"></div>',
 responsive: [
 {
  breakpoint: 2000,
  settings: {
   dots: false,
 }
},
{
  breakpoint: 768,
  settings: {
   dots: true,
   arrows: false,
 }
}      


]
});
$(".slider-testimonials").slick({
 infifte: true,
 slidesToShow: 1,
 slidesToScroll: 1,
 dots: true,  
 nextArrow: '<div onclick="ga(\'send\', \'event\', \'button4\', \'reviews\')" class="arrow-next-red draw-red"></div>',
 prevArrow: '<div onclick="ga(\'send\', \'event\', \'button4\', \'reviews\')" class="arrow-prev-red draw-red"></div>',
 responsive: [
 {
  breakpoint: 769,
  settings: {
   arrows: false,
 }
} 
]
});

$(".slider-one-item").slick({
 infifte: true,
 slidesToShow: 1,
 slidesToScroll: 1,
 dots: false,  
 nextArrow: '<div class="arrow-next-blue"></div>',
 prevArrow: '<div class="arrow-prev-blue"></div>', 
});


$(".slider-page-nav").slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  dots: false,  
  infinite: false,
  nextArrow: '<div class="arrow-next-red draw-red"></div>',
  prevArrow: '<div class="arrow-prev-red draw-red"></div>',
  responsive: [
  {
    breakpoint: 1200,
    settings: {
     slidesToShow: 4,
   }
 },
 {
  breakpoint: 992,
  settings: {
   slidesToShow: 3,
 }
},
{
  breakpoint: 768,
  settings: {
   slidesToShow: 2,
 }
},
{
  breakpoint: 550,
  settings: {
   slidesToShow: 1,
 }
}   
]
});

/*back-button*/
function goBack(){
  window.history.back();
}

$(".back-button").click(function(){
  goBack();
})

/*medialibrary_page select calendar*/
function selectYear(){
  var currentYear = new Date().getFullYear();
  var select = document.getElementById("year-select");
  if(!select){
    console.log("There is no element!");
  }
  else{
    for (var i = currentYear; i >= 2000; i-- ){
      var opt = document.createElement('option');
      opt.value = i;
      opt.innerHTML = i;
      select.appendChild(opt);
    }
  }
}
selectYear();

function selectMonth(){
  var monthes = {
    'name': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    'val': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  }
  var select = document.getElementById("month-select");
  if (!select){
   console.log("There is no element!"); 
 }
 else {
  for (var i = 0; i < 12; i++){
    var opt = document.createElement('option');
    opt.value = monthes['val'][i];
    opt.innerHTML = monthes['name'][i];
    select.appendChild(opt);

  }
}
}
selectMonth();

function selectDays(){
  var select = document.getElementById("day-select");
  if (!select){
   console.log("There is no element!");  
  }
  else{
  for (var i = 1; i < 32; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);

  }
  }
}
selectDays();


/*gallery_page.html gallery-item-description show*/
function galleryInfo(){
  $(".info-label").click(function(){
    $(this).next(".gallery-item-description").slideDown();
    $(this).slideUp();
  });
  $(".item-close-button").click(function(){
    $(this).parent().slideUp();
    $(this).parent().prev().slideDown();
  });  
}
galleryInfo();


/*price_page.html show more button*/
$( ".section-price-page .inner-wrap .show-more" ).click(function() {
 $(this).parent().clone().appendTo($("#modalPrice .modal-body-price"));
 $('#modalPrice').modal('show'); 
});
$("#modalPrice .close, #modalPrice").click(function(){
  $("#modalPrice .modal-body-price").empty();
});

/*price_page.html show more-toggle button*/
$( ".section-price-page .inner-wrap .more-toggle" ).click(function() {
 $('#modalContactForm').modal('show'); 
});

$('#menu-nizhnee-menju-2 a').click(function(){
    ga('send', 'event', 'event', 'Problems');
});

$('.section-price-page .pagination li a').click(function(){
    ga('send', 'event', 'button5', 'prices');
});

});