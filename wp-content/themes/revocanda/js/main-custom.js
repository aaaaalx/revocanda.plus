$(document).ready(function() {

    $('.selectListGET').on('change', function() {
        $('#filter').submit();
    });

    $('.selectList').on('change', function(){
        getSelectList(this);
    });
    //get select list by ajax
    function getSelectList(select) {
        if(select.length){
            select = $(select);
            var connected = select.data('connected');
            var call = select.data('call');
            var value = select.data('value');
            var action = 'revocanda_get_select_list';
            var url = '/wp-admin/admin-ajax.php';
            var code = {
                connected: connected,
                call: call
            };
            if(value){
                code.value = select.val();
            }
            else{
                code.value = null;
            }
            $.ajax({
                url: url,
                type: 'post',
                async: false,
                data: {
                    data: code,
                    action: action
                },
                error: function(response){

                },
                success: function(response){
                    if(response){
                        var responseA = JSON.parse(response);
                        var resConnected = responseA.connected;
                        var resData = responseA.data;
                        if(resConnected){
                            var elem = $('#'+resConnected);
                            if(elem.length){
                                var first = elem.children()[0];
                                elem.html('');
                                if(first){
                                    elem.append(first);
                                }
                                $.each(resData, function (k, v) {
                                    elem.append($("<option></option>").attr("value",k).text(v));
                                });
                            }
                        }
                    }
                }
            });

        }
    }
	
	/*registr form submit*/
    var formProcessing = false;
    $('.form-feedback').on('submit', function(e){
        e.preventDefault();
        if(formProcessing){
            return;
        }
        formProcessing = true;
        var form = $(this);
        //form fields
        var username = $(form).find('.username').val();
        var phone = $(form).find('.phone').val();
        var email = $(form).find('.email').val();
		var message = $(form).find('.message').val();
        var action = 'revocanda_save_contact_form';
        var url = form.data('url');
        form.append('<div class="form-cover"></div>');
        $.ajax({
            url: url,
            type: 'post',
            data: {
                username: username,
                phone: phone,
                email: email,
                message: message,
                action: action
            },
            error: function(response){

            },
            success: function(response){
                if(response){
                    formProcessing = false;
                    form.find('.form-cover').remove();
                    window.location = '/alert'
                }
            }
        });

    });

    $(".slider-4-items").slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                }
            }
        ]
    });

});