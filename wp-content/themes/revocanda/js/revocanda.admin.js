jQuery(document).ready(function($){

    //media library calling
    var mediaUploader = {};
    $(document).on('click', '.widget-upload-button', function(e){
        e.preventDefault();
        var thisId;
        if($(this).data('id')){
            thisId = $(this).data('id');
            var elem = $(this).parent().find('#'+thisId);
        }
        else{
            var elem = $(this).next();
            thisId = $(elem).attr('id');
        }
        var previw = $(this).prev();
        var dataOpt = $(this).data('options');

        //get options from data attribute
        if(dataOpt !== undefined){
            dataOpt = dataOpt.split(',');
            var dataValues = [];
            if(dataOpt.length){
                var dataObj = {};
                $(dataOpt).each(function(k, v){
                    dataValues = v.split(':');
                    if(dataValues[1] === 'true'){
                        dataValues[1] = true;
                    }
                    else if(dataValues[1] === 'false'){
                        dataValues[1] = false;
                    }
                    if(dataValues[0] == 'type'){
                        if(dataValues[0] in dataObj){
                            dataObj[dataValues[0]].push(dataValues[1]);
                        }
                        else{
                            dataObj[dataValues[0]] = [];
                            dataObj[dataValues[0]].push(dataValues[1]);
                        }
                    }
                    else{
                        dataObj[dataValues[0]] = dataValues[1];
                    }
                });
                dataOpt = dataObj;
            }
        }
        else{
            dataOpt = {
                multiple: false,
                type: ['image']
            };
        }

        if(!mediaUploader[thisId]){
            mediaUploader[thisId] = wp.media.frames.file_frame = wp.media({
                title: 'Выбрать файл',
                button: {
                    text: 'Выбрать файл'
                },
                multiple: dataOpt.multiple,
                library: {
                    type: dataOpt.type
                }
            });
            checkSelectedMedia(thisId);
        }

        mediaUploader[thisId].on('select', function(){
            //if multiselect media files
            if(dataOpt.multiple === true){
                var attachments = mediaUploader[thisId].state().get('selection').toJSON();
                if(elem.length && attachments.length){
                    var htmlCont = '';
                    var fieldCont = '';
                    $(attachments).each(function(k, attachment){
                        if (getMediaType(attachment.mime) == 'image'){
                            var path = attachment.sizes.full.url.replace(/^.*\/\/[^\/]+/, '');
                            if(k == 0){
                                fieldCont += path;
                            }
                            else{
                                fieldCont += ', '+path;
                            }
                            if(previw.length){
                                htmlCont += '<a class="thickbox" href="'+path+'?TB_iframe=false&width=100%&height=100%"><img src="'+path+'"></a>';
                            }
                        }
                        else if(getMediaType(attachment.mime) == 'video'){
                            //make multi video insert and preview
                        }
                        else{
                            //make multi media insert and preview
                        }

                        //display slected images in popup window
                        attachment = wp.media.attachment(attachment.id);
                        attachment.fetch({
                            success: function(){
                                mediaUploader[thisId].state().get('selection').add( attachment ? [ attachment ] : [] );
                                $(elem).attr('id');
                            }
                        });

                    });
                    $(elem).val(fieldCont);
                    elem.trigger( 'change' );
                    $(previw).html(htmlCont);
                }
            }
            else{//if single media file
                var attachment = mediaUploader[thisId].state().get('selection').first().toJSON();
                if(elem.length && attachment){
                    if (getMediaType(attachment.mime) == 'image'){
                        var path = attachment.sizes.full.url.replace(/^.*\/\/[^\/]+/, '');
                        $(elem).val(path);
                        elem.trigger( 'change' );
                        if(previw.length){
                            $(previw).html('<a class="thickbox" href="'+path+'?TB_iframe=false&width=100%&height=100%"><img src="'+path+'"></a>');
                        }
                    }
                    else if(getMediaType(attachment.mime) == 'video'){
                        var path = attachment.url.replace(/^.*\/\/[^\/]+/, '');
                        $(elem).val(path);
                        elem.trigger( 'change' );
                        var videoStr = '';
                        videoStr += '<video width="150" height="150" controls preload="metadata">';
                        videoStr += '<source src="'+path+'" type="'+attachment.mime+'">';
                        videoStr += '</video>';
                        $(previw).html(videoStr);
                    }
                    else{
                        var path = attachment.url.replace(/^.*\/\/[^\/]+/, '');
                        $(elem).val(path);
                        elem.trigger( 'change' );
                        var icon = '<div class="contBlock"><span class="dashicons dashicons-paperclip"></span></div>';
                        $(previw).html(icon+attachment.filename);
                    }

                    //display slected images in popup window
                    attachment = wp.media.attachment(attachment.id);
                    attachment.fetch({
                        success: function(){
                            mediaUploader[thisId].state().get('selection').add( attachment ? [ attachment ] : [] );
                        }
                    });

                }
            }
        });
        mediaUploader[thisId].open();
    });

    function checkSelectedMedia(thisId){
         if(selectedMedia[thisId]){
             if(selectedMedia[thisId].length){
                 $(selectedMedia[thisId]).each(function(k, v){
                     var attachment = wp.media.attachment(v);
                     attachment.fetch({
                         success: function(){
                             mediaUploader[thisId].state().get('selection').add( attachment ? [ attachment ] : [] );
                         }
                     });
                 });
             }
         }
    }

    $(document).on('click', '.widget-remove-button', function(e){
        e.preventDefault();
        if($(this).data('id')){
            var thisId = $(this).data('id');
            var elem = $(this).parent().find('#'+thisId);
        }
        else{
            var elem = $(this).prev();
        }
        var imgPreview = $(this).siblings('.widget-block-img');
        var answer = confirm('Вы действительно хотите удалить файл?');
        if(answer == true){
            if(elem){
                $(elem).val('');
                elem.trigger( 'change' );
                $(imgPreview).html('');
            }
        }
        return;
    });

    function getMediaType(media){
        if(media && media.indexOf('/')>=0){
            return media.substr(0,media.indexOf('/'));
        }
    }

    //if link - get url and target from it
    $(document).on('change', '.linkOnly', function(){
        var elem = $(this);
        var n = elem.attr('id').lastIndexOf("-");
        var elemTarget = $('#'+elem.attr('id').substr(0, n)+'-linkTarget');
        var string = elem.val();
        if(string.length){
            div = document.createElement('div');
            div.innerHTML = string;
            var a = $(div).find(' > a');
            var target = a.attr('target');
            a = a.attr('href');
            if(target !== undefined && target){
                elemTarget.prop("checked", true);
            }
            else{
                elemTarget.prop("checked", false);
            }
            if(a){
                elem.val(a);
            }
            else{
                elem.val(string);
            }
        }
    });

    $(document).on('click', '.getLinksList', function () {
        callWpLinkPicker($(this).attr('id'));
    });

    var addadEditors = [];

    $(document).on('widget-updated', function(e, widget) {
        callVisualEditors(widget);
        initColorPicker( widget );
    });
    callVisualEditors();

    $(document).on('widget-added', function(e, widget) {
        callVisualEditors(widget);
        initColorPicker( widget );
    });

    //init color picker in widgets
    function initColorPicker(widget) {
        var widgetId = $(widget)[0].id;
        var colorPickers = $('#'+widgetId).find('.colorPicker');
        if(colorPickers.length){
            colorPickers.each(function (k, v) {
                $(v).wpColorPicker( {
                    change: _.throttle( function() { // For Customizer
                        $(this).trigger( 'change' );
                    }, 3000 ),
                    clear: _.throttle( function() { // For Customizer
                        $(this).trigger( 'change' );
                    }, 4000 )
                });
            });
        }
    }

    //actions on active widgets
    var widgets = $('.widget').not('.ui-draggable');
    if(widgets.length){
        widgets.each(function (k, widget) {
            //init color pickers in the active widgets
            initColorPicker(widget);

            //detect widgets dragging
            isDraggingElem(widget);

        });
    }

    //detect real dragging and calls actions that need to be called on dom elements movement
    function isDraggingElem(elem) {
        if(elem){
            var isDragging = false;
            var dragStartPosY = null
            var dragCurrPosY = null;
            var elemId = $(elem).attr('id');
            elemId = '#'+elemId;
            $(elemId+' .widget-top').mousedown(function(e) {
                isDragging = false;
                dragStartPosY = event.clientY;
            }).mousemove(function() {
                isDragging = true;
            }).mouseup(function() {
                var wasDragging = isDragging;
                var isMove = false;
                dragCurrPosY = event.clientY;
                if(dragCurrPosY > dragStartPosY+25 || dragCurrPosY < dragStartPosY-25){
                    isMove = true;
                }
                isDragging = false;
                if (wasDragging && isMove) {
                    callVisualEditors(elem);
                }
            });
        }
    }

    QTags.addButton( 'ed_span', 'span', '<span>', '</span>', 'h', 'span', 1 );
    //call dynamic visual editors
    function callVisualEditors(elem){
        var editors;
        if(elem){
            var elemId = $(elem).attr('id');
            if(elemId){
                elemId = '#'+elemId;
                editors = $(elemId).find('.addEditor');
            }
            else{
                editors = $(elem).find('.addEditor');
            }
        }
        else{
            editors = $('.addEditor');
        }

        if(editors.length){
            editors.each(function (k, v) {
                if($(v).attr('id') && $(v).attr('id').indexOf('[!num!]')<0  && $(v).attr('id').indexOf('__i__')<0){

                    addadEditors.push($(v).attr('id'));

                    var id = $(v).attr('id');
                    if (tinymce.get(id)){
                        wp.editor.remove(id);
                    }

                    var toolbar1 = 'formatselect,|,bold,italic,bullist,numlist,link,unlink,forecolor';
                    var buttons = 'link';
                    //reinit editor on widget dragged
                    if(elem){
                        setTimeout(function(){
                            initEditor(id, toolbar1, buttons);
                        }, 500);
                    }
                    else{//reinit editors
                        initEditor(id, toolbar1, buttons);
                    }

                }

            });
        }
    }

    function initEditor(id, toolbar, buttons){
        wp.editor.initialize(id, {
            tinymce: {
                wpautop: false,
                teeny: true,
                height : 100,
                toolbar1: toolbar,
                media_buttons: true,
                mediaButtons: true,
                content_css: '/wp-content/themes/revocanda/css/editor-style.css'
            },
            quicktags: {
                buttons: buttons
            }
        });
    }

    $(document).on('tinymce-editor-setup', function(event, editor) {

        if(addadEditors.length){
            $(addadEditors).each(function (k, v) {

                if(editor.id === v){

                    editor.on('change', function(e) {
                        this.save();
                        var textarea = $('#'+$(this)[0].id);
                        textarea.trigger( 'change' );
                    });

                    if(editor.settings.toolbar1.indexOf('mediaButton')<0){
                        editor.settings.toolbar1 += ',mediaButton';
                        editor.addButton('mediaButton', {
                            text: '',
                            icon: 'image',
                            tooltip: "img",
                            onclick: function () {
                                mediaUploaderEditor(editor);
                            }
                        });
                    }

                }

            });
        }

    });

    //media library calling for editor
    function mediaUploaderEditor(editor){
        var mediaUploader = wp.media.frames.file_frame = wp.media({
            frame: 'post',
            title: 'Выбрать файл',
            button: {
                text: 'Выбрать файл'
            },
            multiple: true,
            library: {
                type: ['image', 'video']
            }
        });
        mediaUploader.on('insert', function(selected){
            var attachments = mediaUploader.state().get('selection').toJSON();
            mediaUploader.reset();
            var currSize = mediaUploader.state()._defaultDisplaySettings.size;
            if(attachments.length){
                $(attachments).each(function(k, attachment){
                    var src = '';
                    var height = '';
                    var width = '';
                    var size = '';
                    if(attachment.type == 'image'){
                        if(attachment.sizes.thumbnail && currSize == 'thumbnail'){
                            src = attachment.sizes.thumbnail.url.replace(/^.*\/\/[^\/]+/, '');
                            height = attachment.sizes.thumbnail.height;
                            width = attachment.sizes.thumbnail.width;
                            size = ' size-thumbnail';
                        }
                        else if(attachment.sizes.medium && currSize == 'medium'){
                            src = attachment.sizes.medium.url.replace(/^.*\/\/[^\/]+/, '');
                            height = attachment.sizes.medium.height;
                            width = attachment.sizes.medium.width;
                            size = ' size-medium';
                        }
                        else if(attachment.sizes.large && currSize == 'large'){
                            src = attachment.sizes.large.url.replace(/^.*\/\/[^\/]+/, '');
                            height = attachment.sizes.large.height;
                            width = attachment.sizes.large.width;
                            size = ' size-large';
                        }
                        else{
                            src = attachment.sizes.full.url.replace(/^.*\/\/[^\/]+/, '');
                            height = attachment.sizes.full.height;
                            width = attachment.sizes.full.width;
                            size = ' size-full';
                        }
                        var img = '<img class="alignnone'+size+' wp-image-38" src="'+src+'" alt="" width="'+width+'" height="'+height+'" />';
                        if(editor){
                            editor.insertContent(img);
                        }
                    }
                    else if(attachment.type == 'video'){
                        //sand ajax to proceed shortcode, insert response into editor
                        //remove shortcode execution in the backend before saving field
                        var width = '378';
                        var raito = Math.round(attachment.width/attachment.height);
                        var height = Math.round(width/raito);
                        var src = attachment.url.replace(/^.*\/\/[^\/]+/, '');
                        var code = '[video width="'+width+'px" height="'+height+'px" mp4="'+src+'"][/video]';
                        getShortcode(code, editor);
                    }
                    else{
                        //files shortcode insert
                    }
                });
            }
        });
        mediaUploader.open();
    }

    //replace shortcode in editor
    function getShortcode(code, editor) {
        var action = 'revocanda_get_shortcode';
        //var url = '/ajax_post_content.php';
        var url = '/wp-admin/admin-ajax.php';
        $.ajax({
            url: url,
            type: 'post',
            async: false,
            data: {
                data: code,
                action: action
            },
            error: function(response){

            },
            success: function(response){
                if(response){
                    if(editor){
                        editor.insertContent(response);
                    }
                    //add styles and script to editor's iframe
                }
            }
        });
    }

    $('.thickboxClick').click();

    /*Dynamic blocks*/

    //toggle show/hide for dynamic items
    $(document).on('click', '.block-toggle, .top-wrap', function(e){
        e.stopPropagation();
        var parent = $(this).parents('.dynamic-fields-block');
        var curr = null;
        if($(this).hasClass('block-toggle')){
            curr = $(this);
        }
        else{
            curr = $(this).find('> .block-toggle');
        }
        var cont = parent.find('.content-wrap');
        $(cont).toggle("fast");
        //rorate the arrow
        if(curr.hasClass('opened')){
            curr.css('transform', 'rotate(0deg)');
            curr.removeClass('opened');
        }
        else{
            curr.css('transform', 'rotate(180deg)');
            curr.addClass('opened');
        }
    });

    //add a new dynamic item
    function addNewItem(currElem){
        var area = $(currElem).siblings('.dynamic-fields-blocks');
        var temp = $(currElem).siblings('.fieldsTemplate');
        var blocks = area.find('.dynamic-fields-block-wrap');
        var num = 0;
        if(blocks.length){
            num = blocks.length;
        }
        var tempHtml = getItemTemplate(temp, ++num);
        if(tempHtml){
            area.append(tempHtml);
        }

        //call new editor for added item
        callVisualEditors(area);

        //call new drop down images list for added item
        var ddId = 'dropdown_contact_icon_'+num;
        var ddArgs = ddData[ddId];
        callDropDown(ddArgs, ddId);
    }

    //remove dynamic fields block
    function removeItem(currElem){
        var answer = confirm('Remove this Block?');
        if(answer == true && currElem){

            var parentElem = $(currElem).parents('.widget-content');

            var block = $(currElem).parents('.dynamic-fields-block-wrap');
            block.remove();

            prepareDynFieldsData(parentElem);
            parentElem.find('input, textarea').eq(0).trigger( 'change' );
        }
        return;
    }

    //on change, setup view of dynamic area
    $('.dynamic-fields-blocks').bind("DOMSubtreeModified", function(){
        var elem = $(this);
        setupDynAreaView(elem);
    });
    //setup view of dynamic area
    function setupDynAreaView(elem){
        if(elem){
            if(elem.html()){
                elem.css('border', '1px solid #e5e5e5');
                elem.css('padding', '10px');
            }
            else{
                elem.css('border', 'none');
                elem.css('padding', '0px');
            }
        }
        else{
            $('.dynamic-fields-blocks').each(function(k, v){
                var elem = $(v);
                if(elem.html()){
                    elem.css('border', '1px solid #e5e5e5');
                    elem.css('padding', '10px');
                }
            })
        }
    }
    setupDynAreaView();

    //sort dynamic fields blocks
    function sortDynBlocks(block, direct, blocks){
        if(block && direct){
            var wrap = $(block).parent();
            var replaceWrap = null;
            if(direct == 'up'){
                replaceWrap = wrap.prev();
            }
            else if(direct == 'down'){
                replaceWrap = wrap.next();
            }
            if(replaceWrap){
                wrap.find('textarea.datafield').each(function(){
                    $(this).html($(this).val());
                });
                wrap.find('.datafield').each(function(){
                    $(this).attr('value', $(this).val());
                });
                replaceWrap.find('textarea.datafield').each(function(){
                    $(this).html($(this).val());
                });
                replaceWrap.find('input').each(function(){
                    $(this).attr('value', $(this).val());
                });
                var wrapData = wrap.html();
                var replaceWrapData = replaceWrap.html();
                replaceWrap.html(wrapData);
                replaceWrap.data('num', wrap.data('num'));
                wrap.html(replaceWrapData);
                wrap.data('num', replaceWrap.data('num'));
            }
            callVisualEditors(blocks);
            prepareDynFieldsData(blocks);
            $(blocks).find('input, textarea').eq(0).trigger('change');
        }
    }

    //serialize data of each dynamic fields areas
    function prepareDynFieldsData(caller){
        if(caller){
            var widgetForm = $(caller).parents('form');
            var dynFieldsAras = widgetForm.find('.dynamic-fields');
            if(dynFieldsAras.length>0){
                $.each(dynFieldsAras, function (key, dynFieldsArea){
                    var hiddenVal = $(dynFieldsArea).find('.hidden-input-data');
                    var dynFieldsBlocks = $(dynFieldsArea).find('.dynamic-fields-blocks .dynamic-fields-block-wrap');
                    if(dynFieldsBlocks.length>0){
                        var num = 0;
                        var itemFieldsArray = [];
                        $.each(dynFieldsBlocks, function (key2, dynFieldsBlock){
                            var fields = $(dynFieldsBlock).find('.datafield');
                            itemFieldsArray[num] = [];
                            if(fields.length){
                                var valArr = {};
                                $.each(fields, function (key3, field){
                                    var name = $(field).data('name');
                                    if($(field).attr('type')=='checkbox'){
                                        if(field.checked){
                                            valArr[name] = '1';
                                        }
                                        else{
                                            valArr[name] = '';
                                        }
                                    }
                                    else if($(field).attr('type')=='radio'){
                                        if(field.checked){
                                            valArr[name] = $(field).val();
                                        }
                                        else{
                                            valArr[name] = '';
                                        }
                                    }
                                    else{
                                        valArr[name] = $(field).val();
                                    }
                                });
                                itemFieldsArray[num] = valArr;
                            }
                            num++;
                        });
                        var jsonData = JSON.stringify(itemFieldsArray);
                        hiddenVal.val(jsonData);
                    }
                    else{
                        hiddenVal.val('');
                    }
                });
            }
        }
    }

    //get template for dynamic fields block
    function getItemTemplate(item, itemNum){
        if(item && itemNum){
            var temp = item.html();
            var find = '[!num!]';
            if(temp){
                temp = getItemId(temp, itemNum);
                return replaceInString(temp, find, itemNum);
            }
        }
        return false;
    }

    //replace id in template
    function getItemId(temp, itemNum){
        var find = '[!id!]';
        var datetime = Date.now();
        itemNum = itemNum+datetime+'';
        itemNum = itemNum.hashCode();
        if(itemNum<0){
            itemNum = itemNum-(itemNum*2);
        }
        return replaceInString(temp, find, itemNum);
    }

    //multiple replace num in string
    function replaceInString(str, find, replace) {
        var res = str.replace(find, replace);
        if(res.indexOf(find) !== -1){
            res = replaceInString(res, find, replace)
        }
        return res;
    }

    //add new dynamic fields block on click
    $(document).on('click', '.add-block', function(){
        addNewItem(this);
    });

    //remove dynamic fields block on click
    $(document).on('click', '.remove-block', function(e){
        e.stopPropagation();
        removeItem(this);
    });

    //sort dynamic fields block on click
    $(document).on('click', '.change-pos .dashicons', function(e){
        e.stopPropagation();
        var direct = $(this).data('act');
        var block = $(this).parents('.dynamic-fields-block');
        var blocks = $(this).parents('.dynamic-fields-blocks');
        sortDynBlocks(block, direct, blocks);

    });

    $(document).on('mousedown', '#publish, #btnSubmit', function(e){
        prepareDynFieldsData(this);
    });

    $(document).on('change', '.dynamic-fields-block', function(){
        prepareDynFieldsData(this);
    });

    //call dropdown images list
    if($('.Dropdown').length){
        $('.Dropdown').each(function(k, v){
            var id = $(v).attr('id');
            if(id && id.indexOf("[!num!]")<0){
                var args = ddData[id];
                callDropDown(args, id);
            }
        });
    }

    //drop down spoiler
    $(document).on('click', '.dropDownCont-panel-heading', function(e){
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);
        if(!$this.find('.dropDownCont-clickable').hasClass('dropDownCont-panel-collapsed')) {
            $this.parents('.dropDownCont-panel').find('.dropDownCont-panel-body').slideUp();
            $this.find('.dropDownCont-clickable').addClass('dropDownCont-panel-collapsed');
            $this.find('i').removeClass('dashicons-arrow-up').addClass('dashicons-arrow-down');
        } else {
            $this.parents('.dropDownCont-panel').find('.dropDownCont-panel-body').slideDown();
            $this.find('.dropDownCont-clickable').removeClass('dropDownCont-panel-collapsed');
            $this.find('i').removeClass('dashicons-arrow-down').addClass('dashicons-arrow-up');
        }
    })


    $('.selectList').on('change', function(){
        getSelectList(this);
    });
    //get select list by ajax
    function getSelectList(select) {
        if(select.length){
            select = $(select);
            var connected = select.data('connected');
            var call = select.data('call');
            var value = select.data('value');
            var action = 'revocanda_get_select_list';
            var url = '/wp-admin/admin-ajax.php';
            var code = {
                connected: connected,
                call: call
            }
            if(value){
                code.value = select.val();
            }
            else{
                code.value = null;
            }
            $.ajax({
                url: url,
                type: 'post',
                async: false,
                data: {
                    data: code,
                    action: action
                },
                error: function(response){

                },
                success: function(response){
                    if(response){
                        var responseA = JSON.parse(response);
                        var resConnected = responseA.connected;
                        var resData = responseA.data;
                        if(resConnected){
                            var elem = $('#'+resConnected);
                            if(elem.length){
                                var first = elem.children()[0];
                                elem.html('');
                                if(first){
                                    elem.append(first);
                                }
                                $.each(resData, function (k, v) {
                                    elem.append($("<option></option>").attr("value",k).text(v));
                                });
                            }
                        }
                    }
                }
            });

        }
    }

    if($('.datePicker').length){
        $('.datePicker').datepicker({
            dateFormat : 'dd.mm.yy'
        });
    }

    if($('.colorPicker').length){
        $('.colorPicker').each(function(k, v){
            $(v).wpColorPicker();
        });

    }

    $('.multiselect').on('change', function () {

    });

    //set left offset for opened widget
    $(document).on('click', '.widgets-holder-wrap .widget .widget-top', function(e){
        e.stopPropagation();
        var elem = $(this).parent();
        var opened = elem.find('.widget-inside').css('display')=='block'?true:false;
        if(opened && elem.hasClass('open')){
            elem.css('z-index', 'auto');
            elem.animate({
                marginLeft: '0px'
            }, 100);
        }
        else if(opened && !elem.hasClass('open')){
            var position = elem.offset().left;
            var offsetToLeft = 122;
            if(position<offsetToLeft){
                offsetToLeft = position;
            };
            elem.css('z-index', 9999);
            elem.animate({
                marginLeft: '-'+offsetToLeft+'px'
            }, 100);
        }
    });


});
//add function for string hash
String.prototype.hashCode = function() {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
//if old browser add support for Date.now
if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}

//dropdown images list
function callDropDown(args, elementId){
    if(args && elementId && elementId.indexOf("[!num!]")<0){
        var elem = jQuery('#'+elementId);
        if(elem.length){
            elem.ddslick({
                data: args,
                width: 300,
                imagePosition:"right",
                onSelected: function(selectedData){
                    var inputData = jQuery('#'+elementId+'_datafield');
                    jQuery(inputData).val(selectedData.selectedData.imageSrc);
                }
            });
        }
    }
}

//calling popup to choose link and insert its into the field. func takes field id
function callWpLinkPicker(field_id){
    if(jQuery(field_id).length){
        wpLink.open(jQuery(field_id).attr('id'));
    }
    else if(jQuery('#'+field_id).length){
        wpLink.open(field_id);
    }
}