<?php get_header(); ?>

<?php if(function_exists('dynamic_sidebar') && is_active_sidebar('main_content')): ?>
    <?php dynamic_sidebar('main_content') ?>
<?php endif; ?>

<?php get_footer(); ?>
