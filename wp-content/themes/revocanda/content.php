<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage revocanda
 */

global $isMobile;

$term = @wp_get_post_terms($post->ID, 'category')[0];

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>
<!--begin section-intro-->
<section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-box">
                    <div class="section-head-box">
							<span class="section-head-back">
								<span><?php echo $term->name; ?></span>
							</span>
                        <p><?= get_the_title(); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-intro-->

<!--begin section-item-page-->
<section class="section-about-us-page">
    <div class="container">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?= do_shortcode(get_the_content()); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-item-page-->

