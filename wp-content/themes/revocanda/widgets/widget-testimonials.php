<?php
class RevocandaTestimonialsWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-testimonials-widget', __('Testimonials | Revocanda', 'revocanda'),
            ['description' => __('Testimonials', 'revocanda')]);
    }

    public function form($instance) {
        //static fields
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            //static fields
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        //static fields
        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {
            ?>

            <!--begin section-testimonials-->
            <section class="section-testimonials"<?= $sectionId; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 wow fadeInUp">
                            <div class="section-head-box">
                                <span class="section-head-back"><span>отзывы</span></span>
                                <h2 class="section-head-title"><span>отзывы</span></h2>
                            </div>
                            <div class="slider-testimonials">

                                <?php
                                $args = [
                                    'posts_per_page'   => 10,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'feedbacks',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'author'	   => '',
                                    'author_name'	   => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true,
                                    'meta_query' => array(
                                        array(
                                            'key' => '_feedbacks_reviews_text_value_key',
                                            'compare' => '!=',
                                            'value' => ''
                                        )
                                    )
                                ];
                                $posts_array = get_posts( $args );
                                ?>
                                <?php if(count($posts_array)>0): ?>
                                    <?php foreach($posts_array as $post): ?>
                                        <?php
                                        $name = get_post_meta($post->ID, '_feedbacks_reviews_name_value_key', true);
                                        $age = get_post_meta($post->ID, '_feedbacks_reviews_age_value_key', true);
                                        $text = get_post_meta($post->ID, '_feedbacks_reviews_text_value_key', true);
                                        $text = wp_trim_words( $text, 70, '...' )
                                        ?>
                                        <?php if($name && $text): ?>

                                            <div class="item">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <span><b><?= $name; ?></b></span>
                                                        <span><?= $age; ?></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <p><?= $text; ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </div>
                            <a onclick="ga('send', 'event', 'button4', 'reviews')" href="/feedbacks?type=reviews" class="main-button blue-button to-right"><span>все отзывы</span></a>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section-testimonials-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaTestimonialsWidget");
});