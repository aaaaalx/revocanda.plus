<?php
class RevocandaGalleryWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-gallery-widget', __('Gallery | Revocanda', 'revocanda'),
            ['description' => __('Gallery', 'revocanda')]);
    }

    public function form($instance) {
        //static fields
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            //static fields
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        //static fields
        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-gallery-->
            <section class="section-gallery"<?= $sectionId; ?>>
                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 wow fadeInUp">
                            <div class="section-head-box">
                                <span class="section-head-back"><span>фотографии</span></span>
                                <h2 class="section-head-title"><span>наши фото</span></h2>
                            </div>
                            <div class="slider-gallery">

                                <?php
                                $args = [
                                    'posts_per_page'   => -1,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'gallery',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'author'	   => '',
                                    'author_name'	   => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true
                                ];
                                $posts_array = get_posts( $args );

                                $galleries = [];

                                if(count($posts_array)>0){
                                    foreach ($posts_array as $post){

                                        $pattern = get_shortcode_regex();
                                        preg_match_all( '/'. $pattern .'|(<img[^>]+>)/s', $post->post_content, $matches );

                                        $matches = array_map('array_filter', $matches);
                                        $matches = array_filter($matches);

                                        if (is_array($matches) && isset($matches[0])) {
                                            foreach ($matches[0] as $k =>$shortcode){
                                                if(isset($matches[2]) && isset($matches[2][$k])){//if gallery
                                                    if($matches[2][$k] == 'gallery'){
                                                        $galleries[$post->ID][$k]['shortcode'] = $shortcode;
                                                        $galleries[$post->ID][$k]['type'] = 'gallery';
                                                        if(isset($matches[3][$k])){
                                                            $galleries[$post->ID][$k]['attrs'] = $matches[3][$k];
                                                        }
                                                    }
                                                }
                                                elseif (isset($matches[7]) && strpos($shortcode, '<img')>-1){//if image add to gallery array
                                                    $galleries[$post->ID][$k]['shortcode'] = $shortcode;
                                                    $galleries[$post->ID][$k]['type'] = 'img';
                                                }
                                            }
                                        }

                                    }
                                }
                                $galleries = array_filter($galleries);
                                ?>

                                <?php
                                $pattern = '/src="([^"]*)"';//img src
                                $pattern .= '|ids="([^"]*)"/s';//gallery ids
                                $links = [];
                                ?>
                                <?php if(count($galleries)>0): ?>
                                    <?php foreach($galleries as $gKey => $post_gals): ?>
                                        <?php foreach($post_gals as $gal): ?>

                                            <?php
                                            preg_match($pattern, $gal['shortcode'], $matches);
                                            $matches = array_values(array_filter($matches));
                                            if($gal['type'] == 'gallery'){
                                                $g = explode(',', $matches[1]);
                                                if (count($g)>0){
                                                    foreach ($g as $gId){

                                                        $url = wp_get_attachment_url($gId);
                                                        $image = get_thumbnails_by_id($gId, 'block-small-image');
                                                        $image = $image?$image:$url;

                                                        $date =  get_the_date('m.d.Y', $gKey);
                                                        $location =  get_post_meta($gKey, '_gallery_location_value_key', true);
                                                        $title = get_the_title($gId);
                                                        $links[] = ['title'=>$title,'url'=>$url,'thumbnails'=>$image,'date'=>$date,'location'=>$location];
                                                    }
                                                }
                                            }
                                            elseif($gal['type'] == 'img'){

                                                $url = $matches[1];
                                                $gId = pippin_get_file_id($url);
                                                $image = get_thumbnails_by_id($gId, 'block-small-image');
                                                $image = $image?$image:$url;

                                                if($gId){
                                                    $date = get_the_date('m.d.Y', $gKey);
                                                    $location = get_post_meta($gKey, '_gallery_location_value_key', true);
                                                    $title = get_the_title($gId);
                                                    $links[] = ['title'=>$title,'url'=>$url,'thumbnails'=>$image,'date'=>$date,'location'=>$location];
                                                }
                                                else{
                                                    $date = get_the_date('m.d.Y', $gKey);
                                                    $location = get_post_meta($gKey, '_gallery_location_value_key', true);
                                                    $title = get_the_title($gKey);
                                                    $links[] = ['title'=>$title,'url'=>$url,'thumbnails'=>$image,'date'=>$date,'location'=>$location];
                                                }
                                            }
                                            ?>

                                        <?php endforeach; ?>
                                    <?php endforeach; ?>

                                    <?php if(count($links)>0): ?>
                                        <?php for ($i=0; $i<6; $i++): ?>

                                            <div class="item">
                                                <a href="<?= $links[$i]['url']; ?>" target="_blank" class="thumbnail">
                                                    <img src="<?= $links[$i]['thumbnails']; ?>" alt="">
                                                </a>
                                            </div>

                                        <?php endfor; ?>
                                    <?php endif; ?>

                                <?php endif; ?>

                            </div>
                            <a onclick="ga('send', 'event', 'button3', 'photo')" href="/gallery" class="main-button blue-button to-right">все фото</a>
                        </div>

                    </div>
                </div>
            </section>
            <!--end section-gallery-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaGalleryWidget");
});