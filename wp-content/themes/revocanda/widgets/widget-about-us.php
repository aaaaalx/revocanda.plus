<?php
class RevocandaAboutUsWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-about-us-widget', __('About Us | Revocanda', 'revocanda'),
           ['description' => __('Useful Information', 'revocanda')]);
    }

    public function form($instance) {
            $title = '';
            $desc = '';
            $linkTitle = '';
            $linkUrl = '';
            $background = '';
            $sectionId = '';
            $status = '';

            if (!empty($instance)) {
                $title = esc_attr($instance['title']);
                $desc = do_shortcode(esc_attr($instance['desc']));
                $linkTitle = esc_attr($instance['linkTitle']);
                $linkUrl = esc_attr($instance['linkUrl']);
                $background = esc_attr($instance['background']);
                $sectionId = esc_attr($instance['sectionId']);
                $status = esc_attr($instance['status']);
            }

            $Id = $this->get_field_id('title');
            $Name = $this->get_field_name('title');
            echo '<p><label for="' . $Id . '">' . __('Title', 'revocanda') . ': </label>';
            echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $title . '"></p>';

            $Id = $this->get_field_id('desc');
            $Name = $this->get_field_name('desc');
            echo '<label for="' . $Id . '">'.__( 'Description', 'revocanda' ).': </label>';
            echo '<textarea class="widefat" rows="6" id="' . $Id . '" name="' . $Name . '">' . $desc . '</textarea></p>';

            $Id = $this->get_field_id('linkTitle');
            $Name = $this->get_field_name('linkTitle');
            echo '<label for="' . $Id . '">' . __('Link title', 'revocanda') . ': </label>';
            echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $linkTitle . '"></p>';

            $Id = $this->get_field_id('linkUrl');
            $Name = $this->get_field_name('linkUrl');
            echo '<label for="' . $Id . '">' . __('Link url', 'revocanda') . ': </label>';
            echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $linkUrl . '"></p>';

            $Id = $this->get_field_id('background');
            $Name = $this->get_field_name('background');
            echo '<p><label for="' . $Id . '">Изображение: </label>';
            echo '<div class="widget-block-img">';
            if($background){

                $mediaId = pippin_get_file_id($background);
                $selectedMedia = '';
                if($mediaId){
                    $selectedMedia = '<script>';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');';
                    $selectedMedia .= '</script>';
                }
                add_thickbox();
                echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
                echo $selectedMedia;

            }
            echo '</div>';
            echo '<input data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
            echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
            echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
            echo '</p>';

            $Id = $this->get_field_id('sectionId');
            $Name = $this->get_field_name('sectionId');
            echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
            echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

            $Id = $this->get_field_id('status');
            $Name = $this->get_field_name('status');
            echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
            $checked = '';
            if($status == 1){
                $checked = ' checked';
            }
            echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['desc'] = htmlentities($newInstance['desc']);
        $values['linkTitle'] = htmlentities($newInstance['linkTitle']);
        $values['linkUrl'] = htmlentities($newInstance['linkUrl']);
        $values['background'] =  htmlentities($newInstance['background']);
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $title = $instance['title'];
        $desc = do_shortcode($instance['desc']);
        $linkTitle = $instance['linkTitle'];
        $linkUrl = $instance['linkUrl'];
        $background = $instance['background'];
        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-about-us-->
            <section class="section-about-us"<?= $sectionId; ?>>
                <div class="container">
                    <div class="row">
                        <?php if($background): ?>
                        <?php
                        $gId = pippin_get_file_id($background);
                        $image = get_thumbnails_by_id($gId, 'block-large-image');
                        $image = $image?$image:$background;
                        ?>
                        <div class="col-md-7 col-md-push-5 col-xs-12 wow fadeInRight">
                            <img src="<?= $image; ?>" alt="">
                        </div>
                        <?php endif; ?>
                        <div class="col-md-5 col-md-pull-7 col-xs-12 wow fadeInLeft">
                            <div class="section-head-box">
                                <span class="section-head-back"><span>revocanda +</span></span>
                                <h2 class="section-head-title"><span><?= $title; ?></span></h2>
                            </div>
                            <p><?= $desc; ?></p>
                            <?php if($linkTitle && $linkUrl): ?>
                            <a href="<?= $linkUrl; ?>" class="main-button blue-button to-left"><span><?= $linkTitle; ?></span></a>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </section >
            <!--end section-about-us-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaAboutUsWidget");
});