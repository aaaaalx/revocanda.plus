<?php
class RevocandaServicesWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-services-widget', __('Services | Revocanda', 'revocanda'),
            ['description' => __('Services', 'revocanda')]);
    }

    public function form($instance) {

        //static fields
        $services = [];
        $buttonText = '';
        $buttonLink = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {

            //static fields
            $services = $instance['services'];
            $buttonText = $instance['buttonText'];
            $buttonLink = $instance['buttonLink'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $args = array(
            'sort_order' => 'asc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish'
        );
        $pages = get_pages($args);

        $Id = $this->get_field_id('services');
        $Name = $this->get_field_name('services');
        echo '<p><label for="' . $Id . '">Страницы: </label>';
        echo '<select style="height: 150px;" class="widefat multiselect" id="'.$Id.'" multiple="multiple" name="'.$Name.'[]">';
        if(count($pages)>0){
            echo '<option value=""></option>';
            foreach ($pages as $page){
                $selected = '';
                if(count($services)>0){
                    foreach ($services as $v){
                        if((int)$v == $page->ID){
                            $selected = ' selected';
                            break;
                        }
                    }
                }
                echo '<option value="'.$page->ID.'"'.$selected.'>'.$page->post_title.'</option>';
            }
        }
        echo '</select>';

        $Id = $this->get_field_id('buttonText');
        $Name = $this->get_field_name('buttonText');
        echo '<p><label for="' . $Id . '">Текст кнопки: </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $buttonText . '"></p>';

        $Id = $this->get_field_id('buttonLink');
        $Name = $this->get_field_name('buttonLink');
        echo '<p><label for="' . $Id . '">Ссылка кнопки: </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $buttonLink . '"></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['services'] = $newInstance['services'];
        $values['buttonText'] = $newInstance['buttonText'];
        $values['buttonLink'] = $newInstance['buttonLink'];
        $values['button'] = $newInstance['button'];
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        global $isMobile;

        extract($args);
        //static fields
        $services = $instance['services'];
        $buttonText = $instance['buttonText'];
        $buttonLink = $instance['buttonLink'];

        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <section class="section-slider section-slider-services"<?= $sectionId; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 wow fadeInUp">
                            <div class="slider-our-services">

                                <?php if(count($services)>0): ?>
                                <?php foreach($services as $k => $post_id): ?>

                                <?php
                                $post = get_post($post_id);
                                $image = false;
                                if(!$isMobile){
                                    $image = get_the_post_thumbnail_url($post->ID, 'block-large-image');
                                    $image = $image?$image:get_stylesheet_directory_uri().'/img/No-photo_480x640.jpg';
                                }
                                $title = get_the_title($post_id);
                                ++$k;
                                ?>

                                <div class="item">
                                    <div class="row">
                                        <?php if($image && !$isMobile): ?>
                                        <div class="col-md-5 hidden-sm hidden-xs">
                                            <img src="<?= $image; ?>" alt="">
                                        </div>
                                        <?php endif; ?>
                                        <div class="col-md-7 col-xs-12">
                                            <div class="slider-content-box">
                                                <span><?php $k; ?></span>
                                                <h2><?= $title; ?></h2>
                                                <p><?= wp_trim_words( strip_shortcodes($post->post_content), 50, '...' ); ?></p>
                                                <a href="<?= get_permalink($post_id); ?>" class="more-toggle"><span>подробнее</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php endforeach; ?>
                                <?php endif; ?>

                            </div>
                            <?php if($buttonText && $buttonLink): ?>
                            <a href="<?= $buttonLink; ?>" class="main-button blue-button to-right"><span><?= $buttonText; ?></span></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaServicesWidget");
});