<?php
class RevocandaPartnersWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('Partners-widget', __('Partners | Revocanda', 'revocanda'),
           ['description' => __('Partners information', 'revocanda')]);
    }

    public function form($instance) {
        $sectionTitle = '';
        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = $instance['sectionTitle'];
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section title', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><media [title="Image"/] [name="img"/] /></p>';
        $pattern .= '<p><textarea [class="widefat datafield linkOnly getLinksList"/] [title="Link"/] [name="link"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', 'revocanda'), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['sectionTitle'] = htmlentities($newInstance['sectionTitle']);

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);

        $sectionTitle = $instance['sectionTitle'];

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            ?>

            <section<?= $sectionId; ?> class="section-base section-4-items-slider">
                <div class="container">
                    <div class="row">
                        <?php if($sectionTitle): ?>
                        <div class="col-xs-12">
                            <div class="section-head-box">
                                <span class="section-head-back"><span>партнеры</span></span>
                                <h2 class="section-head-title"><span><?= $sectionTitle; ?></span></h2>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-xs-12">
                            <div class="slider-4-items partnersSlider">
                            <?php if(count($dynamicFieldsArr)>0): ?>
                                <?php foreach($dynamicFieldsArr as $img): ?>
                                <div class="item">
                                    <?php if($img['link']): ?>
                                    <?php
                                    $target = ' target="_blank"';
                                    $itemTitle = @$img['title']?' title="'.$img['title'].'"':'';
                                    ?>
                                    <a href="<?= $img['link']; ?>"<?= $target.$itemTitle; ?>>
                                    <?php endif; ?>
                                        <?php
                                        $gId = pippin_get_file_id($img['img']);
                                        if(!$gId){
                                            $gId =  get_attachment_file_id($img['img']);
                                        }
                                        $imageSmall = get_thumbnails_by_id($gId, 'licenses-image-soft');
                                        $imageSmall = $imageSmall?$imageSmall:$img['img'];
                                        ?>
                                        <img src="<?= $imageSmall; ?>" alt="">
                                    <?php if($img['link']): ?>
                                    </a>
                                    <?php endif; ?>
                                </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaPartnersWidget");
});