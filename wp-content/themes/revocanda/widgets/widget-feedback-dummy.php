<?php
class RevocandaFeedbackDummyWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-feedback-dummy-widget', __('Feedback (dummy) | Revocanda', 'revocanda'),
            ['description' => __('Feedback', 'revocanda')]);
    }

    public function form($instance) {
        //static fields
        $title = '';
        $desc = '';
        $buttonText = '';
        $buttonLink = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            //static fields
            $title = esc_attr($instance['title']);
            $desc = esc_attr($instance['desc']);
            $buttonText = $instance['buttonText'];
            $buttonLink = $instance['buttonLink'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields
        $Id = $this->get_field_id('title');
        $Name = $this->get_field_name('title');
        echo '<p><label for="' . $Id . '">' . __('Title', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $title . '"></p>';

        $Id = $this->get_field_id('desc');
        $Name = $this->get_field_name('desc');
        echo '<p><label for="' . $Id . '">' . __('Description', 'revocanda') . ': </label>';
        echo '<textarea rows=3 class="widefat" id="' . $Id . '" type="text" name="' . $Name . '">' . $desc . '</textarea></p>';

        $Id = $this->get_field_id('buttonText');
        $Name = $this->get_field_name('buttonText');
        echo '<p><label for="' . $Id . '">Текст кнопки: </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $buttonText . '"></p>';

        $Id = $this->get_field_id('buttonLink');
        $Name = $this->get_field_name('buttonLink');
        echo '<p><label for="' . $Id . '">Ссылка кнопки: </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $buttonLink . '"></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['title'] = $newInstance['title'];
        $values['desc'] = $newInstance['desc'];
        $values['buttonText'] = $newInstance['buttonText'];
        $values['buttonLink'] = $newInstance['buttonLink'];
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        //static fields
        $title = esc_attr($instance['title']);
        $desc = esc_attr($instance['desc']);
        $buttonText = esc_attr($instance['buttonText']);
        $buttonLink = esc_attr($instance['buttonLink']);

        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>
                <!--begin section-feedback-->
                <section class="section-feedback"<?= $sectionId; ?>>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7  wow fadeIn">
                                <h2><?= $title; ?></h2>
                                <p><?= $desc; ?></p>
                            </div>
                            <?php if($buttonText && $buttonLink): ?>
                            <div class="col-md-5  wow fadeIn">
                                <a href="<?= $buttonLink; ?>" class="main-button red-button to-right"><?= $buttonText; ?></a>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <!--end section-feedback-->
            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaFeedbackDummyWidget");
});