<?php
class RevocandaMainInfoWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-main-info-widget', __('Main Info | Revocanda', 'revocanda'),
           ['description' => __('Main information', 'revocanda')]);
    }

    public function form($instance) {
            $title = '';
            $desc = '';
            $linkUrl = '';
            $linkTarget = '';
            $background = '';
            $video = '';
            $sectionId = '';
            $status = '';

            if (!empty($instance)) {
                $title = esc_attr($instance['title']);
                $desc = do_shortcode(esc_attr($instance['desc']));
                $linkUrl = esc_attr($instance['linkUrl']);
                $linkTarget = esc_attr($instance['linkTarget']);
                $background = esc_attr($instance['background']);
                $video = esc_attr($instance['video']);
                $sectionId = esc_attr($instance['sectionId']);
                $status = esc_attr($instance['status']);
            }

            $Id = $this->get_field_id('title');
            $Name = $this->get_field_name('title');
            echo '<p><label for="' . $Id . '">' . __('Title', 'revocanda') . ': </label>';
            echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $title . '"></p>';

            $Id = $this->get_field_id('desc');
            $Name = $this->get_field_name('desc');
            echo '<label for="' . $Id . '">'.__( 'Description', 'revocanda' ).': </label>';
            echo '<textarea class="widefat" rows="6" id="' . $Id . '" name="' . $Name . '">' . $desc . '</textarea></p>';

            $Id = $this->get_field_id('linkUrl');
            $Name = $this->get_field_name('linkUrl');
            echo '<label for="' . $Id . '">' . __('Link url', 'revocanda') . ': </label>';
            echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $linkUrl . '"></p>';

            $Id = $this->get_field_id('linkTarget');
            $Name = $this->get_field_name('linkTarget');
            echo '<p><label for="' . $Id . '">' . __('Open link in new window', 'revocanda') . ': </label>';
            $checked = '';
            if($linkTarget == 1){
                $checked = ' checked';
            }
            echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

            echo '<div>';
            $Id = $this->get_field_id('background');
            $Name = $this->get_field_name('background');
            echo '<p><label for="' . $Id . '">' . __('Background', 'revocanda') . ': </label>';
            echo '<div class="widget-block-img">';
            if($background){
                $backgroundMime = get_file_mime_type($background);
                if(getFileFormat($backgroundMime) == 'image'){
                    $mediaId = pippin_get_file_id($background);
                    $selectedMedia = '';
                    if($mediaId){
                        $selectedMedia = '<script>';
                        $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                        $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');';
                        $selectedMedia .= '</script>';
                    }
                    add_thickbox();
                    echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
                    echo $selectedMedia;
                }
            }
            echo '</div>';
            echo '<input data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
            echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
            echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
            echo '</p>';
            echo '</div>';

            echo '<div>';
            $Id = $this->get_field_id('video');
            $Name = $this->get_field_name('video');
            echo '<p><label for="' . $Id . '">' . __('Video', 'revocanda') . ': </label>';
            echo '<div class="widget-block-img">';
            if($video){
                $videoMime = get_file_mime_type($video);
                if(getFileFormat($videoMime) == 'video'){
                    echo '<video width="150" height="150" controls preload="metadata">';
                    echo '<source src="'.$video.'" type="'.$videoMime.'">';
                    echo '</video>';
                }
            }
            echo '</div>';
            echo '<input data-options="multiple:false,type:video" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
            echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$video.'" />';
            echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
            echo '</p>';
            echo '</div>';

            $Id = $this->get_field_id('sectionId');
            $Name = $this->get_field_name('sectionId');
            echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
            echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

            $Id = $this->get_field_id('status');
            $Name = $this->get_field_name('status');
            echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
            $checked = '';
            if($status == 1){
                $checked = ' checked';
            }
            echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['desc'] = htmlentities($newInstance['desc']);
        $values['linkUrl'] = htmlentities($newInstance['linkUrl']);
        $values['linkTarget'] = htmlentities($newInstance['linkTarget']);
        $values['background'] =  htmlentities($newInstance['background']);
        $values['video'] =  htmlentities($newInstance['video']);
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        global $isMobile;

        extract($args);
        $title = $instance['title'];
        $desc = do_shortcode($instance['desc']);
        $linkUrl = $instance['linkUrl'];
        $linkTarget = $instance['linkTarget'];
        $background = $instance['background'];
        $video = $instance['video'];
        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if($linkTarget){
            $linkTarget = ' target="_blank"';
        }
        else{
            $linkTarget = '';
        }

        if(count($instance)>0) {

            ?>

            <!--begin background video-->
            <section class="section-video">
                <?php if($video && !$isMobile): ?>
                <div class="vid-container background">
                    <video class="bgvid" autoplay="autoplay" muted="muted" preload="auto" loop>
                        <source src="<?= $video; ?>" type="video/mp4">
                    </video>
                </div>
                <?php endif; ?>
                <?php if($background): ?>
                    <?php
                    $style = '';
                    if(!$video || $isMobile){
                        $style = ' style="display: block;"';
                    }
                    if($isMobile){
                        $gId = pippin_get_file_id($background);
                        $image = get_thumbnails_by_id($gId, 'mobile-header');
                        $background = $image?$image:$background;
                    }
                    ?>
                <div class="bg-box"<?= $style; ?>>
                    <img src="<?= $background; ?>" alt="">
                </div>
                <?php endif; ?>
                <div class="overlay-box">
                </div>
            </section>
            <!--end section-video-->
            <!--begin section intro-->
            <section class="section-intro"<?= $sectionId; ?>>
                <div class="parallax-window" data-parallax="scroll">
                    <div class="inner-video-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <?php if($linkUrl): ?>
                                    <a href="<?= $linkUrl ; ?>" class="white-button scroll"<?= $linkTarget; ?>>
                                    <?php endif; ?>
                                        <h1 class=" wow fadeIn" data-wow-duration="7s"><?= $title; ?></h1>
                                    <?php if($linkUrl): ?>
                                    </a>
                                    <?php endif; ?>
                                    <div class="video-progress-bar"></div>
                                    <p class="top-video-description wow fadeIn" data-wow-delay="1s" data-wow-duration="7s"><?= $desc; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section intro-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaMainInfoWidget");
});