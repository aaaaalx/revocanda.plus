<?php
class RevocandaSpecialOfferWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-special-offer-widget', __('Special Offer | Revocanda', 'revocanda'),
            ['description' => __('Special Offer', 'revocanda')]);
    }

    public function form($instance) {
        //static fields
        $sectionId = '';
        $status = '';
        $offsets = '';

        if (!empty($instance)) {
            //static fields
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
            $offsets = esc_attr($instance['offsets']);
        }

        //static fields

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

        $Id = $this->get_field_id('offsets');
        $Name = $this->get_field_name('offsets');
        echo '<p><label for="' . $Id . '">' . __('Section offsets', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $offsets . '"></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);
        $values['offsets'] = esc_attr($newInstance['offsets']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!isset($instance['status'])){
            return;
        }

        extract($args);
        //static fields
        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        $offsets = esc_attr($instance['offsets']);
        if($offsets){
            $offsets = " style=\"margin-top:$offsets;margin-bottom:$offsets;padding-top:$offsets;padding-bottom:$offsets;\"";
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-special-offer-1-->
            <section class="section-special-offer-1"<?= $sectionId; ?><?= $offsets; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 wow fadeIn">
                            <div class="flex-container">
                                <div class="dot-list">
                                    <ul>
                                        <li><b>индивидуальный</b> <br>подход и <br> анонимность</li>
                                        <li><b>авторская</b><br> методика лечения</li>
                                        <li><b>немедикаментозное</b> <br> комплексное лечение</li>
                                    </ul>
                                </div>
                                <div class="offer-logo">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-offer.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section special-offer-1-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaSpecialOfferWidget");
});