<?php
class RevocandaSpecial2OfferWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-special-offer-2-widget', __('Special Offer 2 | Revocanda', 'revocanda'),
            ['description' => __('Special Offer 2', 'revocanda')]);
    }

    public function form($instance) {
        //static fields
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            //static fields
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        //static fields
        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-special-offer-2-->
            <section class="section-special-offer-2"<?= $sectionId; ?>>
                <div class="container">
                    <div class="row stretch-y">
                        <div class="col-lg-6 col-xs-12 auto wow fadeInLeft">
                            <h3>Нуждаетесь в помощи в борьбе <br>с пищевой зависимостью?</h3>
                        </div>
                        <div class="col-lg-6 col-xs-12 auto wow fadeInRight">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-transparent.png" alt="" class="logo-transparent">
                        </div>
                    </div>
                </div>
            </section>
            <!--end section-special-offer-2-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaSpecial2OfferWidget");
});