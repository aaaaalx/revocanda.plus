<?php
class RevocandaSpecialistsWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-specialists-widget', __('Specialists | Revocanda', 'revocanda'),
            ['description' => __('Specialists', 'revocanda')]);
    }

    public function form($instance) {
        //static fields
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            //static fields
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        global $isMobile;

        extract($args);
        //static fields
        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-specialist-->
            <section class="section-specialist"<?= $sectionId; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 wow fadeInUp">
                            <div class="section-head-box">
                                <span class="section-head-back"><span>специалисты</span></span>
                                <h2 class="section-head-title"><span>наши специалисты</span></h2>
                            </div>
                            <div class="slider-specialist">

                                <?php
                                $args = [
                                    'posts_per_page'   => 8,
                                    'offset'           => 0,
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'post_type'        => 'specialists',
                                    'post_status'      => 'publish'
                                ];
                                $specialists = new WP_Query($args);
                                $specialists = $specialists->posts;
                                ?>

                                <?php if(count($specialists)>0): ?>
                                    <?php foreach($specialists as $specialist): ?>

                                        <?php
                                        $position = get_post_meta($specialist->ID, '_specialists_position_value_key', true);
                                        $size = 'block-medium-image';
                                        $image = get_the_post_thumbnail_url($specialist->ID, $size);
                                        ?>

                                        <div class="item">
                                            <?php if($image): ?>
                                                <img src="<?= $image; ?>" alt="">
                                            <?php endif; ?>
                                            <div class="description-box">
                                                <div class="inner-wrap">
                                                    <span><?= $specialist->post_title; ?></span>
                                                    <span><?= $position; ?></span>
                                                    <a href="<?= get_permalink($specialist->ID); ?>" class="more-info"><span>подробнее</span></a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </div>
                            <a onclick="ga('send', 'event', 'button2', 'specialists')" href="/specialists" class="main-button blue-button to-right"><span>все специалисты</span></a>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section-specialist-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaSpecialistsWidget");
});