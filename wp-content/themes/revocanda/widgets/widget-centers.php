<?php
class RevocandaCentersWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-centers-widget', __('Centers | Revocanda', 'revocanda'),
            ['description' => __('Centers', 'revocanda')]);
    }

    public function form($instance) {
        //static fields
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            //static fields
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        //static fields
        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-centers-->
            <section class="section-centers"<?= $sectionId; ?>>
                <div class="slider-centers">

                    <?php
                    //get current category
                    $args = [
                        'posts_per_page'   => 5,
                        'offset'           => 0,
                        'category'         => '',
                        'category_name'    => '',
                        'orderby'          => 'date',
                        'order'            => 'DESC',
                        'include'          => '',
                        'exclude'          => '',
                        'meta_key'         => '',
                        'meta_value'       => '',
                        'post_type'        => 'centers',
                        'post_mime_type'   => '',
                        'post_parent'      => '',
                        'author'	   => '',
                        'author_name'	   => '',
                        'post_status'      => 'publish',
                        'suppress_filters' => true
                    ];
                    $posts_array = get_posts( $args );

                    $citiesAndCountries = getCities(true);

                    ?>

                    <?php if(count($posts_array)>0): ?>
                        <?php foreach($posts_array as $post): ?>
                            <?php
                            $address = get_post_meta($post->ID, '_centers_address_value_key', true);
                            $image = get_the_post_thumbnail_url($post->ID, 'large');
                            $country = get_post_meta($post->ID, '_centers_country_value_key', true);
                            $country = isset($citiesAndCountries['countries'][$country])?$citiesAndCountries['countries'][$country]['country']:'';
                            $city = get_post_meta($post->ID, '_centers_city_value_key', true);
                            $city = isset($citiesAndCountries['cities'][$city])?$citiesAndCountries['cities'][$city]['city']:'';
                            ?>

                            <div class="item">
                                <?php if($image): ?>
                                    <div style="background-color:black; opacity:0.6; position:absolute; top:0; left:0; height:100%; width:100%;"></div>
                                    <img src="<?= $image; ?>" alt="">
                                <?php endif; ?>
                                <div class="slider-content">
                                    <div class="container wow fadeIn">
                                        <div class="col-xs-12">
                                            <div class="section-head-box">
                                                <span class="section-head-back"><span><?= $country; ?></span></span>
                                                <h2 class="section-head-title"><span><?= $city; ?></span></h2>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                                            <p><?= wp_trim_words( strip_shortcodes($post->post_content), 12, '...' ); ?></p>
                                            <a href="<?= get_permalink($post->ID); ?>" class="more-info"><span>подробнее</span></a>
                                            <div class="clarfix"></div>
                                            <a href="/centers" class="main-button red-button"><span>все центры</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </section>
            <!--end section-centers-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaCentersWidget");
});