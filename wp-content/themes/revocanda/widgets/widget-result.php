<?php
class RevocandaResultWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-result-widget', __('Result | Revocanda', 'revocanda'),
            ['description' => __('Result', 'revocanda')]);
    }

    public function form($instance) {

        //static fields
        $desc = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {

            //static fields
            $desc = $instance['desc'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $Id = $this->get_field_id('desc');
        $Name = $this->get_field_name('desc');
        echo '<p><label for="' . $Id . '">Описание: </label>';
        echo '<textarea rows="3" class="widefat" id="' . $Id . '" type="text" name="' . $Name . '">' . $desc . '</textarea></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['desc'] = $newInstance['desc'];
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        //static fields
        $desc = $instance['desc'];

        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-reults-->
            <section class="section-results">
                <div class="container">
                    <div class="row stretch-y">
                        <div class="col-md-5 col-md-push-7 col-xs-12 auto wow fadeInRight">
                            <div class="section-head-box">
                                <span class="section-head-back"><span>результаты</span></span>
                                <h2 class="section-head-title"><span>наши результаты</span></h2>
                            </div>
                            <p><?= $desc; ?></p>
                            <a href="/feedbacks" class="main-button red-button to-right hidden-600">все результаты</a>
                        </div>
                        <div class="col-md-7 col-md-pull-5 col-xs-12 auto wow fadeInLeft">
                            <div class="slider-results">

                                <?php
                                $args = [
                                    'posts_per_page'   => -1,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'feedbacks',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'author'	   => '',
                                    'author_name'	   => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true
                                ];
                                $posts_array = get_posts( $args );
                                $posts_array_fin = [];
                                if(count($posts_array)>0){
                                    foreach($posts_array as $key => $post){
                                        $compare_before = get_post_meta($post->ID, '_feedbacks_compare_before_value_key', true);
                                        $compare_after = get_post_meta($post->ID, '_feedbacks_compare_after_value_key', true);

                                        $compare_before_id = get_attachment_file_id($compare_before);
                                        $compare_before_image = get_thumbnails_by_id($compare_before_id, 'block-large-image');
                                        $compare_before_image = $compare_before_image?$compare_before_image:$compare_before;

                                        $compare_after_id = get_attachment_file_id($compare_after);
                                        $compare_after_image = get_thumbnails_by_id($compare_after_id, 'block-large-image');
                                        $compare_after_image = $compare_after_image?$compare_after_image:$compare_after;

                                        if($compare_before_image && $compare_after_image){
                                            $posts_array_fin[$key]['compare_before'] = $compare_before_image;
                                            $posts_array_fin[$key]['compare_after'] = $compare_after_image;
                                        }
                                    }
                                    if(count($posts_array_fin)>0){
                                        $posts_array_fin = array_slice($posts_array_fin, 0, 6);
                                    }
                                }
                                $posts_array = $posts_array_fin;

                                ?>
                                <?php if(count($posts_array)>0): ?>
                                    <?php foreach($posts_array as $post): ?>
                                            <div class="item">
                                                <div class="flipCard">
                                                    <div class="card">
                                                        <div class="face front">
                                                            <img src="<?= $post['compare_before']; ?>" alt="">
                                                            <div class="button-flip-front heartbeat"></div>
                                                        </div>
                                                        <div class="face back">
                                                            <img src="<?= $post['compare_after']; ?>" alt="">
                                                            <div class="button-flip-back heartbeat"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </div>
                            <a href="#" class="main-button red-button to-right visible-600">все результаты</a>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section-results-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaResultWidget");
});