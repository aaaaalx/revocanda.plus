<?php
class RevocandaRegistrationWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('revocanda-registration-widget', __('Registration | Revocanda', 'revocanda'),
            ['description' => __('Registration form', 'revocanda')]);
    }

    public function form($instance) {

        //static fields
        $bg = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {

            //static fields
            $bg = esc_attr($instance['bg']);
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        //static fields

        $Id = $this->get_field_id('bg');
        $Name = $this->get_field_name('bg');
        echo '<p><label for="' . $Id . '">Показывать фон: </label>';
        $checked = '';
        if($bg == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'revocanda') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'revocanda') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        //static fields
        $values['bg'] = esc_attr($newInstance['bg']);
        $values['sectionId'] = esc_attr($newInstance['sectionId']);
        $values['status'] = esc_attr($newInstance['status']);

        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        //static fields

        $bg = $instance['bg'];
        if($bg){
            $bg = ' blue-bg';
        }

        $sectionId = esc_attr($instance['sectionId']);
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-form-->
            <section class="section-form<?= $bg; ?>"<?= $sectionId; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-xs-12 col-xs-offset-0 wow fadeIn">
                            <div class="section-head-box">
                                <span class="section-head-back"><span>обратная связь</span></span>
                                <h2 class="section-head-title"><span>напишите нам</span></h2>
                            </div>
                            <form data-url="<?= admin_url('admin-ajax.php'); ?>" class="form-feedback" method="post">
                                <p>Задайте нам вопрос или расскажите о том, что вас беспокоит.</p>
                                <div class="form-group-name">
                                    <input type="text" placeholder="Ваше имя" class="input-name form-input username" name="username" required>
                                </div>
                                <div class="form-group-phone">
                                    <input type="text" placeholder="Ваш телефон" class="input-phone form-input phone" name="phone" id="phone" required>
                                </div>
                                <div class="form-group-email">
                                    <input type="text" placeholder="Ваш email" class="input-email form-input email" name="email"required>
                                </div>
                                <div class="form-group-message">
                                    <textarea rows="6" class="input-message form-input message" name="message" placeholder="Текст сообщения" required></textarea>
                                </div>

                                <button type="submit" class="main-button red-button">отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section-form-->
            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("RevocandaRegistrationWidget");
});