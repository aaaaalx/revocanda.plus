<?php
/*
 * @package revocanda
 *
 * ================================
 * ADMIN PAGE
 * ================================
 *
 * */
function revocanda_add_admin_page(){

    //add menu item
    add_menu_page('Настройки темы "Revocanda"', 'Revocanda +', 'manage_options', 'revocanda', 'revocanda_theme_options_page',
        get_stylesheet_directory_uri().'/img/logo_icon_20x20.png', 110);

    //add submenu item
    add_submenu_page('revocanda', 'Настройки', 'Настройки темы', 'manage_options', 'revocanda',
        'revocanda_theme_options_page');


    //add submenu item
    add_submenu_page('revocanda', 'Настройки формы обратной связи', 'Контактная форма', 'manage_options', 'revocanda-contact-form',
        'revocanda_contact_form_page');

    //call custom admin page
    add_action('admin_init', 'revocanda_custom_settings');


}
add_action('admin_menu', 'revocanda_add_admin_page');

//custom admin page(settings)
function revocanda_custom_settings(){

    /*Theme options*/

    //add field section
    add_settings_section('revocanda-theme-options', 'Настройки темы', 'revocanda_theme_options', 'revocanda');

    /*Contact form options*/
    //register input fields
    register_setting('revocanda-contact-form-group', 'activate_contact');
    add_settings_section('revocanda-contact-section', 'Контактная форма', 'revocanda_contact_section', 'revocanda-contact');
    add_settings_field('activate-form', 'Активировать контактную форму', 'revocanda_activate_contact', 'revocanda-contact', 'revocanda-contact-section');

    /*Admin email*/
    register_setting('revocanda-theme-group', 'adminEmail');
    add_settings_field('Admin-email', 'Указать email адимнистратора', 'revocanda_admin_email', 'revocanda', 'revocanda-theme-options');

    /*Contact icons options*/
    register_setting('revocanda-theme-group', 'field_contact_icons');
    add_settings_field('Contact-icons', 'Настройки иконок контактов', 'revocanda_contact_icons', 'revocanda', 'revocanda-theme-options');

    /*Countries list*/
    register_setting('revocanda-theme-group', 'field_countries');
    add_settings_field('Countries-list', 'Указать страны', 'revocanda_countries_list', 'revocanda', 'revocanda-theme-options');

    /*Cities list*/
    register_setting('revocanda-theme-group', 'field_cities');
    add_settings_field('Cities-list', 'Указать города', 'revocanda_cities_list', 'revocanda', 'revocanda-theme-options');

    /*Specialization list*/
    register_setting('revocanda-theme-group', 'field_specialization');
    add_settings_field('Specialization-list', 'Указать специализацию', 'revocanda_specialization_list', 'revocanda', 'revocanda-theme-options');

    /*Institution list*/
    register_setting('revocanda-theme-group', 'field_institution');
    add_settings_field('Institution-list', 'Указать тип заведения', 'revocanda_institution_list', 'revocanda', 'revocanda-theme-options');

    /*Events type list*/
    register_setting('revocanda-theme-group', 'field_event_type');
    add_settings_field('Events-type-list', 'Указать тип мероприятия', 'revocanda_event_type_list', 'revocanda', 'revocanda-theme-options');

    /*Shows video category in feedback*/
    register_setting('revocanda-theme-group', 'field_feedback_show_category');
    add_settings_field('Feedback-list', 'Отзывы', 'revocanda_feedback_show_video_category', 'revocanda', 'revocanda-theme-options');

    /*Google map key*/
    register_setting('revocanda-theme-group', 'field_mapKey');
    add_settings_field('Map-key', 'Указать ключ к гугл картам', 'revocanda_map_key', 'revocanda', 'revocanda-theme-options');

    /*Show contacts on load*/
    register_setting('revocanda-theme-group', 'field_activeContacts');
    add_settings_field('activeContacts', 'Активный контакт на странице контактов', 'revocanda_activeContacts', 'revocanda', 'revocanda-theme-options');

}

function revocanda_contact_section(){

}

/*Contact form fields*/
//field output
function revocanda_activate_contact(){
    $options = get_option('activate_contact');
    $checked = @$options == 1?' checked':'';
    echo '<label><input type="checkbox"'.$checked.' name="activate_contact" id="activate_contact" value="1"></label>';
}

//description for section
function revocanda_theme_options(){
    echo '<br><br><br>';
}


//template for theme options page
function revocanda_theme_options_page(){
    require_once get_template_directory().'/inc/template/revocanda-admin.php';
}

//template for contact options page
function revocanda_contact_form_page(){
    require_once get_template_directory().'/inc/template/revocanda-contact-form.php';
}

function revocanda_admin_email(){
    $value = get_option('adminEmail');
    echo '<textarea class="regular-text code" rows="2" name="adminEmail" id="adminEmail">'.$value.'</textarea>';
    echo '<p>Электронные адреса вводятся через запятую</p>';
}

function revocanda_contact_icons(){
    $value = get_option('field_contact_icons');
    $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [type="text"/] [name="title"/] /></p>';
    $pattern .= '<p><media [title="Link"/] [name="img"/] /></p>';
    insertDynamicField('field_contact_icons', __('Add block Field', 'revocanda'), $value, $pattern);
}

function revocanda_countries_list(){
    $value = get_option('field_countries');
    $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [type="text"/] [name="title"/] /></p>';
    insertDynamicField('field_countries', __('Add block Field', 'revocanda'), $value, $pattern);
}

function revocanda_cities_list(){
    $value = get_option('field_cities');
    $pattern = '<p><input [class="widefat datafield"/] [title="Город"/] [type="text"/] [name="city"/] /></p>';
    $pattern .= '<p><select [class="widefat datafield"/] [title="Страна"/] [name="country"/] [data="list:getCountries"/] /></p>';
    insertDynamicField('field_cities', __('Add block Field', 'revocanda'), $value, $pattern);
}

function revocanda_specialization_list(){
    $value = get_option('field_specialization');
    $pattern = '<p><input [class="widefat datafield"/] [title="Specialization"/] [type="text"/] [name="specialization"/] /></p>';
    insertDynamicField('field_specialization', __('Add block Field', 'revocanda'), $value, $pattern);
}

function revocanda_institution_list(){
    $value = get_option('field_institution');
    $pattern = '<p><input [class="widefat datafield"/] [title="Institution"/] [type="text"/] [name="institution"/] /></p>';
    insertDynamicField('field_institution', __('Add block Field', 'revocanda'), $value, $pattern);
}

function revocanda_event_type_list(){
    $value = get_option('field_event_type');
    $pattern = '<p><input [class="widefat datafield"/] [title="Event Type"/] [type="text"/] [name="event_type"/] /></p>';
    insertDynamicField('field_event_type', __('Add block Field', 'revocanda'), $value, $pattern);
}

function revocanda_feedback_show_video_category(){
    $value = get_option('field_feedback_show_category');
    //show video category
    echo '<p><label for="field_feedback_show_category">Показывать категорию "видеоотзывы": </label>';
    $checked = '';
    if($value == 1){
        $checked = ' checked';
    }
    echo '<input id="feedbacks_video_show_cat_value_key" type="checkbox" name="field_feedback_show_category" value="1"'.$checked.'></p>';
}

function revocanda_map_key(){
    $value = get_option('field_mapKey');
    echo '<textarea class="regular-text code" rows="2" name="field_mapKey" id="field_mapKey">'.$value.'</textarea>';
    echo '<p>'.__('Enter google map key', 'revocanda').'</p>';
}

function revocanda_activeContacts(){
    $value = get_option('field_activeContacts');
    $centers = getCenters();
    if(is_array($centers)){
        $centers = array_column($centers, 'post_title', 'ID');
        echo '<select name="field_activeContacts">';
        foreach ($centers as $id => $title) {
            $active = $id==$value?' selected':'';
            echo '<option value="'.$id.'"'.$active.'>'.$title.'</option>';
        }
        echo '</select>';
    }
}