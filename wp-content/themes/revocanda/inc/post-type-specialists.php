<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - SPECIALISTS
 * ================================
 *
 * */

//custom post type
function custom_post_type_specialists(){
    $post_labels = [
        'name' => 'Специалисты',
        'singular_name' => 'Специалисты',
        'menu_name' => 'Специалисты',
        'name_admin_bar' => 'Специалисты'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'specialists',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author', 'page-attributes',  'thumbnail'],
        'show_in_nav_menus' => true,
        //'taxonomies' => ['category_specialists'],
        //'rewrite' => array( 'slug' => 'specialists/%cat%', 'with_front' => true ),
        'map_meta_cap' => true,
    ];

    $cat_labels = array(
        'name'              => __( 'Specialists Categories' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );
    $cat_args = array(
        'labels'            => $cat_labels,
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'publicly_queryable'=> true,
        'show_tagcloud'     => false,
        'rewrite'           => array( 'slug' => 'specialists', 'hierarchical' => true )
    );
    //register_taxonomy( 'category_specialists', ['specialists'], $cat_args );
    register_post_type('specialists', $post_args);
    //flush_rewrite_rules();
}
add_action( 'init', 'custom_post_type_specialists', 0 );

//$thisCustomPosts['specialists'] = true;

function specialists_taxonomy_slug_rewrite_rules($wp_rewrite) {
    $rules['specialists/(.+?)/_([^/]+)?$'] = 'index.php?category_specialists=$matches[1]&post_type=specialists&name=$matches[2]';
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'specialists_taxonomy_slug_rewrite_rules');

//set redirect to select category template
function specialists_template_redirect( $template ) {
    $term = get_queried_object();
    if(isset($term->term_id)){
        $temp = get_term_meta( $term->term_id, 'category_specialists_templates', true );
        if($temp){
            $currPath = substr($template, 0, strrpos($template, '/'));
            $template = $currPath.'/'.$temp;
        }
    }
    return $template;
}
add_filter( 'template_include', 'specialists_template_redirect', 99 );

//Add custom fields to posts//

//add meta boxes
function specialists_add_meta_box(){

    //contacts
    add_meta_box('specialists_contacts', __('Contacts', 'revocanda'), 'specialists_contacts_callback', 'specialists', 'normal', 'default');//wp prebuilt metal box adding function. "side" - position(normal|side|advanced). "default" - priority(high|default|low)
    //Centers
    add_meta_box('specialists_centers', __('Centers', 'revocanda'), 'specialists_centers_callback', 'specialists', 'normal', 'default');
    //Institution
    add_meta_box('specialists_specialization', 'Специализация', 'specialists_specialization_callback', 'specialists', 'normal', 'default');
    //position
    add_meta_box('specialists_position', 'Должность', 'specialists_position_callback', 'specialists', 'normal', 'default');

    //settings
    add_meta_box('specialists_settings', 'Настройки', 'specialists_settings_callback', 'specialists', 'normal', 'default');

}

function specialists_contacts_callback($post){

    //contacts
    wp_nonce_field('specialists_save_data', 'specialists_contacts_meta_box_nonce');//add unique verifying field
    $phones = get_post_meta($post->ID, '_specialists_phones_value_key', true);//get custom meta box
    $emails = get_post_meta($post->ID, '_specialists_emails_value_key', true);//get custom meta box
    $contacts = get_post_meta($post->ID, '_specialists_contacts_value_key', true);//get custom meta box
    $country = get_post_meta($post->ID, '_specialists_country_value_key', true);//get custom meta box
    $city = get_post_meta($post->ID, '_specialists_city_value_key', true);//get custom meta box

    //country
    $countries = getCountries();
    echo '<p>Страна: </p>';
    echo '<select 
    class="widefat selectList" 
    id="specialists_country_value_key" 
    name="specialists_country_value_key" 
    data-connected="specialists_city_value_key" 
    data-call="getCitiesByCountry" 
    data-value="true">';
    echo '<option value=""></option>';
    if(count($countries)>0){
        foreach ($countries as $k => $item){
            $selected = '';
            if($country == $k){
                $selected = ' selected';
            }
            echo '<option value="'.$k.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select>';

    //city
    $cities = [];
    if($country){
        $cities = getCitiesByCountry($country);
    }
    echo '<p>Город: </p>';
    echo '<select class="widefat" id="specialists_city_value_key" name="specialists_city_value_key">';
    echo '<option value=""></option>';
    if(count($cities)>0){
        foreach ($cities as $k => $item){
            $selected = '';
            if($city == $k){
                $selected = ' selected';
            }
            echo '<option value="'.$k.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select>';

    //phones
    echo '<p>Телефоны: </p>';
    $pattern = '<p><input [class="widefat datafield"/] [title="Phone"/] [type="text"/] [name="phone"/] /></p>';
    $pattern .= '<p><input [class="widefat datafield"/] [title="Phone Link"/] [type="text"/] [name="phone_link"/] /></p>';
    insertDynamicField('specialists_phones_value_key', __('Add block Field', 'revocanda'), $phones, $pattern);

    //emails
    echo '<p>Emails: </p>';
    $pattern = '<p><input [class="widefat datafield"/] [title="Email"/] [type="text"/] [name="email"/] /></p>';
    $pattern .= '<p><input [class="widefat datafield"/] [title="Email Link"/] [type="text"/] [name="email_link"/] /></p>';
    insertDynamicField('specialists_emails_value_key', __('Add block Field', 'revocanda'), $emails, $pattern);

    //other contacts
    echo '<p>Друге контакты: </p>';
    $pattern = '<p><input [class="widefat datafield"/] [title="Contacts"/] [type="text"/] [name="contacts"/] /></p>';
    $pattern .= '<p><input [class="widefat datafield"/] [title="Contacts Link"/] [type="text"/] [name="contacts_link"/] /></p>';
    $pattern .= '<p><select [class="widefat datafield"/] [title="Icon"/] [name="icon"/] [data="list:getContactsIcons"/] /></p>';
    insertDynamicField('specialists_contacts_value_key', __('Add block Field', 'revocanda'), $contacts, $pattern);

}

function specialists_centers_callback($post){

    //centers
    wp_nonce_field('specialists_save_data', 'specialists_centers_meta_box_nonce');//add unique verifying field
    $value = get_post_meta($post->ID, '_specialists_centers_value_key', true);//get custom meta box

    $args = array(
        'numberposts' => -1,
        'offset' => 0,
        'cat'    => 0,
        'orderby'     => 0,
        'order'       => 0,
        'post_type'   => 'centers',
        'status' => 0
    );

    $posts = getPostsByPostType($args['post_type'], $args['cat'], $args['numberposts'], $args['offset'], $args['orderby'], $args['order'], $args['status']);

    echo '<select class="widefat" id="specialists_centers_value_key" multiple="multiple" name="specialists_centers_value_key[]">';
    if(count($posts)>0){
        echo '<option value=""></option>';
        foreach ($posts as $post){
            $selected = '';
            if(count($value)>0){
                foreach ($value as $v){
                    if((int)$v == $post->ID){
                        $selected = ' selected';
                        break;
                    }
                }
            }
            echo '<option value="'.$post->ID.'"'.$selected.'>'.$post->post_title.'</option>';
        }
    }
    echo '</select>';
}

function specialists_specialization_callback($post){

    //specialization
    wp_nonce_field('specialists_save_data', 'specialists_specialization_meta_box_nonce');//add unique verifying field
    $value = get_post_meta($post->ID, '_specialists_specialization_value_key', true);//get custom meta box

    $specializations = getSpecializations();

    echo '<select class="widefat" id="specialists_specialization_value_key" multiple="multiple" name="specialists_specialization_value_key[]">';
    if(count($specializations)>0){
        echo '<option value=""></option>';
        foreach ($specializations as $key => $specialization){
            $selected = '';
            if(count($value)>0){
                foreach ($value as $v){
                    if((int)$v == $key){
                        $selected = ' selected';
                        break;
                    }
                }
            }
            echo '<option value="'.$key.'"'.$selected.'>'.$specialization.'</option>';
        }
    }
    echo '</select>';
}

function specialists_position_callback($post){

    //position
    wp_nonce_field('specialists_save_data', 'specialists_position_meta_box_nonce');//add unique verifying field
    $value = get_post_meta($post->ID, '_specialists_position_value_key', true);//get custom meta box

    echo '<p><textarea id="specialists_position_value_key" class="widefat datafield" title="Position" name="specialists_position_value_key">'.$value.'</textarea></p>';

}

function specialists_save_data($post_id){

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    //centers
    if(isset($_POST['specialists_centers_meta_box_nonce']) &&
        wp_verify_nonce($_POST['specialists_centers_meta_box_nonce'], 'specialists_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['specialists_centers_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_centers_value_key'];
        update_post_meta($post_id, '_specialists_centers_value_key', $my_data);//save data from meta box field
    }

    //specialization
    if(isset($_POST['specialists_specialization_meta_box_nonce']) &&
        wp_verify_nonce($_POST['specialists_specialization_meta_box_nonce'], 'specialists_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['specialists_specialization_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_specialization_value_key'];
        update_post_meta($post_id, '_specialists_specialization_value_key', $my_data);//save data from meta box field
    }

    //position
    if(isset($_POST['specialists_position_meta_box_nonce']) &&
        wp_verify_nonce($_POST['specialists_position_meta_box_nonce'], 'specialists_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['specialists_position_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_position_value_key'];
        update_post_meta($post_id, '_specialists_position_value_key', $my_data);//save data from meta box field
    }

    //phones, emails, contacts
    if(isset($_POST['specialists_contacts_meta_box_nonce']) &&
        wp_verify_nonce($_POST['specialists_contacts_meta_box_nonce'], 'specialists_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //phones
    if(isset($_POST['specialists_phones_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_phones_value_key'];
        update_post_meta($post_id, '_specialists_phones_value_key', $my_data);//save data from meta box field
    }
    //emails
    if(isset($_POST['specialists_emails_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_emails_value_key'];
        update_post_meta($post_id, '_specialists_emails_value_key', $my_data);//save data from meta box field
    }
    //contacts
    if(isset($_POST['specialists_contacts_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_contacts_value_key'];
        update_post_meta($post_id, '_specialists_contacts_value_key', $my_data);//save data from meta box field
    }

    //country
    if(isset($_POST['specialists_country_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_country_value_key'];
        update_post_meta($post_id, '_specialists_country_value_key', $my_data);//save data from meta box field
    }
    //city
    if(isset($_POST['specialists_city_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_city_value_key'];
        update_post_meta($post_id, '_specialists_city_value_key', $my_data);//save data from meta box field
    }

    //settings background
    if(isset($_POST['specialists_settings_bg_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['specialists_settings_bg_value_key'];
        update_post_meta($post_id, '_specialists_settings_bg_value_key', $my_data);//save data from meta box field
    }

}

function specialists_settings_callback($post){

    //settings
    wp_nonce_field('specialists_save_data', 'specialists_settings_meta_box_nonce');//add unique verifying field
    $background = get_post_meta($post->ID, '_specialists_settings_bg_value_key', true);//get custom meta box

    //background
    $Id = 'specialists_settings_bg_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">' . __('Background', 'revocanda') . ': </label>';
    echo '<div class="widget-block-img">';
    if($background){
        $backgroundMime = get_file_mime_type($background);
        if(getFileFormat($backgroundMime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$background.'" type="'.$backgroundMime.'">';
            echo '</video>';
        }
        else{
            if($background && strpos($background, ', ')>-1){
                //check is multi media
                $mediaStringA = explode(', ', $background);
                if(count($mediaStringA)>0){
                    add_thickbox();
                    $selectedMedia = '';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    foreach($mediaStringA as $item){
                        $mediaId = pippin_get_file_id($item);
                        if($mediaId){
                            $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                        }
                        echo '<a class="thickbox" href="'.$item.'?TB_iframe=false&width=100%&height=100%"><img src="'.$item.'"></a>';
                    }
                    if($selectedMedia){
                        echo '<script>'.$selectedMedia.'</script>';
                    }
                }
            }
            else{
                $mediaId = pippin_get_file_id($background);
                $selectedMedia = '';
                if($mediaId){
                    $selectedMedia = '<script>';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                    $selectedMedia .= '</script>';
                }
                add_thickbox();
                echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
                echo $selectedMedia;
            }

            //add_thickbox();
            //echo '<a class="thickbox" href="'.$background.'?TB_iframe=true&width=100%&height=100%"><img src="'.$background.'"></a>';
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';
}

add_action('add_meta_boxes', 'specialists_add_meta_box');//add custom metabox
add_action('save_post', 'specialists_save_data');//save custom meta box


/*Add custom fields to taxonomies*/

//taxonomies templates list
function get_category_specialists_templates(){
    return [
        '' => '',
        //'video' => 'taxonomy-category_specialists-video.php',
        //'images' => 'taxonomy-category_specialists-images.php'
    ];
}

//display term meta fields on add
function category_specialists_add_meta_fields( $taxonomy ) {
    //term settings
}
add_action( 'category_specialists_add_form_fields', 'category_specialists_add_meta_fields', 10, 2 );

//display term meta fields on edit
function category_specialists_edit_meta_fields( $term, $taxonomy ) {
    $background = get_term_meta( $term->term_id, 'category_specialists_background', true );
    $temp = get_term_meta( $term->term_id, 'category_specialists_templates', true );

    //set header bg image
    $Id = 'category_specialists_background';
    $Name = $Id;
    echo '<p><label for="' . $Id . '"><b>' . __('Background', 'revocanda') . '</b></label>';
    echo '<div class="widget-block-img">';
    add_thickbox();
    if($background){
        $backgroundMime = get_file_mime_type($background);
        if(getFileFormat($backgroundMime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$background.'" type="'.$backgroundMime.'">';
            echo '</video>';
        }
        else{
            echo '<a class="thickbox" href="'.$background.'?TB_iframe=true&width=100%&height=100%"><img src="'.$background.'"></a>';
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
    echo '<input data-id="'.$Id.'" class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';

    $temps = get_category_specialists_templates();
    if(count($temps)>0){
        $Id = 'category_specialists_templates';
        $Name = $Id;
        echo '<p><label for="'.$Id.'">' . __('Template', 'revocanda') . '</label>';
        echo '<select name="'.$Name.'" id="'.$Id.'"></p>';
        foreach ($temps as $k => $val) {
            echo '<option value="'.$val.'" '.selected($temp, $val, false).'>'.$k."</option>\n";
        }
    }

}
add_action( 'category_specialists_edit_form_fields', 'category_specialists_edit_meta_fields', 10, 2 );

// Save custom terms meta fields
function category_specialists_save_taxonomy_meta( $term_id, $tag_id ) {

    //category_specialists_background
    if (isset($_POST['category_specialists_background'])) {
        update_term_meta($term_id, 'category_specialists_background', $_POST['category_specialists_background']);
    }

    //category_specialists_templates
    if (isset($_POST['category_specialists_templates'])) {
        update_term_meta($term_id, 'category_specialists_templates', $_POST['category_specialists_templates']);
    }

}
add_action( 'created_category_specialists', 'category_specialists_save_taxonomy_meta', 10, 2 );
add_action( 'edited_category_specialists', 'category_specialists_save_taxonomy_meta', 10, 2 );