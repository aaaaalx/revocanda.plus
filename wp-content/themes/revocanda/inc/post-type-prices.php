<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - PRICES
 * ================================
 *
 * */

//custom post type
function custom_post_type_prices(){
    $post_labels = [
        'name' => 'Цены',
        'singular_name' => 'Цены',
        'menu_name' => 'Цены',
        'name_admin_bar' => 'Цены'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor'],
        'show_in_nav_menus' => true,
        'taxonomies' => ['category_prices'],
        'rewrite' => array( 'slug' => 'prices/%cat%', 'with_front' => true ),
        'map_meta_cap' => true,
    ];

    $cat_labels = array(
        'name'              => 'Категории цен',
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );
    $cat_args = array(
        'labels'            => $cat_labels,
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'publicly_queryable'=> true,
        'show_tagcloud'     => false,
        'rewrite'           => array( 'slug' => 'prices', 'hierarchical' => true )
    );
    register_taxonomy( 'category_prices', ['prices'], $cat_args );
    register_post_type('prices', $post_args);
}
add_action( 'init', 'custom_post_type_prices');

add_action('add_meta_boxes', 'revocanda_prices_add_meta_box');//add custom metabox
add_action('save_post', 'revocanda_save_prices_data');//save custom meta box

/*prices meta boxes*/
function revocanda_prices_add_meta_box(){
    add_meta_box('prices_price', 'Цены', 'revocanda_prices_callback', 'prices', 'normal', 'default');//wp prebuilt metal box adding function. "side" - position(normal|side|advanced). "default" - priority(high|default|low)
}

function revocanda_prices_callback($post){
    wp_nonce_field('revocanda_save_prices_data', 'revocanda_prices_meta_box_nonce');//add unique verifying field
    $price = get_post_meta($post->ID, '_prices_price_value_key', true);//get custom meta box
    $persons = get_post_meta($post->ID, '_prices_persons_value_key', true);//get custom meta box

    echo '<p><lable for="revocanda_prices_price_field">Цены: </lable>';
    echo '<input type="text" id="revocanda_prices_price_field" name="revocanda_prices_price_field" value="'.esc_attr($price).'" size="25"> ';

    echo '<lable for="revocanda_prices_persons_field">Люди: </lable>';
    echo '<input type="text" id="revocanda_prices_persons_field" name="revocanda_prices_persons_field" value="'.esc_attr($persons).'" size="25"></p>';

}

function revocanda_save_prices_data($post_id){

    if(!isset($_POST['revocanda_prices_meta_box_nonce'])){//check is metabox exist
        return;
    }
    if(wp_verify_nonce($_POST['revocanda_prices_meta_box_nonce'], 'revocanda_prices_add_meta_box')){//check is nonce exist
        return;
    }

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }
    if(!isset($_POST['revocanda_prices_price_field']) || !isset($_POST['revocanda_prices_persons_field'])){//check is metabox field exist
        return;
    }

    $my_data = sanitize_text_field($_POST['revocanda_prices_price_field']);//get posted data from meta box field
    update_post_meta($post_id, '_prices_price_value_key', $my_data);//save data from meta box field

    $my_data = sanitize_text_field($_POST['revocanda_prices_persons_field']);//get posted data from meta box field
    update_post_meta($post_id, '_prices_persons_value_key', $my_data);//save data from meta box field

}

function prices_post_link( $post_link, $post, $leavename, $sample ){
    if($post && $post->post_type == 'prices'){
            $post_link_array = parse_url($post_link);
            $terms = wp_get_object_terms( $post->ID, 'category_prices' );
            $term = null;
            if(count($terms)>0){
                $term = $terms[0];
            }
            if($term){
                return $post_link_array['scheme'].'://'.$post_link_array['host'].'/prices/'.$term->slug;
            }
        }
    return $post_link;
}
add_filter( 'post_type_link', 'prices_post_link', 10, 4 );
