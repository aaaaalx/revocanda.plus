<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - LICENSES
 * ================================
 *
 * */

//custom post type
function custom_post_type_licenses(){
    $post_labels = [
        'name' => 'Лицензии',
        'singular_name' => 'Лицензии',
        'menu_name' => 'Лицензии',
        'name_admin_bar' => 'Лицензии'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'licenses',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'author', 'thumbnail'],
        'show_in_nav_menus' => true,
        'taxonomies' => ['category_licenses'],
        //'rewrite' => array( 'slug' => 'specialists/%cat%', 'with_front' => true ),
        'map_meta_cap' => true,
    ];

    $cat_labels = array(
        'name'              => 'Лицензии',
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );
    $cat_args = array(
        'labels'            => $cat_labels,
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'publicly_queryable'=> true,
        'show_tagcloud'     => false,
        //'rewrite'           => array( 'slug' => 'licenses', 'hierarchical' => true )
    );
    register_taxonomy( 'category_licenses', ['licenses'], $cat_args );
    register_post_type('licenses', $post_args);
}
add_action( 'init', 'custom_post_type_licenses', 0 );

function licenses_post_link( $post_link, $post, $leavename, $sample ){
    if($post && $post->post_type == 'licenses'){
        $post_link = parse_url($post_link);
        return $post_link['scheme'].'://'.$post_link['host'].'/licenses';
    }
    return $post_link;
}
add_filter( 'post_type_link', 'licenses_post_link', 10, 4 );