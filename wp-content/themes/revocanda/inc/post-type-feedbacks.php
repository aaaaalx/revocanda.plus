<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - FEEDBACKS
 * ================================
 *
 * */

//custom post type
function custom_post_type_feedbacks(){
    $post_labels = [
        'name' => 'Отзывы',
        'singular_name' => 'Отзывы',
        'menu_name' => 'Отзывы',
        'name_admin_bar' => 'Отзывы'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'feedbacks',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'author'],
        'show_in_nav_menus' => true,
        'rewrite' => array( 'slug' => 'feedbacks', 'with_front' => true ),
        'map_meta_cap' => true,
    ];

    register_post_type('feedbacks', $post_args);
}
add_action( 'init', 'custom_post_type_feedbacks', 0 );

//add meta boxes
function feedbacks_add_meta_box(){

    //feedbacks photo before and after (compare)
    add_meta_box('feedbacks_compare', 'Фото до и после', 'feedbacks_compare_callback', 'feedbacks', 'normal', 'default');

    //feedbacks reviews
    add_meta_box('feedbacks_reviews', 'Отзывы', 'feedbacks_reviews_callback', 'feedbacks', 'normal', 'default');

    //feedbacks photo reviews
    add_meta_box('feedbacks_photo_reviews', 'Фото отзывы', 'feedbacks_photo_reviews_callback', 'feedbacks', 'normal', 'default');

    //feedbacks video
    add_meta_box('feedbacks_video', 'Видео', 'feedbacks_video_callback', 'feedbacks', 'normal', 'default');

}

function feedbacks_compare_callback($post){

    wp_nonce_field('feedbacks_save_data', 'feedbacks_compare_meta_box_nonce');//add unique verifying field
    $compare_before = get_post_meta($post->ID, '_feedbacks_compare_before_value_key', true);//get custom meta box
    $compare_after = get_post_meta($post->ID, '_feedbacks_compare_after_value_key', true);//get custom meta box
    $compare_status = get_post_meta($post->ID, '_feedbacks_compare_status_value_key', true);//get custom meta box

    //compare before
    $Id = 'feedbacks_compare_before_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Фото до: </label>';
    echo '<div class="widget-block-img">';
    if($compare_before){
        $mediaId = pippin_get_file_id($compare_before);
        $selectedMedia = '';
        if($mediaId){
            $selectedMedia = '<script>';
            $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
            $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
            $selectedMedia .= '</script>';
        }
        add_thickbox();
        echo '<a class="thickbox" href="'.$compare_before.'?TB_iframe=false&width=100%&height=100%"><img src="'.$compare_before.'"></a>';
        echo $selectedMedia;
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$compare_before.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';

    //compare after
    $Id = 'feedbacks_compare_after_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Фото после: </label>';
    echo '<div class="widget-block-img">';
    if($compare_after){
        $mediaId = pippin_get_file_id($compare_after);
        $selectedMedia = '';
        if($mediaId){
            $selectedMedia = '<script>';
            $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
            $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
            $selectedMedia .= '</script>';
        }
        add_thickbox();
        echo '<a class="thickbox" href="'.$compare_after.'?TB_iframe=false&width=100%&height=100%"><img src="'.$compare_after.'"></a>';
        echo $selectedMedia;
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$compare_after.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';

}

function feedbacks_reviews_callback($post){

    wp_nonce_field('feedbacks_save_data', 'feedbacks_compare_meta_box_nonce');//add unique verifying field
    $reviews_name = get_post_meta($post->ID, '_feedbacks_reviews_name_value_key', true);//get custom meta box
    $reviews_age = get_post_meta($post->ID, '_feedbacks_reviews_age_value_key', true);//get custom meta box
    $reviews_text = get_post_meta($post->ID, '_feedbacks_reviews_text_value_key', true);//get custom meta box
    $reviews_status = get_post_meta($post->ID, '_feedbacks_reviews_status_value_key', true);//get custom meta box

    //name
    echo '<p>Имя: </p>';
    echo '<p><input id="feedbacks_reviews_name_value_key" class="widefat datafield" title="Video description" type="text" name="feedbacks_reviews_name_value_key" value="'.$reviews_name.'"></p>';

    //age
    echo '<p>Возраст: </p>';
    echo '<p><input id="feedbacks_reviews_age_value_key" class="widefat datafield" title="Video description" type="text" name="feedbacks_reviews_age_value_key" value="'.$reviews_age.'"></p>';

    //text
    echo '<p>Текст: </p>';
    echo '<p><textarea rows="6" id="feedbacks_reviews_text_value_key" class="widefat datafield" title="Video description" name="feedbacks_reviews_text_value_key">'.$reviews_text.'</textarea></p>';

}

function feedbacks_photo_reviews_callback($post){

    wp_nonce_field('feedbacks_save_data', 'feedbacks_compare_meta_box_nonce');//add unique verifying field
    $photo_reviews = get_post_meta($post->ID, '_feedbacks_photo_reviews_value_key', true);//get custom meta box

    //photo reviews
    $Id = 'feedbacks_photo_reviews_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Фото: </label>';
    echo '<div class="widget-block-img">';
    if($photo_reviews){
        $mediaId = pippin_get_file_id($photo_reviews);
        $selectedMedia = '';
        if($mediaId){
            $selectedMedia = '<script>';
            $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
            $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
            $selectedMedia .= '</script>';
        }
        add_thickbox();
        echo '<a class="thickbox" href="'.$photo_reviews.'?TB_iframe=false&width=100%&height=100%"><img src="'.$photo_reviews.'"></a>';
        echo $selectedMedia;
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$photo_reviews.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';

}

function feedbacks_video_callback($post){
    wp_nonce_field('feedbacks_save_data', 'feedbacks_video_meta_box_nonce');//add unique verifying field
    $video = get_post_meta($post->ID, '_feedbacks_video_value_key', true);//get custom meta box
    $video_link = get_post_meta($post->ID, '_feedbacks_video_link_value_key', true);//get custom meta box
    $video_type = get_post_meta($post->ID, '_feedbacks_video_type_value_key', true);//get custom meta box
    $video_title = get_post_meta($post->ID, '_feedbacks_video_title_value_key', true);//get custom meta box
    $video_desc = get_post_meta($post->ID, '_feedbacks_video_desc_value_key', true);//get custom meta box
    $isVideoActive = get_post_meta($post->ID, '_feedbacks_video_active_value_key', true);//get custom meta box
    $showCat = get_post_meta($post->ID, '_feedbacks_video_show_cat_value_key', true);//get custom meta box

    //is shown specialist section
    echo '<p><label for="feedbacks_video_active_value_key">Показывать видео: </label>';
    $checked = '';
    if($isVideoActive == 1){
        $checked = ' checked';
    }
    echo '<input id="feedbacks_video_active_value_key" type="checkbox" name="feedbacks_video_active_value_key" value="1"'.$checked.'></p>';

    echo '<p>Тип видео: </p>';
    echo '<input type="radio" name="feedbacks_video_type_value_key" value="video"'.(!$video_type||$video_type=='video'?' checked':'').'> Видео<br>';
    echo '<input type="radio" name="feedbacks_video_type_value_key" value="embed"'.($video_type=='embed'?' checked':'').'> Вставленное видео<br>';

    //video
    $Id = 'feedbacks_video_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Видео: </label>';
    echo '<div class="widget-block-img">';
    if($video){
        $mime = get_file_mime_type($video);
        if(getFileFormat($mime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$video.'" type="'.$mime.'">';
            echo '</video>';
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:video" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$video.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';

    //video embed
    echo '<p>Ссылка на видео для вставки: </p>';
    echo '<p><textarea id="feedbacks_video_link_value_key" class="widefat datafield" title="Вставленное видео" name="feedbacks_video_link_value_key">'.$video_link.'</textarea></p>';

    //title
    echo '<p>'.__('Title', 'revocanda').': </p>';
    echo '<p><input id="feedbacks_video_title_value_key" class="widefat datafield" title="'.__('Title', 'revocanda').'" type="text" name="feedbacks_video_title_value_key" value="'.$video_title.'"></p>';

}

function feedbacks_save_data($post_id){

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    //compare
    if(isset($_POST['feedbacks_compare_value_key']) &&
        wp_verify_nonce($_POST['feedbacks_compare_value_key'], 'feedbacks_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //compare before
    if(isset($_POST['feedbacks_compare_before_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_compare_before_value_key'];
        update_post_meta($post_id, '_feedbacks_compare_before_value_key', $my_data);//save data from meta box field
    }
    //compare after
    if(isset($_POST['feedbacks_compare_after_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_compare_after_value_key'];
        update_post_meta($post_id, '_feedbacks_compare_after_value_key', $my_data);//save data from meta box field
    }

    //reviews
    if(isset($_POST['feedbacks_reviews_value_key']) &&
        wp_verify_nonce($_POST['feedbacks_reviews_value_key'], 'feedbacks_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //reviews name
    if(isset($_POST['feedbacks_reviews_name_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_reviews_name_value_key'];
        update_post_meta($post_id, '_feedbacks_reviews_name_value_key', $my_data);//save data from meta box field
    }
    //reviews age
    if(isset($_POST['feedbacks_reviews_age_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_reviews_age_value_key'];
        update_post_meta($post_id, '_feedbacks_reviews_age_value_key', $my_data);//save data from meta box field
    }
    //reviews text
    if(isset($_POST['feedbacks_reviews_text_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_reviews_text_value_key'];
        update_post_meta($post_id, '_feedbacks_reviews_text_value_key', $my_data);//save data from meta box field
    }

    //photo reviews
    if(isset($_POST['feedbacks_photo_reviews_value_key']) &&
        wp_verify_nonce($_POST['feedbacks_photo_reviews_value_key'], 'feedbacks_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //photo reviews
    if(isset($_POST['feedbacks_photo_reviews_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_photo_reviews_value_key'];
        update_post_meta($post_id, '_feedbacks_photo_reviews_value_key', $my_data);//save data from meta box field
    }

    //video settings
    if(isset($_POST['feedbacks_video_meta_box_nonce']) &&
        wp_verify_nonce($_POST['feedbacks_video_meta_box_nonce'], 'feedbacks_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //is video active
    if(@$_POST['feedbacks_video_active_value_key'] == 1){//check is meta box verifying exist
        update_post_meta($post_id, '_feedbacks_video_active_value_key', 1);//save data from meta box field
    }
    else{
        update_post_meta($post_id, '_feedbacks_video_active_value_key', '');//save data from meta box field
    }
    //video type
    if(isset($_POST['feedbacks_video_type_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_video_type_value_key'];
        update_post_meta($post_id, '_feedbacks_video_type_value_key', $my_data);//save data from meta box field
    }
    //video
    if(isset($_POST['feedbacks_video_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_video_value_key'];
        update_post_meta($post_id, '_feedbacks_video_value_key', $my_data);//save data from meta box field
    }
    //video embed
    if(isset($_POST['feedbacks_video_link_value_key'])){//check is meta box verifying exist
        $my_data = esc_attr($_POST['feedbacks_video_link_value_key']);
        update_post_meta($post_id, '_feedbacks_video_link_value_key', $my_data);//save data from meta box field
    }
    //video title
    if(isset($_POST['feedbacks_video_title_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['feedbacks_video_title_value_key'];
        update_post_meta($post_id, '_feedbacks_video_title_value_key', $my_data);//save data from meta box field
    }

}

add_action('add_meta_boxes', 'feedbacks_add_meta_box');//add custom metabox
add_action('save_post', 'feedbacks_save_data');//save custom meta box

function feedbacks_post_link( $post_link, $post, $leavename, $sample ){
    if($post && $post->post_type == 'feedbacks'){
        $post_link = parse_url($post_link);
        return $post_link['scheme'].'://'.$post_link['host'].'/feedbacks';
    }
    return $post_link;
}
add_filter( 'post_type_link', 'feedbacks_post_link', 10, 4 );