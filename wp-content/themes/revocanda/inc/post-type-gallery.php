<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - GALLERY
 * ================================
 *
 * */

//custom post type
function custom_post_type_gallery(){
    $post_labels = [
        'name' => 'Галерея',
        'singular_name' => 'Галерея',
        'menu_name' => 'Галерея',
        'name_admin_bar' => 'Галерея'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'page',
        'has_archive' => 'gallery',
        'hierarchical' => false,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author', 'page-attributes'],
        'show_in_nav_menus' => true,
        'map_meta_cap' => true,
        'rewrite' => array( 'slug' => 'gallery', 'with_front' => true )
    ];

    register_post_type('gallery', $post_args);
}
add_action( 'init', 'custom_post_type_gallery', 0 );

//add meta boxes
function gallery_add_meta_box(){
    add_meta_box('gallery_options', 'Опции', 'gallery_callback', 'gallery', 'normal', 'default');
}

function gallery_callback($post){

    wp_nonce_field('gallery_save_data', 'gallery_meta_box_nonce');//add unique verifying field
    $centers = get_post_meta($post->ID, '_gallery_centers_value_key', true);//get custom meta box
    $location = get_post_meta($post->ID, '_gallery_location_value_key', true);//get custom meta box

    //centers
    $args = array(
        'numberposts' => -1,
        'offset' => 0,
        'cat'    => 0,
        'orderby'     => 0,
        'order'       => 0,
        'post_type'   => 'centers',
        'status' => 0
    );

    $posts = getPostsByPostType($args['post_type'], $args['cat'], $args['numberposts'], $args['offset'], $args['orderby'], $args['order'], $args['status']);

    echo '<p><lable for="gallery_centers_field">Центры: </lable>';
    echo '<select class="widefat" id="gallery_centers_field" multiple="multiple" name="gallery_centers_field[]">';
    if(count($posts)>0){
        echo '<option value=""></option>';
        foreach ($posts as $post){
            $selected = '';
            if(count($centers)>0){
                foreach ($centers as $v){
                    if((int)$v == $post->ID){
                        $selected = ' selected';
                        break;
                    }
                }
            }
            echo '<option value="'.$post->ID.'"'.$selected.'>'.$post->post_title.'</option>';
        }
    }
    echo '</select>';

    //location
    echo '<p><lable for="gallery_location_field">Локация: </lable>';
    echo '<input type="text" id="gallery_location_field" name="gallery_location_field" value="'.esc_attr($location).'" size="25"> ';

}

function gallery_save_data($post_id){

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    if(isset($_POST['gallery_meta_box_nonce']) &&
        wp_verify_nonce($_POST['gallery_meta_box_nonce'], 'gallery_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }

    //centers
    if(isset($_POST['gallery_centers_field'])){//check is meta box verifying exist

        $my_data = $_POST['gallery_centers_field'];
        update_post_meta($post_id, '_gallery_centers_value_key', $my_data);//save data from meta box field
    }

    //location
    if(isset($_POST['gallery_location_field'])){//check is meta box verifying exist

        $my_data = $my_data = sanitize_text_field($_POST['gallery_location_field']);
        update_post_meta($post_id, '_gallery_location_value_key', $my_data);//save data from meta box field
    }

    return;

}
add_action('add_meta_boxes', 'gallery_add_meta_box');//add custom metabox
add_action('save_post', 'gallery_save_data');//save custom meta box


function gallery_post_link( $post_link, $post, $leavename, $sample ){
    if($post && $post->post_type == 'gallery'){
        $post_link = parse_url($post_link);
        return $post_link['scheme'].'://'.$post_link['host'].'/gallery';
    }
    return $post_link;
}
add_filter( 'post_type_link', 'gallery_post_link', 10, 4 );