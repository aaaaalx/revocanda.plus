<?php
/*
 * @package revocanda
 *
 *
 * */

add_action('wp_ajax_nopriv_revocanda_save_contact_form', 'save_contact_form');
add_action('wp_ajax_revocanda_save_contact_form', 'save_contact_form');

function save_contact_form(){

    $username = wp_strip_all_tags(@$_POST['username']);
    $phone = wp_strip_all_tags(@$_POST['phone']);
    $email = wp_strip_all_tags(@$_POST['email']);
    $message = wp_strip_all_tags(@$_POST['message']);

    $_SESSION['contactFormMessage'] = '';

    $errorMess_1 = '<h1 style="color: red;">'.__('The message was not sent! Please try again.', 'winnbrain-land').'</h1>';
    $errorMess_2 = '<h1 style="color: red;">'.__('Fill all the fields, please!', 'winnbrain-land').'</h1>';
    $successMess = '<h1>'.__('Thank you!', 'winnbrain-land').'</h1>';
    $successMess .= '<p>'.__('We are grateful for your interest in our company', 'winnbrain-land').'</p>';
    $successMess .= '<p>'.__('Our specialists will contact you shortly', 'winnbrain-land').'</p>';

    if(!$username || !$email){
        $_SESSION['contactFormMessage'] .= $errorMess_2;
        return true;
        exit();
    }

    $title = $username;
    $content = $title?'<p>'.__('Full name', 'revocanda').': '.$title.'</p>':'';
    $content .= $phone?'<p>'.__('Phone', 'revocanda').': '.$phone.'</p>':'';
    $content .= $email?'<p>'.__('Email', 'revocanda').': '.$email.'</p>':'';
    $content .= $message?'<p>'.__('Message', 'revocanda').': '.$message.'</p>':'';

    //get admins emails
    $adminEmail = get_option('adminEmail');
    if($adminEmail && strpos($adminEmail, ',')>-1){
        $adminEmail = explode(',', $adminEmail);
        $adminEmail = array_filter($adminEmail);
    }

    $subjectText = 'Revocanda контактная форма';

    $ards = [
        'post_title' => $title,
        'post_content' => $content,
        'post_author' => 1,
        'post_status' => 'publish',
        'post_type' => 'contacts'
    ];

    $postId = wp_insert_post($ards);

    if($postId !== 0){
        //send to admin
        if(!is_array($adminEmail)){
            $to = trim($adminEmail);
            $subject = $subjectText;
            $headers[] = 'From: '.get_bloginfo('name').' <'.$to.'>'. "\r\n";
            $headers[] = 'content-type: text/html';
            if(!wp_mail($to, $subject, $content, $headers)){
                $_SESSION['contactFormMessage'] .= $errorMess_1;
            }
        }
        elseif(count($adminEmail)>0){
            foreach($adminEmail as $m){
                $to = trim($m);
                $subject = $subjectText;
                $headers[] = 'From: '.get_bloginfo('name').' <'.$to.'>'. "\r\n";
                $headers[] = 'content-type: text/html';
                wp_mail($to, $subject, $content, $headers);
            }
        }
        $_SESSION['contactFormMessage'] .= $successMess;
        return true;
        exit();
    }
    else{
        $_SESSION['contactFormMessage'] .= $errorMess_1;
        return true;
        exit();
    }

}

if(is_admin()){
    add_action('wp_ajax_nopriv_revocanda_get_shortcode', 'get_shortcode');
    add_action('wp_ajax_revocanda_get_shortcode', 'get_shortcode');
}
function get_shortcode(){
    $code = wp_strip_all_tags(@$_POST['data']);
    $code = str_replace('\"', '"', $code);
    echo do_shortcode($code);
    exit();
}

add_action('wp_ajax_nopriv_revocanda_get_select_list', 'getSelectList');
add_action('wp_ajax_revocanda_get_select_list', 'getSelectList');
function getSelectList(){
    $code = @$_POST['data'];
    if($code){
        $connected = @$code['connected'];
        $call = @$code['call'];
        $value = @$code['value'];
        if($connected && $call){
            $list = null;
            if($value){
                $list = call_user_func($call, $value);
            }
            elseif($value != null){
                $list = call_user_func($call);
            }
            if($list){
                $listJson = json_encode($list, true);
                echo '{"data":'.$listJson.', "connected":"'.$connected.'"}';
                exit();
            }
            else{
                echo '{"data":{}, "connected":"'.$connected.'"}';
                exit();
            }
        }
    }
}