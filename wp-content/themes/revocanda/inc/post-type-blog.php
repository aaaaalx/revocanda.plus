<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - blog
 * ================================
 *
 * */

//custom post type
function custom_post_type_blog(){
    $post_labels = [
        'name' => 'Блог',
        'singular_name' => 'Блог',
        'menu_name' => 'Блог',
        'name_admin_bar' => 'Блог'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'blog',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'author', 'editor', 'thumbnail'],
        'show_in_nav_menus' => true,
        'taxonomies' => ['category_blog'],
        'map_meta_cap' => true,
    ];

    $cat_labels = array(
        'name'              => 'Лицензии',
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );
    $cat_args = array(
        'labels'            => $cat_labels,
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'publicly_queryable'=> true,
        'show_tagcloud'     => false,
    );
    register_taxonomy( 'category_blog', ['blog'], $cat_args );
    register_post_type('blog', $post_args);
}
add_action( 'init', 'custom_post_type_blog', 0 );