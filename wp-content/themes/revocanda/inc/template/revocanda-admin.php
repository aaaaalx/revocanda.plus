<?php settings_errors(); ?>

<form method="post" action="options.php" class="revocanda-general-form">
    <?php add_thickbox(); ?>

    <?php settings_fields('revocanda-theme-group'); ?>
    <?php do_settings_sections('revocanda'); ?>
    <?php submit_button('Сохранить', 'primary', 'btnSubmit'); ?>
</form>
