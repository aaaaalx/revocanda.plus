<h1>Revocanda Sidebar Options</h1>
<?php settings_errors(); ?>
<?php
$name = esc_attr(get_option('name'));
$last_name = esc_attr(get_option('last_name'));
$fullname = $name.' '.$last_name;
$description = esc_attr(get_option('description'));
$profile_picture = esc_attr(get_option('profile_img'));
?>

<div class="revocanda-sidebar-preview">
    <div class="revocanda-sidebar">
        <div class="image-container">
            <div id="profile-picture-block" class="profile-picture">
                <?php if($profile_picture): ?>
                <img src="<?= $profile_picture; ?>" alt="Profile Picture">
                <?php endif; ?>
            </div>
        </div>
        <div class="sidebar-username"><?= $fullname; ?></div>
        <div class="sidebar-description"><?= $description; ?></div>
        <div class="icon-wraper">

        </div>
    </div>
</div>

<form id="sidebar-options-form" method="post" action="options.php" class="revocanda-general-form">
    <?php settings_fields('revocanda-sidebar-group'); ?>
    <?php do_settings_sections('revocanda-sidebar'); ?>
    <?php submit_button('Save changes', 'primary', 'btnSubmit'); ?>
</form>