<?php settings_errors(); ?>

<form method="post" action="options.php" class="revocanda-contact-form">
    <?php settings_fields('revocanda-contact-form-group'); ?>
    <?php do_settings_sections('revocanda-contact'); ?>
    <?php submit_button(); ?>
</form>

