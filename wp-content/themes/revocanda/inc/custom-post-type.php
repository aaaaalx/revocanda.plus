<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE
 * ================================
 *
 * */

/*
$contact = get_option('activate_contact');
if(@$contact == 1){
    add_action('init', 'revocanda_custom_post_type');//initialize(add) custom post type
    add_filter('manage_revocanda-contact_posts_columns', 'revocanda_set_contact_columns');//"manage_posts_columns" as first argument - to editing default posts list!
    add_action('manage_revocanda-contact_posts_custom_column', 'revocanda_contact_custom_column', 10, 2);//set custom description for columns. 10 - default sequence position. 2 - args count

    add_action('add_meta_boxes', 'revocanda_contact_add_meta_box');//add custom metabox
    add_action('save_post', 'revocanda_save_email_data');//save custom meta box
}

//contact cpt(custom post type)
function revocanda_custom_post_type(){
    $labels = [
        'name' => __('Messages', 'revocanda'),
        'singular_name' => __('Message', 'revocanda'),
        'menu_name' => __('Messages', 'revocanda'),
        'name_admin_bar' => __('Message', 'revocanda')
    ];
    $args = [
        'labels' => $labels,
        'show_ui' => true,
        'show_in_menu' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author'],
        'show_in_nav_menus' => true,
        'public' => false,
        'publicly_queryable' => false,
        'query_var' => false,
    ];
    register_post_type('revocanda-contact', $args);
}

function revocanda_set_contact_columns($columns){//setup custom columns
    $newColumns = [];
    $newColumns['title'] = __('Full name', 'revocanda');
    $newColumns['message'] = __('Message', 'revocanda');
    $newColumns['email'] = 'Email';
    $newColumns['date'] = __('Date');
    return $newColumns;
}
function revocanda_contact_custom_column($column, $post_id){//setup custom columns descriptions
    switch($column){
        case 'message':
            echo get_the_excerpt();
            break;
        case 'email':
            $value = get_post_meta($post_id, '_contact_email_value_key', true);//get custom meta box
            if($value){
                echo '<a href="mailto:'.$value.'" hreflang="ru">'.$value.'</a>';
            }
            break;
    }
}
*/
/*Contact meta boxes*/
/*
function revocanda_contact_add_meta_box(){
    add_meta_box('contact_email', 'User Email', 'revocanda_contact_email_callback', 'revocanda-contact', 'side', 'default');//wp prebuilt metal box adding function. "side" - position(normal|side|advanced). "default" - priority(high|default|low)
}

function revocanda_contact_email_callback($post){
    wp_nonce_field('revocanda_save_email_data', 'revocanda_contact_email_meta_box_nonce');//add unique verifying field
    $value = get_post_meta($post->ID, '_contact_email_value_key', true);//get custom meta box
    $value = apply_filters('the_title', $value);
    echo '<lable for="revocanda_contact_email_field">User Email Adress: </lable>';
    echo '<input type="text" id="revocanda_contact_email_field" name="revocanda_contact_email_field" value="'.esc_attr($value).'" size="25">';
}

function revocanda_save_email_data($post_id){

    if(!isset($_POST['revocanda_contact_email_meta_box_nonce'])){//check is metabox exist
        return;
    }
    if(wp_verify_nonce($_POST['revocanda_contact_email_meta_box_nonce'], 'revocanda_contact_add_meta_box')){//check is nonce exist
        return;
    }

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }
    if(!isset($_POST['revocanda_contact_email_field'])){//check is metabox field exist
        return;
    }

    $my_data = sanitize_text_field($_POST['revocanda_contact_email_field']);//get posted data from meta box field
    update_post_meta($post_id, '_contact_email_value_key', $my_data);//save data from meta box field
    
}
*/