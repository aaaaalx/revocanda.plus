<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME SUPPORT OPTIONS
 * ================================
 *
 * */
$options = get_option('post_formats');
$formats = ['aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'];
$output = [];
foreach ($formats as $format){
    if(isset($options[$format]) && $options[$format] == 1){
        $output[] = $format;
    }
}
if(!empty($options)){
    //add lost of post formats
    add_theme_support('post-formats', $output);
}

$header = get_option('custom_header');
if(@$header == 1){
    add_theme_support('custom-header');
}
$background = get_option('custom_background ');
if(@$background  == 1){
    add_theme_support('custom-background');
}