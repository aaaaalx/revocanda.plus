<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - EVENTS
 * ================================
 *
 * */

//custom post type
function custom_post_type_media(){
    $post_labels = [
        'name' => 'Медиа',
        'singular_name' => 'Медиа',
        'menu_name' => 'Медиа',
        'name_admin_bar' => 'Медиа'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'media',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author', 'thumbnail'],
        'show_in_nav_menus' => true,
        'rewrite' => array( 'slug' => 'media', 'with_front' => true ),
        'map_meta_cap' => true,
    ];
    register_post_type('media', $post_args);

}
add_action( 'init', 'custom_post_type_media', 0 );

//add meta boxes
function media_add_meta_box(){

    //video
    add_meta_box('media_video', 'Видео', 'media_video_callback', 'media', 'normal', 'default');

    //settings
    add_meta_box('media_settings', 'Настройки', 'media_settings_callback', 'media', 'normal', 'default');
}

function media_video_callback($post){
    wp_nonce_field('media_save_data', 'media_video_meta_box_nonce');//add unique verifying field
    $video = get_post_meta($post->ID, '_media_video_value_key', true);//get custom meta box
    $video_link = get_post_meta($post->ID, '_media_video_link_value_key', true);//get custom meta box
    $video_type = get_post_meta($post->ID, '_media_video_type_value_key', true);//get custom meta box
    $video_title = get_post_meta($post->ID, '_media_video_title_value_key', true);//get custom meta box
    $video_desc = get_post_meta($post->ID, '_media_video_desc_value_key', true);//get custom meta box
    $isVideoActive = get_post_meta($post->ID, '_media_video_active_value_key', true);//get custom meta box
    $videoStatus = get_post_meta($post->ID, '_media_video_status_value_key', true);//get custom meta box

    //is shown specialist section
    echo '<p><label for="media_video_active_value_key">Показывать видео: </label>';
    $checked = '';
    if($isVideoActive == 1){
        $checked = ' checked';
    }
    echo '<input id="media_video_active_value_key" type="checkbox" name="media_video_active_value_key" value="1"'.$checked.'></p>';

    echo '<p>Тип видео: </p>';
    echo '<input type="radio" name="media_video_type_value_key" value="video"'.(!$video_type||$video_type=='video'?' checked':'').'> Видео<br>';
    echo '<input type="radio" name="media_video_type_value_key" value="embed"'.($video_type=='embed'?' checked':'').'> Вставленное видео<br>';

    //video
    $Id = 'media_video_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Видео: </label>';
    echo '<div class="widget-block-img">';
    if($video){
        $mime = get_file_mime_type($video);
        if(getFileFormat($mime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$video.'" type="'.$mime.'">';
            echo '</video>';
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:video" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$video.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';

    //video embed
    echo '<p>Ссылка на видео для вставки: </p>';
    echo '<p><textarea id="media_video_link_value_key" class="widefat datafield" title="'.__('Embed video', 'revocanda').'" name="media_video_link_value_key">'.$video_link.'</textarea></p>';

    //title
    echo '<p>'.__('Title', 'revocanda').': </p>';
    echo '<p><input id="media_video_title_value_key" class="widefat datafield" title="Video description" type="text" name="media_video_title_value_key" value="'.$video_title.'"></p>';

}

function media_settings_callback($post){

    //settings
    wp_nonce_field('media_save_data', 'media_settings_meta_box_nonce');//add unique verifying field
    $background = get_post_meta($post->ID, '_media_settings_bg_value_key', true);//get custom meta box

    //background
    $Id = 'media_settings_bg_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">' . __('Background', 'revocanda') . ': </label>';
    echo '<div class="widget-block-img">';
    if($background){
        $backgroundMime = get_file_mime_type($background);
        if(getFileFormat($backgroundMime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$background.'" type="'.$backgroundMime.'">';
            echo '</video>';
        }
        else{
            if($background && strpos($background, ', ')>-1){
                //check is multi media
                $mediaStringA = explode(', ', $background);
                if(count($mediaStringA)>0){
                    add_thickbox();
                    $selectedMedia = '';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    foreach($mediaStringA as $item){
                        $mediaId = pippin_get_file_id($item);
                        if($mediaId){
                            $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                        }
                        echo '<a class="thickbox" href="'.$item.'?TB_iframe=false&width=100%&height=100%"><img src="'.$item.'"></a>';
                    }
                    if($selectedMedia){
                        echo '<script>'.$selectedMedia.'</script>';
                    }
                }
            }
            else{
                $mediaId = pippin_get_file_id($background);
                $selectedMedia = '';
                if($mediaId){
                    $selectedMedia = '<script>';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                    $selectedMedia .= '</script>';
                }
                add_thickbox();
                echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
                echo $selectedMedia;
            }
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';
}

function media_save_data($post_id){

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    //settings
    if(isset($_POST['media_settings_meta_box_nonce']) &&
        wp_verify_nonce($_POST['media_settings_meta_box_nonce'], 'media_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //settings background
    if(isset($_POST['media_settings_bg_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['media_settings_bg_value_key'];
        update_post_meta($post_id, '_media_settings_bg_value_key', $my_data);//save data from meta box field
    }

    //video settings
    if(isset($_POST['media_video_meta_box_nonce']) &&
        wp_verify_nonce($_POST['media_video_meta_box_nonce'], 'media_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //is video active
    if(@$_POST['media_video_active_value_key'] == 1){//check is meta box verifying exist
        update_post_meta($post_id, '_media_video_active_value_key', 1);//save data from meta box field
    }
    else{
        update_post_meta($post_id, '_media_video_active_value_key', '');//save data from meta box field
    }
    //video type
    if(isset($_POST['media_video_type_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['media_video_type_value_key'];
        update_post_meta($post_id, '_media_video_type_value_key', $my_data);//save data from meta box field
    }
    //video
    if(isset($_POST['media_video_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['media_video_value_key'];
        update_post_meta($post_id, '_media_video_value_key', $my_data);//save data from meta box field
    }
    //video embed
    if(isset($_POST['media_video_link_value_key'])){//check is meta box verifying exist
        $my_data = esc_attr($_POST['media_video_link_value_key']);
        update_post_meta($post_id, '_media_video_link_value_key', $my_data);//save data from meta box field
    }
    //video title
    if(isset($_POST['media_video_title_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['media_video_title_value_key'];
        update_post_meta($post_id, '_media_video_title_value_key', $my_data);//save data from meta box field
    }
    //video status
    if(@$_POST['media_video_active_value_key'] == 1 && isset($_POST['media_video_value_key']) || isset($_POST['media_video_link_value_key'])){//check is meta box verifying exist
        update_post_meta($post_id, '_media_video_status_value_key', 1);//save data from meta box field
    }
    else{
        update_post_meta($post_id, '_media_video_status_value_key', 0);//save data from meta box field
    }

}

add_action('add_meta_boxes', 'media_add_meta_box');//add custom metabox
add_action('save_post', 'media_save_data');//save custom meta box