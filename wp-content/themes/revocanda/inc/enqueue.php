<?php
/*
 * @package revocanda
 *
 * ================================
 * ADMIN PAGE
 * ================================
 *
 * */

function revocanda_load_admin_scripts($hook){
    /*
    if (strpos($hook, 'page_revocanda') === false) {
        return;
    }
    */

    //link choosing support (look js callWpLinkPicker func)
    wp_enqueue_script( 'wp-link' );
    /*
    wp_enqueue_style( 'editor-buttons' );
    wp_enqueue_script('wpdialogs-popup');
    wp_enqueue_style('wp-jquery-ui-dialog');
    */

    //wp_enqueue_style('thickbox');
    //wp_enqueue_script('thickbox');

    //datepicker
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
    wp_enqueue_style('jquery-ui');


    //enable wp color picker
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'underscore' );

    wp_register_style('revocanda_admin', get_template_directory_uri().'/css/revocanda.admin.css');
    wp_enqueue_style('revocanda_admin');

    //enable media files
    wp_enqueue_media();
    //enable visual editor
    wp_enqueue_editor();

    wp_register_script('revocanda_admin_script', get_template_directory_uri().'/js/revocanda.admin.js', ['jquery'], '1.0', true);
    wp_enqueue_script('revocanda_admin_script');

    //dropdown icons list
    wp_register_style('revocanda_dropdown', get_template_directory_uri().'/libs/ddslick-master/css/ddslick.css', [], '1.0', 'all');
    wp_enqueue_style('revocanda_dropdown');

    wp_register_script('revocanda_dropdown_script', get_template_directory_uri().'/libs/ddslick-master/js/jquery.ddslick.min.js', ['jquery'], '1.0', true);
    wp_enqueue_script('revocanda_dropdown_script');

}
add_action('admin_enqueue_scripts', 'revocanda_load_admin_scripts');

function revocanda_load_scripts($hook){

    wp_enqueue_style('wp-mediaelement');
    wp_enqueue_script('wp-mediaelement');

    //lightbox
    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');

    //css styles
    wp_register_style('user_googlefonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
    wp_enqueue_style('user_googlefonts');

    wp_register_style('user_libs', get_template_directory_uri().'/css/libs.css', [], false, 'all');
    wp_enqueue_style('user_libs');

    wp_register_style('user_style', get_template_directory_uri().'/css/style.css');
    wp_enqueue_style('user_style');

    wp_register_style('user_style_custom', get_template_directory_uri().'/css/style-custom.css');
    wp_enqueue_style('user_style_custom');

    //js scripts
    wp_register_script('revocanda_jquery_script', get_template_directory_uri().'/libs/js/jquery-3.1.1.min.js', '', '', true);
    wp_enqueue_script('revocanda_jquery_script');

    wp_register_script('revocanda_bootstrap_script', get_template_directory_uri().'/libs/js/bootstrap.min.js', '', '', true);
    wp_enqueue_script('revocanda_bootstrap_script');

    wp_register_script('revocanda_parallax', get_template_directory_uri().'/libs/js/parallax.js', '', '', true);
    wp_enqueue_script('revocanda_parallax');

    wp_register_script('revocanda_slick_script', get_template_directory_uri().'/libs/js/slick.js', '', '', true);
    wp_enqueue_script('revocanda_slick_script');

    wp_register_script('revocanda_slick_lightbox', get_template_directory_uri().'/libs/js/slick-lightbox.js', '', '', true);
    wp_enqueue_script('revocanda_slick_lightbox');

    wp_register_script('revocanda_jquery_validate', get_template_directory_uri().'/libs/js/jquery.validate.js', '', '', true);
    wp_enqueue_script('revocanda_jquery_validate');

    wp_register_script('revocanda_wow', get_template_directory_uri().'/libs/js/wow.min.js', '', '', true);
    wp_add_inline_script('revocanda_wow', 'new WOW().init();');
    wp_enqueue_script('revocanda_wow');

    wp_register_script('revocanda_maskedinput_script', get_template_directory_uri().'/libs/js/jquery.maskedinput.min.js', '', '', true);
    wp_enqueue_script('revocanda_maskedinput_script');

    wp_register_script('revocanda_main_script', get_template_directory_uri().'/js/main.js', '', '', true);
    wp_enqueue_script('revocanda_main_script');

    wp_register_script('user_main_script_custom', get_template_directory_uri().'/js/main-custom.js', '', '', true);
    wp_enqueue_script('user_main_script_custom');


}
add_action('wp_enqueue_scripts', 'revocanda_load_scripts');