<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - EVENTS
 * ================================
 *
 * */

//custom post type
function custom_post_type_events(){
    $post_labels = [
        'name' => 'Мероприятия',
        'singular_name' => 'Мероприятия',
        'menu_name' => 'Мероприятия',
        'name_admin_bar' => 'Мероприятия'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'events',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author', 'thumbnail'],
        'show_in_nav_menus' => true,
        'rewrite' => array( 'slug' => 'events', 'with_front' => true ),
        'map_meta_cap' => true,
    ];

    register_post_type('events', $post_args);
    flush_rewrite_rules();
}
add_action( 'init', 'custom_post_type_events', 0 );

//add meta boxes
function events_add_meta_box(){
    //events detals
    add_meta_box('events_detals', 'Детали', 'events_detals_callback', 'events', 'normal', 'default');

    //settings
    add_meta_box('events_settings', 'Настройки', 'events_settings_callback', 'events', 'normal', 'default');

    //video
    add_meta_box('events_video', 'Видео', 'events_video_callback', 'events', 'normal', 'default');
}

function events_video_callback($post){
    wp_nonce_field('events_save_data', 'events_video_meta_box_nonce');//add unique verifying field
    $video = get_post_meta($post->ID, '_events_video_value_key', true);//get custom meta box
    $video_link = get_post_meta($post->ID, '_events_video_link_value_key', true);//get custom meta box
    $video_type = get_post_meta($post->ID, '_events_video_type_value_key', true);//get custom meta box
    $video_title = get_post_meta($post->ID, '_events_video_title_value_key', true);//get custom meta box
    $video_desc = get_post_meta($post->ID, '_events_video_desc_value_key', true);//get custom meta box
    $isVideoActive = get_post_meta($post->ID, '_events_video_active_value_key', true);//get custom meta box

    //is shown specialist section
    echo '<p><label for="events_video_active_value_key">Показывать видео: </label>';
    $checked = '';
    if($isVideoActive == 1){
        $checked = ' checked';
    }
    echo '<input id="events_video_active_value_key" type="checkbox" name="events_video_active_value_key" value="1"'.$checked.'></p>';

    echo '<p>Тип видео: </p>';
    echo '<input type="radio" name="events_video_type_value_key" value="video"'.(!$video_type||$video_type=='video'?' checked':'').'> Видео<br>';
    echo '<input type="radio" name="events_video_type_value_key" value="embed"'.($video_type=='embed'?' checked':'').'> Вставленное видео<br>';

    //video
    $Id = 'events_video_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Видео: </label>';
    echo '<div class="widget-block-img">';
    if($video){
        $mime = get_file_mime_type($video);
        if(getFileFormat($mime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$video.'" type="'.$mime.'">';
            echo '</video>';
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:video" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$video.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';

    //video embed
    echo '<p>Ссылка на видео для вставки: </p>';
    echo '<p><textarea id="events_video_link_value_key" class="widefat datafield" title="'.__('Embed video', 'revocanda').'" name="events_video_link_value_key">'.$video_link.'</textarea></p>';

    //title
    echo '<p>'.__('Title', 'revocanda').': </p>';
    echo '<p><input id="events_video_title_value_key" class="widefat datafield" title="Video description" type="text" name="events_video_title_value_key" value="'.$video_title.'"></p>';

    //desc
    echo '<p>'.__('Description', 'revocanda').': </p>';
    echo '<p><textarea id="events_video_desc_value_key" class="widefat datafield" title="Video description" name="events_video_desc_value_key">'.$video_desc.'</textarea></p>';

}

function events_detals_callback($post){

    wp_nonce_field('events_save_data', 'events_detals_meta_box_nonce');//add unique verifying field
    $short_desc = get_post_meta($post->ID, '_events_short_desc_value_key', true);//get custom meta box
    $address = get_post_meta($post->ID, '_events_address_value_key', true);//get custom meta box
    $date = get_post_meta($post->ID, '_events_date_value_key', true);//get custom meta box
    $time = get_post_meta($post->ID, '_events_time_value_key', true);//get custom meta box
    $country = get_post_meta($post->ID, '_events_country_value_key', true);//get custom meta box
    $city = get_post_meta($post->ID, '_events_city_value_key', true);//get custom meta box
    $event_type = get_post_meta($post->ID, '_events_events_type_value_key', true);//get custom meta box

    //events types
    $eventsTypes = getEventsTypes();
    echo '<p>Тип мероприятий: </p>';
    echo '<select class="widefat" id="events_events_type_value_key" multiple="multiple" name="events_events_type_value_key[]">';
    if(count($eventsTypes)>0){
        echo '<option value=""></option>';
        foreach ($eventsTypes as $key => $item){
            $selected = '';
            if(count($event_type)>0){
                foreach ($event_type as $v){
                    if((int)$v == (int)$key){
                        $selected = ' selected';
                        break;
                    }
                }
            }
            echo '<option value="'.$key.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select><br><br>';

    //short description
    $ed_id = 'events_short_desc_value_key';
    $ed_name = $ed_id;
    echo '<label for="' . $ed_id . '">Краткое описание: </label>';
    $args = [
        'wpautop' => 0,
        'media_buttons' => 1,
        'textarea_name' => $ed_name,
        'textarea_rows' => 10,
        'tabindex' => null,
        'editor_css' => '',
        'editor_class' => $ed_id.' linkOnly',
        'teeny' => 1,
        'dfw' => 0,
        'tinymce' => 1,
        'drag_drop_upload' => false
    ];

    echo '<div class="wp-editor-conteiner">';
    wp_editor($short_desc, $ed_id, $args);
    echo '</div>';

    //country
    $countries = getCountries();
    echo '<p>Страна: </p>';
    echo '<select 
    class="widefat selectList" 
    id="events_country_value_key" 
    name="events_country_value_key" 
    data-connected="events_city_value_key" 
    data-call="getCitiesByCountry" 
    data-value="true">';
    echo '<option value=""></option>';
    if(count($countries)>0){
        foreach ($countries as $k => $item){
            $selected = '';
            if($country == $k){
                $selected = ' selected';
            }
            echo '<option value="'.$k.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select>';

    //city
    $cities = [];
    if($country){
        $cities = getCitiesByCountry($country);
    }
    echo '<p>Город: </p>';
    echo '<select class="widefat" id="events_city_value_key" name="events_city_value_key">';
    echo '<option value=""></option>';
    if(count($cities)>0){
        foreach ($cities as $k => $item){
            $selected = '';
            if($city == $k){
                $selected = ' selected';
            }
            echo '<option value="'.$k.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select>';

    //arddess
    echo '<p>Адрес: </p>';
    echo '<p><textarea id="events_address_value_key" class="widefat datafield" title="Address" type="text" name="events_address_value_key">'.$address.'</textarea></p>';

    //date
    $Id = 'events_date_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Дата: </label>';
    echo '<input class="widefat datePicker" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $date . '"></p>';
    //time
    $Id = 'events_time_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">Время: </label>';
    echo '<input class="widefat" id="' . $Id . '" type="time" name="' . $Name . '" value="' . $time . '"></p>';
}

function events_settings_callback($post){

    //settings
    wp_nonce_field('events_save_data', 'events_settings_meta_box_nonce');//add unique verifying field
    $background = get_post_meta($post->ID, '_events_settings_bg_value_key', true);//get custom meta box
    $past_event = get_post_meta($post->ID, '_events_settings_past_event_value_key', true);//get custom meta box

    //is event set as past
    echo '<p><label for="events_settings_past_event_value_key">Событие состоялось: </label>';
    $checked = '';
    if($past_event == 1){
        $checked = ' checked';
    }
    echo '<input id="events_settings_past_event_value_key" type="checkbox" name="events_settings_past_event_value_key" value="1"'.$checked.'></p>';


    //background
    $Id = 'events_settings_bg_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">' . __('Background', 'revocanda') . ': </label>';
    echo '<div class="widget-block-img">';
    if($background){
        $backgroundMime = get_file_mime_type($background);
        if(getFileFormat($backgroundMime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$background.'" type="'.$backgroundMime.'">';
            echo '</video>';
        }
        else{
            if($background && strpos($background, ', ')>-1){
                //check is multi media
                $mediaStringA = explode(', ', $background);
                if(count($mediaStringA)>0){
                    add_thickbox();
                    $selectedMedia = '';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    foreach($mediaStringA as $item){
                        $mediaId = pippin_get_file_id($item);
                        if($mediaId){
                            $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                        }
                        echo '<a class="thickbox" href="'.$item.'?TB_iframe=false&width=100%&height=100%"><img src="'.$item.'"></a>';
                    }
                    if($selectedMedia){
                        echo '<script>'.$selectedMedia.'</script>';
                    }
                }
            }
            else{
                $mediaId = pippin_get_file_id($background);
                $selectedMedia = '';
                if($mediaId){
                    $selectedMedia = '<script>';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                    $selectedMedia .= '</script>';
                }
                add_thickbox();
                echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
                echo $selectedMedia;
            }
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';
}

function events_save_data($post_id){

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    //settings
    if(isset($_POST['events_settings_meta_box_nonce']) &&
        wp_verify_nonce($_POST['events_settings_meta_box_nonce'], 'events_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //settings past event
    if(@$_POST['events_settings_past_event_value_key'] == 1){//check is meta box verifying exist
        update_post_meta($post_id, '_events_settings_past_event_value_key', 1);//save data from meta box field
    }
    else{
        update_post_meta($post_id, '_events_settings_past_event_value_key', '');//save data from meta box field
    }
    //settings background
    if(isset($_POST['events_settings_bg_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_settings_bg_value_key'];
        update_post_meta($post_id, '_events_settings_bg_value_key', $my_data);//save data from meta box field
    }

    //video settings
    if(isset($_POST['events_video_meta_box_nonce']) &&
        wp_verify_nonce($_POST['events_video_meta_box_nonce'], 'events_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //is video active
    if(@$_POST['events_video_active_value_key'] == 1){//check is meta box verifying exist
        update_post_meta($post_id, '_events_video_active_value_key', 1);//save data from meta box field
    }
    else{
        update_post_meta($post_id, '_events_video_active_value_key', '');//save data from meta box field
    }
    //video type
    if(isset($_POST['events_video_type_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_video_type_value_key'];
        update_post_meta($post_id, '_events_video_type_value_key', $my_data);//save data from meta box field
    }
    //video
    if(isset($_POST['events_video_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_video_value_key'];
        update_post_meta($post_id, '_events_video_value_key', $my_data);//save data from meta box field
    }
    //video embed
    if(isset($_POST['events_video_link_value_key'])){//check is meta box verifying exist
        $my_data = esc_attr($_POST['events_video_link_value_key']);
        update_post_meta($post_id, '_events_video_link_value_key', $my_data);//save data from meta box field
    }
    //video title
    if(isset($_POST['events_video_title_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_video_title_value_key'];
        update_post_meta($post_id, '_events_video_title_value_key', $my_data);//save data from meta box field
    }
    //video desc
    if(isset($_POST['events_video_desc_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_video_desc_value_key'];
        update_post_meta($post_id, '_events_video_desc_value_key', $my_data);//save data from meta box field
    }

    //events types, short desc, address, date, time
    if(isset($_POST['events_detals_meta_box_nonce']) &&
        wp_verify_nonce($_POST['events_detals_meta_box_nonce'], 'events_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //events types
    if(isset($_POST['events_events_type_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_events_type_value_key'];
        update_post_meta($post_id, '_events_events_type_value_key', $my_data);//save data from meta box field
    }
    //short description
    if(isset($_POST['events_short_desc_value_key'])){//check is meta box verifying exist
        $my_data = trim($_POST['events_short_desc_value_key']);
        update_post_meta($post_id, '_events_short_desc_value_key', $my_data);//save data from meta box field
    }
    //address
    if(isset($_POST['events_address_value_key'])){//check is meta box verifying exist
        $my_data = trim($_POST['events_address_value_key']);
        update_post_meta($post_id, '_events_address_value_key', $my_data);//save data from meta box field
    }
    //date
    if(isset($_POST['events_date_value_key'])){//check is meta box verifying exist
        $my_data = trim($_POST['events_date_value_key']);
        update_post_meta($post_id, '_events_date_value_key', $my_data);//save data from meta box field
    }
    //time
    if(isset($_POST['events_time_value_key'])){//check is meta box verifying exist
        $my_data = trim($_POST['events_time_value_key']);
        update_post_meta($post_id, '_events_time_value_key', $my_data);//save data from meta box field
    }
    //country
    if(isset($_POST['events_country_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_country_value_key'];
        update_post_meta($post_id, '_events_country_value_key', $my_data);//save data from meta box field
    }
    //city
    if(isset($_POST['events_city_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['events_city_value_key'];
        update_post_meta($post_id, '_events_city_value_key', $my_data);//save data from meta box field
    }

}

add_action('add_meta_boxes', 'events_add_meta_box');//add custom metabox
add_action('save_post', 'events_save_data');//save custom meta box

//do action on pos
function events_on_post_save( $post_id ) {
    // If this is just a revision, don't send the email.
    if ( wp_is_post_revision( $post_id ) )
        return;

    $post = get_post($post_id);
    if($post->post_type != 'events')
        return;

    $content = get_post_field('post_content', $post_id);
    $content = strip_tags($content, '<img><audio><video>');
    $content = trim(preg_replace("/&#?[a-z0-9]{2,8};/i","",$content));
    if($content){
        //set value to 1 in custom meta field that responsible for status
    }
}
add_action( 'save_post', 'events_on_post_save' );