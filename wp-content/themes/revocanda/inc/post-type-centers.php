<?php
/*
 * @package revocanda
 *
 * ================================
 * THEME CUSTOM POST TYPE - CENTERS
 * ================================
 *
 * */

//custom post type
function custom_post_type_centers(){
    $post_labels = [
        'name' => 'Центры',
        'singular_name' => 'Центры',
        'menu_name' => 'Центры',
        'name_admin_bar' => 'Центры'
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'centers',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author', 'thumbnail'],
        'show_in_nav_menus' => true,
        'map_meta_cap' => true,
    ];
    register_post_type('centers', $post_args);
}
add_action( 'init', 'custom_post_type_centers', 0 );

function centers_taxonomy_slug_rewrite_rules($wp_rewrite) {
    $rules['centers/(.+?)/_([^/]+)?$'] = 'index.php?category_centers=$matches[1]&post_type=centers&name=$matches[2]';
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'centers_taxonomy_slug_rewrite_rules');

//add meta boxes
function centers_add_meta_box(){

    //contacts
    add_meta_box('centers_contacts', 'Контакты', 'centers_contacts_callback', 'centers', 'normal', 'default');//wp prebuilt metal box adding function. "side" - position(normal|side|advanced). "default" - priority(high|default|low)
    //institution
    add_meta_box('centers_institution', 'Учреждение', 'centers_institution_callback', 'centers', 'normal', 'default');
    //settings
    add_meta_box('centers_settings', 'Настройки', 'centers_settings_callback', 'centers', 'normal', 'default');

}

function centers_contacts_callback($post){

    //contacts
    wp_nonce_field('centers_save_data', 'centers_contacts_meta_box_nonce');//add unique verifying field
    $address = get_post_meta($post->ID, '_centers_address_value_key', true);//get custom meta box
    $addressAdditionalInfo = get_post_meta($post->ID, '_centers_addressAdditionalInfo_value_key', true);//get custom meta box
    $coordinates = get_post_meta($post->ID, '_centers_coordinates_value_key', true);//get custom meta box
    $phones = get_post_meta($post->ID, '_centers_phones_value_key', true);//get custom meta box
    $emails = get_post_meta($post->ID, '_centers_emails_value_key', true);//get custom meta box
    $contacts = get_post_meta($post->ID, '_centers_contacts_value_key', true);//get custom meta box
    $country = get_post_meta($post->ID, '_centers_country_value_key', true);//get custom meta box
    $city = get_post_meta($post->ID, '_centers_city_value_key', true);//get custom meta box

    //country
    $countries = getCountries();
    echo '<p>Страна: </p>';
    echo '<select 
    class="widefat selectList" 
    id="centers_country_value_key" 
    name="centers_country_value_key" 
    data-connected="centers_city_value_key" 
    data-call="getCitiesByCountry" 
    data-value="true">';
    echo '<option value=""></option>';
    if(count($countries)>0){
        foreach ($countries as $k => $item){
            $selected = '';
            if($country == $k){
                $selected = ' selected';
            }
            echo '<option value="'.$k.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select>';

    //city
    $cities = [];
    if($country){
        $cities = getCitiesByCountry($country);
    }
    echo '<p>Город: </p>';
    echo '<select class="widefat" id="centers_city_value_key" name="centers_city_value_key">';
    echo '<option value=""></option>';
    if(count($cities)>0){
        foreach ($cities as $k => $item){
            $selected = '';
            if($city == $k){
                $selected = ' selected';
            }
            echo '<option value="'.$k.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select>';

    //address
    echo '<p>Адрес: </p>';
    echo '<p><textarea id="centers_address_value_key" class="widefat datafield" title="Адрес" type="text" name="centers_address_value_key">'.$address.'</textarea></p>';
    if($address && !$coordinates){
        echo '<p>Не удалось получить координаты по указанному адресу</p>';
    }

    //address
    echo '<p>Адрес (Дополнительная информация):</p>';
    echo '<p><textarea id="centers_addressAdditionalInfo_value_key" class="widefat datafield" title="Адрес (Дополнительная информация)" type="text" name="centers_addressAdditionalInfo_value_key">'.$addressAdditionalInfo.'</textarea></p>';

    //coordinates
    echo '<p>Координаты:</p>';
    echo '<p><input class="widefat datafield" title="Координаты" type="text" name="centers_coordinates_value_key" value="'.$coordinates.'" /></p>';

    //phones
    echo '<p>Телефоны: </p>';
    $pattern = '<p><input [class="widefat datafield"/] [title="Phone"/] [type="text"/] [name="phone"/] /></p>';
    $pattern .= '<p><input [class="widefat datafield"/] [title="Phone Link"/] [type="text"/] [name="phone_link"/] /></p>';
    insertDynamicField('centers_phones_value_key', __('Add block Field', 'revocanda'), $phones, $pattern);

    //emails
    echo '<p>Emails: </p>';
    $pattern = '<p><input [class="widefat datafield"/] [title="Email"/] [type="text"/] [name="email"/] /></p>';
    $pattern .= '<p><input [class="widefat datafield"/] [title="Email Link"/] [type="text"/] [name="email_link"/] /></p>';
    insertDynamicField('centers_emails_value_key', __('Add block Field', 'revocanda'), $emails, $pattern);

    //other contacts
    echo '<p>Друге контакты: </p>';
    $pattern = '<p><input [class="widefat datafield"/] [title="Contacts"/] [type="text"/] [name="contacts"/] /></p>';
    $pattern .= '<p><input [class="widefat datafield"/] [title="Contacts Link"/] [type="text"/] [name="contacts_link"/] /></p>';
    $pattern .= '<p><select [class="widefat datafield"/] [title="Icon"/] [name="icon"/] [data="list:getContactsIcons"/] /></p>';
    insertDynamicField('centers_contacts_value_key', __('Add block Field', 'revocanda'), $contacts, $pattern);

}

function centers_institution_callback($post){

    //institution
    wp_nonce_field('centers_save_data', 'centers_institution_meta_box_nonce');//add unique verifying field
    $value = get_post_meta($post->ID, '_centers_institution_value_key', true);//get custom meta box

    $institutions = getInstitutions();

    echo '<select class="widefat" id="centers_institution_value_key" multiple="multiple" name="centers_institution_value_key[]">';
    if(count($institutions)>0){
        echo '<option value=""></option>';
        foreach ($institutions as $key => $institution){
            $selected = '';
            if(count($value)>0){
                foreach ($value as $v){
                    if((int)$v == $key){
                        $selected = ' selected';
                        break;
                    }
                }
            }
            echo '<option value="'.$key.'"'.$selected.'>'.$institution.'</option>';
        }
    }
    echo '</select>';
}

function centers_settings_callback($post){

    //settings
    wp_nonce_field('centers_save_data', 'centers_settings_meta_box_nonce');//add unique verifying field
    $specialistsOn = get_post_meta($post->ID, '_centers_settings_specialists_on_value_key', true);//get custom meta box
    $contactsOn = get_post_meta($post->ID, '_centers_settings_contacts_on_value_key', true);//get custom meta box
    $background = get_post_meta($post->ID, '_centers_settings_bg_value_key', true);//get custom meta box

    //is shown specialist section
    echo '<p><label for="centers_settings_specialists_on_value_key">Показывать специалистов: </label>';
    $checked = '';
    if($specialistsOn == 1){
        $checked = ' checked';
    }
    echo '<input id="centers_settings_specialists_on_value_key" type="checkbox" name="centers_settings_specialists_on_value_key" value="1"'.$checked.'></p>';

    //is shown contacts section
    echo '<p><label for="centers_settings_contacts_on_value_key">Показывать контакты: </label>';
    $checked = '';
    if($contactsOn == 1){
        $checked = ' checked';
    }
    echo '<input id="centers_settings_contacts_on_value_key" type="checkbox" name="centers_settings_contacts_on_value_key" value="1"'.$checked.'></p>';

    //background
    $Id = 'centers_settings_bg_value_key';
    $Name = $Id;
    echo '<p><label for="' . $Id . '">' . __('Background', 'revocanda') . ': </label>';
    echo '<div class="widget-block-img">';
    if($background){
        $backgroundMime = get_file_mime_type($background);
        if(getFileFormat($backgroundMime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$background.'" type="'.$backgroundMime.'">';
            echo '</video>';
        }
        else{
            if($background && strpos($background, ', ')>-1){
                //check is multi media
                $mediaStringA = explode(', ', $background);
                if(count($mediaStringA)>0){
                    add_thickbox();
                    $selectedMedia = '';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    foreach($mediaStringA as $item){
                        $mediaId = pippin_get_file_id($item);
                        if($mediaId){
                            $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                        }
                        echo '<a class="thickbox" href="'.$item.'?TB_iframe=false&width=100%&height=100%"><img src="'.$item.'"></a>';
                    }
                    if($selectedMedia){
                        echo '<script>'.$selectedMedia.'</script>';
                    }
                }
            }
            else{
                $mediaId = pippin_get_file_id($background);
                $selectedMedia = '';
                if($mediaId){
                    $selectedMedia = '<script>';
                    $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                    $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                    $selectedMedia .= '</script>';
                }
                add_thickbox();
                echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
                echo $selectedMedia;
            }

        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
    echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
    echo '</p>';
}

function centers_save_data($post_id){

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    //institution
    if(isset($_POST['centers_institution_meta_box_nonce']) &&
        wp_verify_nonce($_POST['centers_institution_meta_box_nonce'], 'centers_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['centers_institution_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_institution_value_key'];
        update_post_meta($post_id, '_centers_institution_value_key', $my_data);//save data from meta box field
    }

    //settings
    if(isset($_POST['centers_settings_meta_box_nonce']) &&
        wp_verify_nonce($_POST['centers_settings_meta_box_nonce'], 'centers_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //settings specialists
    if(@$_POST['centers_settings_specialists_on_value_key'] == 1){//check is meta box verifying exist
        update_post_meta($post_id, '_centers_settings_specialists_on_value_key', 1);//save data from meta box field
    }
    else{
        update_post_meta($post_id, '_centers_settings_specialists_on_value_key', '');//save data from meta box field
    }
    //settings contacts
    if(@$_POST['centers_settings_contacts_on_value_key'] == 1){//check is meta box verifying exist
        update_post_meta($post_id, '_centers_settings_contacts_on_value_key', 1);//save data from meta box field
    }
    else{
        update_post_meta($post_id, '_centers_settings_contacts_on_value_key', '');//save data from meta box field
    }
    //settings background
    if(isset($_POST['centers_settings_bg_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_settings_bg_value_key'];
        update_post_meta($post_id, '_centers_settings_bg_value_key', $my_data);//save data from meta box field
    }

    //address, phones, emails, contacts
    if(isset($_POST['centers_contacts_meta_box_nonce']) &&
        wp_verify_nonce($_POST['centers_contacts_meta_box_nonce'], 'centers_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    //country
    if(isset($_POST['centers_country_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_country_value_key'];
        update_post_meta($post_id, '_centers_country_value_key', $my_data);//save data from meta box field
    }
    //city
    if(isset($_POST['centers_city_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_city_value_key'];
        update_post_meta($post_id, '_centers_city_value_key', $my_data);//save data from meta box field
    }
    //address
    $post_coordinates = $_POST['centers_addressAdditionalInfo_value_key']??null;
    if(isset($_POST['centers_address_value_key'])){//check is meta box verifying exist
        $citiesAndCountries = getCities(true);
        $old_val = trim(get_post_meta($post_id, '_centers_address_value_key', true));//get custom meta box
        $my_data = trim($_POST['centers_address_value_key']);
        $country = get_post_meta($post_id, '_centers_country_value_key', true);//get custom meta box
        $country = isset($citiesAndCountries['countries'][$country])?$citiesAndCountries['countries'][$country]['country']:'';
        $city = get_post_meta($post_id, '_centers_city_value_key', true);//get custom meta box
        $city = isset($citiesAndCountries['cities'][$city])?$citiesAndCountries['cities'][$city]['city']:'';
        $coordinates_old = get_post_meta($post_id, '_centers_coordinates_value_key', true);

        $full_address_old = '';
        if($country){
            $full_address_old = $country;
        }
        if($city){
            $full_address_old .= $full_address_old?', ':'';
            $full_address_old .= $city;
        }
        if($old_val){
            $full_address_old .= $full_address_old?', ':'';
            $full_address_old .= $old_val;
        }
        $full_address = '';
        if($country){
            $full_address = $country;
        }
        if($city){
            $full_address .= $full_address?', ':'';
            $full_address .= $city;
        }
        if($my_data){
            $full_address .= $full_address?', ':'';
            $full_address .= $my_data;
        }

        if($full_address && $full_address_old != $full_address || $full_address && !$coordinates_old || $post_coordinates != $coordinates_old){
            $coordinates = getCoordinates($full_address);
            update_post_meta($post_id, '_centers_coordinates_value_key', $coordinates);//save data from meta box field
        }

        update_post_meta($post_id, '_centers_address_value_key', $my_data);//save data from meta box field
    }
    //addressAdditionalInfo
    if(isset($_POST['centers_addressAdditionalInfo_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_addressAdditionalInfo_value_key'];
        update_post_meta($post_id, '_centers_addressAdditionalInfo_value_key', $my_data);//save data from meta box field
    }

    //coordinates
    if(!empty($_POST['centers_coordinates_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_coordinates_value_key'];
        update_post_meta($post_id, '_centers_coordinates_value_key', $my_data);//save data from meta box field
    }

    //phones
    if(isset($_POST['centers_phones_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_phones_value_key'];
        update_post_meta($post_id, '_centers_phones_value_key', $my_data);//save data from meta box field
    }
    //emails
    if(isset($_POST['centers_emails_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_emails_value_key'];
        update_post_meta($post_id, '_centers_emails_value_key', $my_data);//save data from meta box field
    }
    //contacts
    if(isset($_POST['centers_contacts_value_key'])){//check is meta box verifying exist

        $my_data = $_POST['centers_contacts_value_key'];
        update_post_meta($post_id, '_centers_contacts_value_key', $my_data);//save data from meta box field
    }

}

add_action('add_meta_boxes', 'centers_add_meta_box');//add custom metabox
add_action('save_post', 'centers_save_data');//save custom meta box