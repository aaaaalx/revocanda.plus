<?php
/**
* The template for displaying contacts
*
* Template Name: Страница контактов
* Template Post Type: page
*
* @package Revocanda
* @since 1.0
* @version 1.0
*/
get_header();



global $isMobile;

if ($isMobile) {
    $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg-mobile.png);"';
} else {
    $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg.png);"';
}

?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/quiz.css'; ?>?v=1.01">

    <!--begin section-intro-->
    <section class="section-poster-intro"<?= $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>
                                    Тест для проверки анорексии и булимии
								</span>
							</span>
                            <p><?= get_the_title(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section our-contacts-->
    <section class="section-our-contacts">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p>Уважаемый испытуемый, вашему вниманию предоставлен тест, благодаря которому, вы можете обнаружить у себя
                        предрасположенность к развитию анорексии, булимии. Ответьте на ниже предложенные вопросы «Да», «Нет», «Иногда» и
                        после мы подведем итог. Тест является абсолютно анонимным и вся информация строго конфиденциальная.</p>
                    <div id="quiz"></div>
                    <div class="show-after-result">
                        <h2>Результат</h2>
                        <div class="enter"></div>
                        <a href="/" class="btn btn-main">Вернуться на главную</a>
                    </div>
                </div>
            </div>
        </div>

<script src="<?php echo get_template_directory_uri() . '/js/quiz.js'; ?>?v=1.0.1"></script>
<?php get_footer(); ?>