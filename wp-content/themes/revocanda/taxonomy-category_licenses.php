<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

$cat = get_queried_object()?get_queried_object():false;
$args = array(
    'taxonomy' => 'category_licenses',
    'hide_empty' => false,
);
$terms = get_terms( $args );
$currentCat = new stdClass();
$currentCat->term_id = null;
$currentCat->name = '';
$queriedCat = (int)@$_GET['category'];
if(is_array($terms) && count($terms)>0 && !$queriedCat){
    $currentCat = $terms[0];
}
elseif (is_array($terms) && count($terms)>0 && $queriedCat){
    foreach ($terms as $term){
        if($term->term_id === $queriedCat){
            $currentCat = $term;
            break;
        }
    }
}

$args = [
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'licenses',
    'genre'            => 'category_licenses',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
];
if($currentCat->term_id){
    $args['tax_query'] = [
        [
            'taxonomy' => 'category_licenses',
            'field' => 'term_id',
            'terms' => $currentCat->term_id,
            'include_children' => false
        ]
    ];
}
$posts_array = get_posts( $args );
$res = getPaged($posts_array);
$posts_array = $res['posts_array'];

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>
    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span><?= $currentCat->name; ?></span>
							</span>
                            <p><?= $currentCat->name; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-gallery-page-->
    <section class="section-gallery-page">
        <div class="container">
            <div class="row">

                <?php if(is_array($terms) && count($terms)>0): ?>
                <!--begin page-nav-wrap-->
                <div class="col-xs-12">
                    <div class="page-nav-wrap">
                        <ul class="slider-page-nav">
                            <?php
                            foreach ($terms as $term){
                                $active = $term->term_id === $currentCat->term_id?' active':'';
                                echo '<li class="item'.$active.'"><a href="/licenses?category='.$term->term_id.'">'.$term->name.'</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <!--end page-nav-wrap-->
                <?php endif; ?>

                <!--begin gallery-items-->
                <?php if(count($posts_array)>0): ?>

                <div class="col-xs-12">
                    <div class="tab-description">
                        <p>Нажмите на кнопку в правом верхнем углу, чтобы увидеть подробную информацию</p>
                    </div>
                    <div class="row">
                        <div class="lightbox-wrap">

                            <?php foreach($posts_array as $post): ?>
                            <?php
                            $url = get_the_post_thumbnail_url($post->ID);
                            $image = get_the_post_thumbnail_url($post->ID, 'licenses-image-hard');
                            $image = $image?$image:get_stylesheet_directory_uri().'/img/No-photo_380x534.jpg';

                            $title = get_the_title($post->ID);
                            $date =  get_the_date('m.d.Y', $post->ID);
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="list-item-box-3">
                                    <?php if($image): ?>
                                        <?php if($url): ?>
                                        <a href="<?= $url; ?>">
                                        <?php endif; ?>
                                            <img src="<?= $image; ?>" alt="">
                                        <?php if($url): ?>
                                        </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <div class="info-label"></div>
                                    <div class="gallery-item-description">
                                        <div class="item-close-button"></div>
                                        <div class="inner-wrap">
                                            <div class="item-caption">
                                                <p><?= $title; ?></p>
                                            </div>
                                            <div class="divider"></div>
                                            <?php //<div class="item-date">
                                                //<span> $date </span>
                                             //</div> ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
                <?php else: ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="list-item-box-2">
                            Нет материалов для отображения
                        </div>
                    </div>
                <?php endif; ?>
                <!--end gallery-items-->

                <?php getPaganation($res); ?>

            </div>
        </div>
    </section>
    <!--end section-gallery-page-->
<?php
get_footer();
?>