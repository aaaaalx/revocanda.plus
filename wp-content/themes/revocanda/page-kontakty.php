<?php
/**
* The template for displaying contacts
*
* Template Name: Страница контактов
* Template Post Type: page
*
* @package Revocanda
* @since 1.0
* @version 1.0
*/
get_header();

global $isMobile;

if($isMobile){
    $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg-mobile.png);"';
}
else{
    $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg.png);"';
}

$country_filter = (int)@$_GET['country'];
$city_filter = $country_filter?(int)@$_GET['city']:'';
$institution_filter = (int)@$_GET['institution'];

$centers = getCenters();

$institutions = getInstitutions();
$countries = getCountries();

$cities = [];
$currentCity = '';
$currentCountry = '';
$citiesAndCountries = getCities(true);
if($country_filter){
    $cities = getCitiesByCountry($country_filter);
    if(!isset($cities[$city_filter])){
        $city_filter = '';
    }
    else{
        $currentCity = $cities[$city_filter];
    }
    $currentCountry = $citiesAndCountries['countries'][$country_filter]['country'];
}
$locationCoordinates = '';
if($city_filter && $country_filter){
    $locationCoordinates = $currentCountry.', '.$currentCity;
}
elseif($country_filter){
    $locationCoordinates = $currentCountry;
}
if($locationCoordinates){
    $locationCoordinates = getCoordinates($locationCoordinates);
}

$map_zoom = 7;//default settings
$map_start_pos = [50.216457, 30.663533];//[42.2626219, 14.9862552];//default settings

$centersString = '';
$markers = [];

if(is_array($centers) && count($centers)>0){

    //start hidden content
    $centersString .= '<div id="all_centers" class="all_centers">';

    //start items
    foreach($centers as $item){
        $title = $item->post_title;

        $address = get_post_meta($item->ID, '_centers_address_value_key', true);
        $addressAdditionalInfo = get_post_meta($item->ID, '_centers_addressAdditionalInfo_value_key', true);
        $coordinates = get_post_meta($item->ID, '_centers_coordinates_value_key', true);
        $country = get_post_meta($item->ID, '_centers_country_value_key', true);//get custom meta box
        $countryCont = isset($citiesAndCountries['countries'][$country])?$citiesAndCountries['countries'][$country]['country']:'';
        $city = get_post_meta($item->ID, '_centers_city_value_key', true);//get custom meta box
        $cityCont = isset($citiesAndCountries['cities'][$city])?$citiesAndCountries['cities'][$city]['city']:'';
        $full_address = '';
        if($countryCont){
            $full_address = $countryCont;
        }
        if($cityCont){
            $full_address .= $full_address?', ':'';
            $full_address .= $cityCont;
        }
        if($address){
            $full_address .= $full_address?', ':'';
            $full_address .= $address;
        }
        if($full_address && $addressAdditionalInfo){
            $full_address = $full_address.', '.$addressAdditionalInfo;
        }
        $address = $full_address;

        $institutionsArr = get_post_meta($item->ID, '_centers_institution_value_key', true);

        if(!$coordinates){
            continue;
        }

        //filters
        if($institution_filter){
            $condition = false;
            if(is_array($institutionsArr) && count($institutionsArr)>0){
                foreach ($institutionsArr as $institutionStr){
                    if($institution_filter == $institutionStr){
                        $condition = true;
                        break;
                    }
                }
            }
            if(!$condition){
                continue;
            }
        }
        if($city_filter){
            if($city_filter != $city){
                continue;
            }
            else{
                $map_zoom = 10;
                //$coordinatesArr = explode(',', $coordinates);
                //$map_start_pos = [$coordinatesArr[0], $coordinatesArr[1]];
                if($locationCoordinates && strpos($locationCoordinates, ',')>-1){
                    $coordinatesArr = explode(',', $locationCoordinates);
                    $map_start_pos = [$coordinatesArr[0], $coordinatesArr[1]];
                }
            }
        }
        elseif($country_filter){
            if($country_filter != $country){
                continue;
            }
            else{
                $map_zoom = 5;
                //$coordinatesArr = explode(',', $coordinates);
                //$map_start_pos = [$coordinatesArr[0], $coordinatesArr[1]];
                if($locationCoordinates && strpos($locationCoordinates, ',')>-1){
                    $coordinatesArr = explode(',', $locationCoordinates);
                    $map_start_pos = [$coordinatesArr[0], $coordinatesArr[1]];
                }
            }
        }
        //filters end

        $markers[$item->ID]['id'] = $item->ID;//add center's id to markers
        $markers[$item->ID]['title'] = $title;//add titles to markers
        $markers[$item->ID]['address'] = preg_replace('/(\>)\s*(\<)/m', '$1$2', $address);//add address to markers
        $markers[$item->ID]['coordinates'] = explode(',', $coordinates);//add coordinates to markers
        $markers[$item->ID]['country'] = $country;
        $markers[$item->ID]['city'] = $city;
        $markers[$item->ID]['institution'] = $institutionsArr?implode(',', $institutionsArr):'';

        $phones = get_post_meta($item->ID, '_centers_phones_value_key', true);
        $phones = $phones?json_decode($phones,true):[];

        $markers[$item->ID]['phones'] = '';

        $emails = get_post_meta($item->ID, '_centers_emails_value_key', true);
        $emails = $emails?json_decode($emails,true):[];

        $contacts = get_post_meta($item->ID, '_centers_contacts_value_key', true);
        $contacts = $contacts?json_decode($contacts,true):[];

        //start items content
        $centersString .= '<div id="item_'.$item->ID.'" class="all_centers_item">';

        //title
        $centersString .= '<div class="col-lg-8 col-x-12"><h3>'.$title.'</h3></div>';
        //link to center
        $centersString .= '<div class="col-lg-4 col-x-12"><a href="'.get_permalink($item->ID).'" hreflang="ru" class="main-button blue-button">Подробнее о центре</a></div>';
        $centersString .= '<div class="col-xs-12"><div class="divider"></div></div>';
        //address
        if($address){
            $centersString .= '<div class="col-lg-3 col-sm-6 col-xs-12"><div class="address-box">'.$address.'</div></div>';
        }
        //phones
        if(is_array($phones) && count($phones)>0){
            $centersString .= '<div class="col-lg-3 col-sm-6 col-xs-12"><div class="phone-box">';
            foreach ($phones as $i){
                if(!$i['phone_link']){
                    $i['phone_link'] = 'tel:'.str_replace(' ', '',$i['phone']);
                }
                $centersString .= '<a href="'.$i['phone_link'].'" hreflang="ru">'.$i['phone'].'</a>';
                $markers[$item->ID]['phones'] .= '<p>'.$i['phone'].'</p>';//add phones to markers
            }
            $centersString .= '</div></div>';
        }
        //emails
        if(is_array($emails) && count($emails)>0){
            $centersString .= '<div class="col-lg-3 col-sm-6 col-xs-12"><div class="email-box">';
            foreach ($emails as $i){
                if(!$i['email_link']){
                    $i['email_link'] = 'mailto:'.str_replace(' ', '',$i['email']);
                }
                $centersString .= '<a href="'.$i['email_link'].'" hreflang="ru">'.$i['email'].'</a>';
            }
            $centersString .= '</div></div>';
        }
        //other contacts
        if(is_array($contacts) && count($contacts)>0){
            $centersString .= '<div class="col-lg-3 col-sm-6 col-xs-12">';
            foreach ($contacts as $i){
                $centersString .= '<div class="contact-box '.$i['icon'].'">';
                if(isset($i['contacts_link'])){
                    $centersString .= '<a href="'.$i['contacts_link'].'>" hreflang="ru">';
                    $centersString .= $i['contacts'];
                    $centersString .= '</a>';
                }
                else{
                    $centersString .= '<span>'.$i['contacts'].'</span>';
                }
                $centersString .= '</div>';
            }
            $centersString .= '</div>';
        }

        //end items content
        $centersString .= '</div>';
    }
    //end items

    //end hidden content
    $centersString .= '</div>';
}
?>

    <!-- hidden content -->
    <?= $centersString; ?>
    <!-- hidden content end -->

    <!--begin section-intro-->
    <section class="section-poster-intro"<?= $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>
									Контакты
								</span>
							</span>
                            <p><?= get_the_title(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section our-contacts-->
    <section class="section-our-contacts">
        <div class="container">
            <div class="row">
                <!--begin filters-->
                <div class="col-xs-12">
                    <div class="list-filter-group">
                        <div class="row">
                            <form id="filter" method="get">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select name="institution" id="institution_filter" class="select-2 selectListGET">
                                        <option value>Офис и Стационарный центр</option>
                                        <?php
                                        if(count($institutions)>0) {
                                            foreach ($institutions as $k => $item) {
                                                $selected = '';
                                                if ($institution_filter == $k) {
                                                    $selected = ' selected';
                                                }
                                                echo '<option value="' . $k . '"' . $selected . '>' . $item . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select name="country" id="country_filter" class="select-2 selectListGET">
                                        <option value>Страна</option>
                                        <?php
                                        if(count($countries)>0) {
                                            foreach ($countries as $k => $item) {
                                                $selected = '';
                                                if ($country_filter == $k) {
                                                    $selected = ' selected';
                                                }
                                                echo '<option value="' . $k . '"' . $selected . '>' . $item . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select name="city" id="city_filter" class="select-2 selectListGET">
                                        <option value>Город</option>
                                        <?php
                                        if(count($cities)>0) {
                                            foreach ($cities as $k => $item) {
                                                $selected = '';
                                                if ($city_filter == $k) {
                                                    $selected = ' selected';
                                                }
                                                echo '<option value="' . $k . '"' . $selected . '>' . $item . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--end filters-->
            </div>
        </div>

        <div class="container-fluid no-padding">
            <div class="col-xs-12 no-padding">
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="container content-wrap">
                        <div class="row">
                            <div class="col-lg-9 col-md-8 col-xs-12"><?= get_the_content(); ?></div>
                        </div>
                    </div>
                <?php endwhile; ?>

                <?php if(count($markers)<1): ?>
                    <div class="container content-wrap">
                        <div class="row" style="text-align: center;">
                            По вашему запросу ничего не найдено!
                        </div>
                    </div>
                <?php endif; ?>

                <div class="map-container">
                    <div id="map-canvas-list">

                    </div>
                </div>
                <script>
                    if (document.getElementById('map-canvas-list')) {
                        function initMap() {
                            var locations = [
                                <?php
                                $activeMarker = 0;
                                if(count($markers)>0){
                                    $str = '';
                                    $contactsCenters = get_option('field_activeContacts');
                                    $index = 0;
                                    foreach ($markers as $marker){
                                        /*if(@$marker['id']==$contactsCenters){
                                            $activeMarker = $index;
                                        }*/
                                        $str = $str?$str.',':$str;
                                        $cont = '<h4>'.addslashes($marker['title']).'</h4><p>'.addslashes($marker['address']).'</p>'.addslashes($marker['phones']);
                                        $str .= '['.$marker['id'].',"'.$cont.'",'.$marker['coordinates'][0].','.$marker['coordinates'][1].']';
                                        $index++;
                                    }
                                    echo $str;
                                }
                                ?>
                            ];
                            var map = new google.maps.Map(document.getElementById('map-canvas-list'), {
                                zoom: <?= $map_zoom; ?>,
                                center: new google.maps.LatLng(<?= $map_start_pos[0]; ?>, <?= $map_start_pos[1]; ?>),
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            });

                            var infowindow = new google.maps.InfoWindow();

                            var marker, i;

                            for (i = 0; i < locations.length; i++) {
                                marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(locations[i][2], locations[i][3]),
                                    map: map,
                                    location_id: locations[i][0]
                                });

                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                    return function () {
                                        infowindow.setContent(locations[i][1]);
                                        infowindow.open(map, marker);
                                        var center = $('#item_'+marker.location_id);
                                        var inserto = $('#insert_content');
                                        if(center.length && inserto.length){
                                            inserto.slideUp(100);
                                            inserto.html(center.html());
                                            inserto.show(300);
                                        }
                                    }
                                })(marker, i));

                                if(i==<?= $activeMarker; ?>){
                                    setContacts(<?= $activeMarker; ?>);
                                }

                            }

                            function setContacts(i) {
                                infowindow.setContent(locations[i][1]);
                                infowindow.open(map, marker);
                                var center = $('#item_'+marker.location_id);
                                var inserto = $('#insert_content');
                                if(center.length && inserto.length){
                                    inserto.slideUp(100);
                                    inserto.html(center.html());
                                    inserto.show(300);
                                }
                            }

                        }
                    }
                </script>
                <?php
                $key = get_option('field_mapKey');
                wp_register_script('user_map_script', 'https://maps.googleapis.com/maps/api/js?key='.$key.'&callback=initMap', '', '', true);
                wp_enqueue_script('user_map_script');
                ?>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="container">
            <div class="row">

                <div class="col-xs-12">
                    <div class="center-item-wrap">
                        <div id="insert_content" class="row"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end section our-contacts-->

<?php if(function_exists('dynamic_sidebar') && is_active_sidebar('contacts_page')): ?>
    <?php dynamic_sidebar('contacts_page') ?>
<?php endif; ?>

<?php get_footer(); ?>