<?php
//get current template file name
global $template;
$currTemplate = substr($template, strrpos($template, '/')+1);
$addStyles = '';
if(is_page() || $currTemplate == '404.php' || $currTemplate == 'single-events.php'){
    $addStyles = ' class="footer-grey"';
}
?>
<!--begin footer-->
<footer<?=$addStyles;?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-sm-6">
                        <a href="#" hreflang="ru"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-footer.png" alt=""></a>
                    </div>
                    <div class="col-lg-12 col-sm-6">
                        <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('footer_content')): ?>
                            <p><?php dynamic_sidebar('footer_content') ?></p>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="footer-links">
                    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('footer_block_1')): ?>
                        <p><?php dynamic_sidebar('footer_block_1') ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="footer-links">
                    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('footer_block_2')): ?>
                        <p><?php dynamic_sidebar('footer_block_2') ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12">
                <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('soc')): ?>
                    <p><?php dynamic_sidebar('soc') ?></p>
                <?php endif; ?>
                <div class="copyrights">
                    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('copyright')): ?>
                    <p><?php dynamic_sidebar('copyright') ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--end footer-->

<!-- begin modal -->

<!--begin modal testimonials-video-->
<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span id="closebutton" aria-hidden="true">&times;</span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body-video">
            </div>
        </div>
    </div>
</div>
<!--end modal testimonials-video-->

<!--begin modal testimonials-photo-->
<div class="modal fade" id="modalTestimonialsPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!--end modal testimonials-photo-->

<!--begin modal modal-price-->
<div class="modal fade" id="modalPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body-price">
            </div>
        </div>
    </div>
</div>
<!--end modal modal-price-->

<!--begin modal modal-price-->
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4><?= __( 'Напишите нам', 'revocanda' ); ?></h4>
                <p><?= __( 'Задайте нам вопрос или сообщите нам, что вас беспокоит.', 'revocanda' ); ?></p>
            </div>
            <div class="modal-body-form">
                <form action="" class="form-contact-popup">
                    <input type="text" class="form-input form-control" placeholder="Ваше имя">
                    <input type="text" class="form-input form-control" placeholder="Ваш телефон">
                    <input type="text" class="form-input form-control" placeholder="Ваш email">
                    <textarea type="text" class="form-message" rows="7" placeholder="Ваше сообщение"></textarea>
                    <button class="main-button blue-button" type="submit">отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!--end modal modal-price-->

<!--SCRIPTS-->
<?php wp_footer(); ?>

<script type="text/javascript">
  (function(d, w, s) {
	var widgetHash = '6fa2ige9yj6xnta2o4zp', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true;
	gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js';
	var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);
  })(document, window, 'script');
</script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'WXP4zH42XG';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

</body>
</html>