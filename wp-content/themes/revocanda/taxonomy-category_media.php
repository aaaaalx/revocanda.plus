<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

$contentType = @$_GET['type'];
$posts_array = [];
if(!$contentType){
    $args = [
        'posts_per_page'   => -1,
        'offset'           => 0,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'media',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'	   => '',
        'author_name'	   => '',
        'post_status'      => 'publish',
        'suppress_filters' => true
    ];
    $posts_array = get_posts($args);

    if(count($posts_array)>0){
        foreach ($posts_array as $key => $post){
            if(!$post->post_content){
                unset($posts_array[$key]);
            }
        }
        $posts_array = array_values($posts_array);
    }

}
elseif($contentType === 'video'){
    $args = [
        'posts_per_page'   => -1,
        'offset'           => 0,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'media',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'	   => '',
        'author_name'	   => '',
        'post_status'      => 'publish',
        'suppress_filters' => true,
        'meta_query' => array(
            array(
                'key' => '_media_video_status_value_key',
                'compare' => '==',
                'value' => 1
            ),
			array(
				'key' => '_media_video_active_value_key',
				'compare' => '==',
				'value' => 1,
			)
        )
    ];
    $posts_array = get_posts($args);
}

$res = getPaged($posts_array);
$posts_array = $res['posts_array'];

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>
    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>СМИ о нас</span>
							</span>
                            <p>СМИ о нас</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-feedback-page-->
    <section class="section-feedback-page section-media-articles">
        <div class="container">
            <div class="row">
                <!--begin page-nav-wrap-->
                <div class="col-xs-12">
                    <div class="page-nav-wrap">
                        <ul class="slider-page-nav">
                            <?php
                            echo '<li class="item'.($contentType==='video'?'':' active').'"><a href="/media">статьи</a></li>';
                            echo '<li class="item'.($contentType==='video'?' active':'').'"><a href="/media?type=video">видео</a></li>';
                            ?>
                        </ul>
                    </div>
                </div>
                <!--end page-nav-wrap-->

                <!--begin media-about-us-items-->
                <div class="col-xs-12">
                    <div class="tab-description">
                        <?php if($contentType==='video'): ?>
                        <p>Нажмите на кнопку для просмотра видеоролика.</p>
                        <?php endif; ?>
                    </div>
                    <div class="row">

                    <?php if(count($posts_array)>0): ?>
                        <?php foreach($posts_array as $post): ?>
                            <?php
                            $title = get_the_title($post->ID);
                            $date = get_the_date('m.d.Y', $post->ID);
                            $image = '';
                            $author = '';
                            $video = '';
                            $video_title = '';
                            $video_desc = '';
                            $video_type = '';
                            if($contentType==='video'){
                                $isVideoActive = get_post_meta($post->ID, '_media_video_active_value_key', true);
                                $video_type = get_post_meta($post->ID, '_media_video_type_value_key', true);
                                if($video_type == 'video'){
                                    $video = get_post_meta($post->ID, '_media_video_value_key', true);
                                } else{
                                    $video = get_post_meta($post->ID, '_media_video_link_value_key', true);
                                }

                                $video_title = $video?get_post_meta($post->ID, '_media_video_title_value_key', true):'';
                                $video_title = $video_title?$video_title:$title;
                                $video_desc = $video?get_post_meta($post->ID, '_media_video_desc_value_key', true):'';
                            }
                            else{
                                $image = get_the_post_thumbnail_url($post->ID, 'block-medium-image');
                                $image = $image?$image:get_stylesheet_directory_uri().'/img/No-photo_380x290.jpg';
                                $author = get_post_field('post_author', $post->ID);
                                $author = get_the_author_meta('display_name', $author);
                            }
                            ?>

                        <?php if($contentType==='video'): ?>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="video-box">
                                <div class="video-wrap">
                                    <?php
                                    if($video_type == 'embed'){
                                        $videoHtml = wp_oembed_get($video, [
                                            'width'=>480,
                                            'height'=>353
                                        ]);
                                    }
                                    else{
                                        $videoHtml = wp_video_shortcode([
                                            'src'=>$video,
                                            'width'=>480,
                                            'height'=>350
                                        ]);
                                    }
                                    ?>
                                    <?= $videoHtml; ?>
                                    <div class="overlay-box video-1"></div>
                                </div>
                                <div class="video-title">
                                    <h3><?= $video_title; ?></h3>
                                </div>
                                <div class="divider"></div>
                                <div class="date-box">
                                    <span><?= $date; ?></span>
                                </div>
                            </div>
                        </div>

                        <?php else: ?>

                        <div class="col-xs-12">
                            <div class="inner-wrap">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-xs-12">
                                        <?php if($image): ?>
                                            <img src="<?= $image; ?>" alt="">
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-lg-8 col-md-6 col-xs-12">
                                        <div class="article-wrap">
                                            <div class="article-title">
                                                <h3><?= $title; ?></h3>
                                            </div>
                                            <div class="article-info-box">
                                                <?php //<div class="date"><span> $date; </span></div> ?>
                                                <div class="name"><span><?= $author; ?></span></div>
                                            </div>
                                            <div class="article-description">
                                                <p><?= wp_trim_words( strip_shortcodes($post->post_content), 22, '...' ); ?></p>
                                            </div>
                                            <a href="<?= get_permalink($post->ID); ?>" class="more-toggle"><span>Подробнее</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php endif; ?>

                        <?php endforeach; ?>

                    <?php else: ?>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            Нет материалов для отображения
                        </div>
                    <?php endif; ?>

                    </div>
                    <!--end tab media-articles-->
                </div>
                <!--end media-about-us-items-->

                <?php getPaganation($res); ?>

            </div>
        </div>
    </section>
    <!--end section-feedback-page-->
<?php
get_footer();
?>