<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage revocanda
 */


get_header();

global $isMobile;

$background = get_post_meta($post->ID, '_specialists_settings_bg_value_key', true);
if($background){
    if($isMobile){
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'mobile-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
    else{
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'desktop-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
}
else{
    if($isMobile){
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg-mobile.png);"';
    }
    else{
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg.png);"';
    }
}
?>
    <!--begin section-intro-->
    <section class="section-poster-intro"<?= $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>
									специалисты
								</span>
							</span>
                            <p>Наши специалисты</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <?php
    $image = get_the_post_thumbnail($post->ID, 'block-medium-image');
    $phones = get_post_meta($post->ID, '_specialists_phones_value_key', true);
    if($phones){
        $phones = json_decode($phones, true);
    }
    $emails = get_post_meta($post->ID, '_specialists_emails_value_key', true);
    if($emails){
        $emails = json_decode($emails, true);
    }
    $contacts = get_post_meta($post->ID, '_specialists_contacts_value_key', true);
    if($contacts){
        $contacts = json_decode($contacts, true);
    }

    ?>

    <!--begin section-specialist-list-->
    <section class="section-item-page image-default">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a class="back-button" href="/specialists" hreflang="ru"><span>Все специалисты</span></a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-12">
                    <?php if($image): ?>
                        <?= $image; ?>
                    <?php endif; ?>
                </div>
                <div class="col-lg-9 col-md-8 col-xs-12">

                    <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();
                    ?>

                        <div class="article-title">
                        <h1><?= get_the_title(); ?></h1>
                    </div>
                    <div class="divider"></div>
                        <?= do_shortcode(get_the_content()); ?>
                    <div class="divider"></div>
                    <div class="contact-wrap">

                        <?php if(is_array($phones) && count($phones)>0): ?>
                        <div class="contact-box phone-box">
                            <?php foreach($phones as $item): ?>
                            <?php if(!$item['phone_link']){
                                    $item['phone_link'] = 'tel:'.str_replace(' ', '', $item['phone']);
                                }
                            ?>
                            <a href="<?= $item['phone_link']; ?>" hreflang="ru"><?= $item['phone']; ?></a>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>

                        <?php if(is_array($emails) && count($emails)>0): ?>
                        <div class="contact-box email-box">
                            <?php foreach($emails as $item): ?>
                                <?php if(!$item['email_link']){
                                    $item['email_link'] = 'mailto:'.str_replace(' ', '', $item['email']);
                                }
                                ?>
                                <a href="<?= $item['email_link']; ?>" hreflang="ru"><?= $item['email']; ?></a>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>

                        <?php if(is_array($contacts) && count($contacts)>0): ?>
                        <?php foreach($contacts as $item): ?>
                        <div class="contact-box <?= $item['icon']; ?>">

                        <?php
                        $str = '';
                        if(isset($item['contacts_link'])){
                            $str .= '<a href="'.$item['contacts_link'].'>" hreflang="ru">';
                            $str .= $item['contacts'];
                            $str .= '</a>';
                        }
                        else{
                            $str .= '<span>'.$item['contacts'].'</span>';
                        }
                        echo $str;
                        ?>

                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                    <button class="more-button button-down"></button>

                    <?php
                        // End the loop.
                    endwhile;
                    ?>

                </div>
            </div>
        </div>
    </section>
    <!--end section-specialist-->

<?php get_footer(); ?>