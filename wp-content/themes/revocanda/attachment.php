<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header();

global $isMobile;

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>

    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->
	
	<!--begin section-about-us-page-->
    <section class="section-about-us-page">
        <div class="container">
            <div class="row">
				<div class="attachment">
					<?php
					$format = substr(get_post()->post_mime_type, 0, strpos(get_post()->post_mime_type, '/'));
					if($format == 'image'){
						echo '<a class="thickbox" href="'.get_post()->guid.'" hreflang="ru"><img src="'.get_post()->guid.'" alt=""></a>';
					}
					elseif($format == 'video'){
						echo wp_video_shortcode( ['src'=>get_post()->guid] );
					}
					elseif($format == 'audio'){
						echo wp_audio_shortcode( ['src'=>get_post()->guid] );
					}
					else{
						echo '<a href="'.get_post()->guid.'" hreflang="ru">'.get_the_title( get_post()->ID ).'</a>';
					}
					?> 
				</div>
            </div>
        </div>
    </section>
    <!--end section-about-us-page-->

<?php get_footer(); ?>
