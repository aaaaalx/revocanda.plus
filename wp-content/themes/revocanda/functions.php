<?php
if (!session_id()) {
    session_start();
}

$thisCustomPosts = [];

$text_domain = 'revocanda';

require get_template_directory().'/inc/function-admin.php';
require get_template_directory().'/inc/enqueue.php';
require get_template_directory().'/inc/function-support.php';
require get_template_directory().'/inc/custom-post-type.php';

//inqlude custom post types
require get_template_directory().'/inc/post-type-gallery.php';
//inqlude custom post price
require get_template_directory().'/inc/post-type-prices.php';
require get_template_directory().'/inc/post-type-specialists.php';
require get_template_directory().'/inc/post-type-media.php';
require get_template_directory().'/inc/post-type-feedbacks.php';
require get_template_directory().'/inc/post-type-licenses.php';
require get_template_directory().'/inc/post-type-centers.php';
require get_template_directory().'/inc/post-type-events.php';
require get_template_directory().'/inc/post-type-contacts.php';

//include ajax
require_once get_template_directory().'/inc/ajax.php';

//mime types
require_once get_template_directory().'/inc/mime_type_lib.php';

/*WIDGETS*/
//include widget "Main Info"
require_once get_template_directory().'/widgets/widget-main-info.php';
//include widget "Registration"
require_once get_template_directory().'/widgets/widget-registration.php';
//include widget "Services"
require_once get_template_directory().'/widgets/widget-services.php';
//include widget "About Us"
require_once get_template_directory().'/widgets/widget-about-us.php';
//include widget "Feedback Dummy"
require_once get_template_directory().'/widgets/widget-feedback-dummy.php';
//include widget "Special Offer"
require_once get_template_directory().'/widgets/widget-special-offer.php';
//include widget "Specialists"
require_once get_template_directory().'/widgets/widget-specialists.php';
//include widget "Result"
require_once get_template_directory().'/widgets/widget-result.php';
//include widget "Gallery"
require_once get_template_directory().'/widgets/widget-gallery.php';
//include widget "Centers"
require_once get_template_directory().'/widgets/widget-centers.php';
//include widget "Special Offer 2"
require_once get_template_directory().'/widgets/widget-special-offer-2.php';
//include widget "Testimonials"
require_once get_template_directory().'/widgets/widget-testimonials.php';
//include widget "Social links"
require_once get_template_directory().'/widgets/widget-soc-links.php';
//include widget "Html"
require_once get_template_directory().'/widgets/widget-html.php';
//include widget "Partners"
require_once get_template_directory().'/widgets/widget-partners.php';

//detect mobile
function is_mobile(){
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if(
        preg_match(
            '/(android|bb\d+|meego).+mobile|avantgo|bada\/|android|ipad|playbook|silk|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',
            $useragent
        )
        ||
        preg_match(
            '/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',
            substr($useragent,0,4)
        )
    )
        return true;
    return false;
}
$isMobile = is_mobile();

//do shortcode
add_filter('the_content', 'do_shortcode', 11);

//hide admin bar
show_admin_bar(false);

//turn on thumbnails support in posts
add_theme_support('post-thumbnails');

//move theme localization into the theme folder
add_action( 'after_setup_theme', 'localization_dir_setup' );
function localization_dir_setup(){
    load_theme_textdomain('winnbrain-land', get_template_directory().'/languages');
}

//don't show console input: "JQMIGRATE: Migrate is installed, version 1.4.1"
add_action( 'wp_default_scripts', function( $scripts ) {
    if ( ! empty( $scripts->registered['jquery'] ) ) {
        $scripts->registered['jquery']->deps = array_diff( $scripts->registered['jquery']->deps, array( 'jquery-migrate' ) );
    }
} );

//get post attachment id by url
function pippin_get_file_id($url) {
    global $wpdb;
    $pUrl = parse_url($url);
    $url = get_site_url().@$pUrl['path'];
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $url));
    return @$attachment[0];
}

//get attachment postmeta id by url
function get_attachment_file_id($url) {
    global $wpdb;
    $pUrl = parse_url($url);
    $url = @$pUrl['path'];
    $url = str_replace('/wp-content/uploads/', '', $url);
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_value='%s';", $url));
    return @$attachment[0];
}

//get thumbnails from image ID
function get_thumbnails_by_id($id, $size='thumbnails'){
    $fileName = false;
    if($id){
        $metadata = wp_get_attachment_metadata($id);
        $path = @$metadata['file'];
        $thumbnailName = @$metadata['sizes'][$size]['file']?$metadata['sizes'][$size]['file']:false;
        $fileName = substr($path, 0, strrpos($path, '/'));
        if($thumbnailName){
            $fileName = $fileName?'/wp-content/uploads/'.$fileName.'/'.$thumbnailName:false;
        }
        else{
            $fileName = wp_get_attachment_url($id);
        }
    }
    return $fileName;
}

//return array of widget instance with their fields
function getWidgetFieldByClassName($className){
    if(class_exists($className)){
        $widget = new $className;
        $id_base = $widget->id_base;
        $settings = $widget->get_settings();
        $res = [];
        if(count($settings)>0){
            foreach($settings as $k => $v){
                $res[$id_base.'-'.$k] = $v;
            }
        }
        return $res;
    }
    return false;
}

//get only format from mime
function getFileFormat($mime){
    if($mime){
        $mime = substr($mime, 0, strpos($mime, '/'));
        return $mime;
    }
    return false;
}

function revocanda_widgets_init() {
    register_sidebar([
        'name'          => __( 'Tag Head', 'revocanda' ),
        'id'            => 'head',
        'description'   => __( '<head>', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Tag Body', 'revocanda' ),
        'id'            => 'body',
        'description'   => __( '<body>', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Header Email', 'revocanda' ),
        'id'            => 'header_email',
        'description'   => __( 'Header Email', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Header Phone', 'revocanda' ),
        'id'            => 'header_phone',
        'description'   => __( 'Header Phone', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Phones', 'revocanda' ),
        'id'            => 'phones',
        'description'   => __( 'Phones', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Main Content', 'revocanda' ),
        'id'            => 'main_content',
        'description'   => __( 'Main Content', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    /*
    register_sidebar([
        'name'          => __( 'Main Bottom Content', 'revocanda' ),
        'id'            => 'main_content_bottom',
        'description'   => __( 'Main Bottom Content', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Main Header Content', 'revocanda' ),
        'id'            => 'header_content',
        'description'   => __( 'Main Header Content', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    */
    register_sidebar([
        'name'          => __( 'Copyright', 'revocanda' ),
        'id'            => 'copyright',
        'description'   => __( 'Copyright', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Social Links', 'revocanda' ),
        'id'            => 'soc',
        'description'   => __( 'Social Links', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Footer Content', 'revocanda' ),
        'id'            => 'footer_content',
        'description'   => __( 'Footer Content', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Footer Block 1', 'revocanda' ),
        'id'            => 'footer_block_1',
        'description'   => __( 'Footer Block 1', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Footer Block 2', 'revocanda' ),
        'id'            => 'footer_block_2',
        'description'   => __( 'Footer Block 2', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);

    //page bottom
    register_sidebar([
        'name'          => __( 'Page bottom', 'revocanda' ),
        'id'            => 'page_bottom',
        'description'   => __( 'Page bottom', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);

    //contacts page
    register_sidebar([
        'name'          => __( 'Contacts', 'revocanda' ),
        'id'            => 'contacts_page',
        'description'   => __( 'Contacts', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);

    //free widgets
    register_sidebar([
        'name'          => __( 'Free Widgets Area', 'revocanda' ),
        'id'            => 'free_widgets_area',
        'description'   => __( 'Area for custom using widgets', 'revocanda' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
}
add_action('widgets_init', 'revocanda_widgets_init');

//get posts/pages/custom posts by params(category, offset, order, post type, status)
function posts($cat=false, $offset=false, $post_type='post', $status='publish', $order='DESC'){
        $args = [
            'posts_per_page'   => -1,
            'offset'           => $offset,
            'category'         => $cat,
            'category_name'    => '',
            'orderby'          => 'modified',
            'order'            => $order,
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => $post_type,
            'post_mime_type'   => '',
            'post_parent'      => '',
            'post_status'      => $status,
            'suppress_filters' => true
        ];
        $posts_array = get_posts($args);
        return $posts_array;
}

//return list(key=>value) from inserted posts array
function getPostsSelectList($posts=[]){
    $list = [];
    if(count($posts)>0){
        foreach ($posts as $v){
            $list[$v->ID] = $v->post_title;
        }
    }
    if(count($list)>0){
        return $list;
    }
    return false;
}

function force_404() {
    global $wp_query; //$posts (if required)
    status_header( 404 );
    nocache_headers();
    include( get_query_template( '404' ) );
    die();
}

function alertPage($title='') {
    global $wp_query; //$posts (if required)
    status_header($title);
    nocache_headers();
    include( get_query_template('alert-page'));
    die();
}

//checking whether exists the categories and/or posts by user requests
function checkUrlPath($request=null, $taxonomy=null, $query=null, $type=null){
    if($request && $taxonomy && $query && $type){
        $currRequestSlug = @$query->query[$taxonomy];
        $currCatSlug = @$query->query_vars[$taxonomy];
        if(strrpos($currRequestSlug, '/')>0){
            $currCatSlug = substr($currRequestSlug, strrpos($currRequestSlug, '/')+1);
        }
        if(!$currCatSlug && !$currRequestSlug){
            return false;
        }
        $terms = get_terms( array(
            'taxonomy' => $taxonomy,
            'hide_empty' => false
        ));
        $currcat = null;
        if(count($terms)>0){
            foreach ($terms as $term){
                if($term->slug === $currCatSlug){
                    $currcat = $term;
                    break;
                }
            }
        }
        $parents = [];
        if($currcat){
            $parents = get_ancestors($currcat->term_id, $taxonomy, 'taxonomy');
            $parents = array_reverse($parents);
        }
        $terms = get_terms($taxonomy, array('hide_empty' => false));
        $path = '';
        if(count($parents)>0 && count($terms)>0){//if have parents
            foreach ($parents as $v){
                foreach ($terms as $v2){
                    if($v2->term_id === $v){
                        $path .= $v2->slug.'/';
                    }
                }
            }
            $path = $path.$currcat->slug;
        }
        elseif(!count($parents)>0 && $currcat){//if no parents
            $path = $currcat->slug;
        }
        else{//if post without category

            if($currCatSlug === 'uncategorized'){
                return true;
            }
            return false;
        }
        if($type == 'category' && $path === $currRequestSlug){
            return true;
        }
        elseif($type == 'post' && $path === $currRequestSlug && isset($query->post)){
            $request_str = str_replace('_', '', substr($request, strrpos($request, '/')+1));
            if($request_str != $query->post->post_name){
                return false;
            }
            $ischild = get_the_terms($query->post->ID, $taxonomy);
            $ischild = @$ischild[0]->slug===$currCatSlug?true:false;
            if($ischild){
                return true;
            }
        }
    }
    return false;
}

//dynamic fields
//pattern template: <p><input [title="My Title"/] [type="text"/] [name=""/] [class="field"/] [data="type:image"/] [value=""/] /></p>
//pattern fields types: input, textarea, editor, checkbox, radio, select, media
//select have additional param with json array: [list="value="[{"title":"Hello","desc":"xxx"}]"/]
//fields types: text, password, email...
function insertDynamicField($id, $title, $dynamicFields, $pattern=false, $name=false){
    if(!$pattern){
        return;
    }

    if(!$name){
        $name = $id;
    }

    echo '<p><label for="' . $id . '">'.$title.': </label>';

    $dynamicFieldsArray = json_decode($dynamicFields, true);

    echo '<div class="dynamic-fields">';
    echo '<div class="dynamic-fields-blocks">';

    if($dynamicFieldsArray){
        $num = 1;
        foreach ($dynamicFieldsArray as $item){
            echo getDynamicFieldTemplate($num, $item, $pattern, $id);
            $num++;
        }
    }

    echo '</div>';
    echo '<input class="hidden-input-data" id="'.$id.'" type="hidden" name="'.$name.'" value="'.htmlentities($dynamicFields).'">';
    echo '<span class="add-block-title">'.__('Add block Field', 'revocanda').': </span>';
    echo '<div class="add-block"><span class="dashicons dashicons-plus"></span></div>';

    echo '<div class="fieldsTemplate">';
    echo getDynamicFieldTemplate('[!num!]', false, $pattern, $id);
    echo '</div>';
    echo '</div>';
    echo '<hr><br>';
}

function getDynamicFieldTemplate($num, $item = false, $pattern = false, $blockid = false){

    if(!$pattern || !$blockid){
        return false;
    }

    //seeking fields in pattern
    $fields = getFieldsFromPattern($pattern);
    $fieldsAndOptions = [];
    $patternMod = $pattern;

    if(count($fields)>0){
        foreach ($fields as $k => $v){
            $anchor = '[!input('.$k.')!]';
            $patternMod = str_replace($v['field'], $anchor, $patternMod);
            //find args
            $fieldsAndOptions[$k] = ['field'=>$v, 'options'=>getArgsFromPattern($v['field']), 'anchor'=>$anchor];
        }
    }

    $templateFilds = [];
    $itemTitle = '';
    if(count($fieldsAndOptions)>0){
        foreach ($fieldsAndOptions as $k => $v){
            switch (@$v['field']['field_type']){
                case 'input':

                    $title = @$v['options']['title'];
                    $type = isset($v['options']['type'])?' type="'.$v['options']['type'].'"':'';
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $class = isset($v['options']['class'])?' class="'.$v['options']['class'].'"':'';
                    $value = isset($item[@$v['options']['name']])?' value="'.$item[$v['options']['name']].'"':' value=""';

                    //set item title
                    if($v['options']['name']=='title' ||
                        $v['options']['name']=='city' ||
                        $v['options']['name']=='specialization' ||
                        $v['options']['name']=='institution' ||
                        $v['options']['name']=='event_type'){
                        $itemTitle = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';
                    }

                    $str = __($title, 'revocanda').': ';
                    $str .= '<input id="'.$blockid.'_'.$num.'-'.$v['options']['name'].'"'.$class.$type.$name.$value.'>';
                    $templateFilds[$k] = $str;

                    break;
                case 'textarea':

                    $title = @$v['options']['title'];
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $class = isset($v['options']['class'])?' class="'.$v['options']['class'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';

                    $str = __($title, 'revocanda').': ';
                    $str .= '<textarea id="'.$blockid.'_'.$num.'-'.$v['options']['name'].'"'.$class.' rows="3"'.$name.'>'.$value.'</textarea>';
                    $templateFilds[$k] = $str;

                    break;
                case 'media':

                    $title = @$v['options']['title'];
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';
                    $id = $blockid.'-'.$num.'-'.$v['options']['name'];
                    $options = isset($v['options']['data(options)'])?' data-options="'.$v['options']['data(options)'].'"':'';

                    $str = '<span class="img-title">'.__($title, 'revocanda').': </span>';
                    $str .= '<div class="widget-block-img">';

                    if($value && strpos($value, ', ')>-1){
                        //check is multi media
                        $mediaStringA = explode(', ', $value);
                        if(count($mediaStringA)>0){
                            add_thickbox();
                            foreach($mediaStringA as $img){
                                $gId = pippin_get_file_id($img);
                                $imageSmall = get_thumbnails_by_id($gId, 'icons');
                                $imageSmall = $imageSmall?$imageSmall:$img;
                                $str .= '<a class="thickbox" href="'.$img.'?TB_iframe=false&width=100%&height=100%"><img src="'.$imageSmall.'"></a>';
                            }
                        }
                    }
                    else{
                        $gId = pippin_get_file_id($value);
                        $imageSmall = get_thumbnails_by_id($gId, 'icons');
                        $imageSmall = $imageSmall?$imageSmall:$value;
                        add_thickbox();
                        $str .= '<a class="thickbox" href="'.$value.'?TB_iframe=false&width=100%&height=100%"><img src="'.$imageSmall.'"></a>';
                    }

                    $str .= '</div>';
                    $str .= '<input'.$options.' class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'revocanda').'" />';
                    $str .= '<input id="'.$id.'" class="input-value datafield" type="hidden"'.$name.' value="'.$value.'" />';
                    $str .= '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'revocanda').'" />';
                    $str .= '<div data-num="'.$num.'" class="change-pos"><span data-act="up" class="dashicons dashicons-arrow-up-alt2"></span><span data-act="down" class="dashicons dashicons-arrow-down-alt2"></span></div>';
                    $templateFilds[$k] = $str;

                    break;
                case 'editor':

                    $title = @$v['options']['title'];
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $class = isset($v['options']['class'])?' class="'.$v['options']['class'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';

                    $str = __($title, 'revocanda').': ';
                    $str .= '<textarea id="'.$num.'_'.$v['options']['name'].'"'.$class.' rows="3"'.$name.'>'.$value.'</textarea>';
                    $templateFilds[$k] = $str;

                    break;
                case 'imageslist':

                    $title = @$v['options']['title'];
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';

                    $icons = getItemsFromDynamicFields('field_contact_icons');

                    $str = __($title, 'revocanda').': ';
                    //Dropdown plugin data
                    $dropdown = 'dropdown_'.$v['options']['name'].'_'.$num;
                    $str .= '<div id="'.$dropdown.'" class="Dropdown"></div>';
                    $datafield = 'dropdown_'.$v['options']['name'].'_'.$num.'_datafield';
                    $str .= '<input id="'.$datafield.'" class="input-value datafield" type="hidden"'.$name.' value="'.$value.'" />';
                    $str .= '<script>';
                    $str .= 'ddData["'.$dropdown.'"] = [';

                    if(count($icons)>0){
                        foreach ($icons as $val) {
                            $selected = '';
                            if($value == $val['img']){
                                $selected = 'selected: true,';
                            }
                            $str .= '{';
                            $str .=     'text: "'.$val['title'].'",';
                            $str .=     $selected;
                            $str .=     'imageSrc: "'.$val['img'].'",';
                            $str .= '},';
                        }
                    }
                    $str = substr($str, 0, strlen($str)-1);
                    $str .= ']; ';
                    $str .= '</script>';

                    $templateFilds[$k] = $str;
                    break;
                case 'select':

                    $title = @$v['options']['title'];
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $class = isset($v['options']['class'])?' class="'.$v['options']['class'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';
                    $options = isset($v['options']['data'])?' data-options="'.$v['options']['data'].'"':'';

                    $optionsArray = [];
                    if(isset($v['options']['data'])){
                        $optionsArray = explode(',', $v['options']['data']);
                        foreach ($optionsArray as $key => $option){
                            $var = explode(':', $option);
                            unset($optionsArray[$key]);
                            $optionsArray[$var[0]] = $var[1];
                        }
                    }

                    //get list
                    $list = null;
                    if(isset($optionsArray['list'])){
                        $list = call_user_func($optionsArray['list']);
                    }

                    $str = __($title, 'revocanda').': ';
                    $str .= '<select'.$class.' rows="3"'.$name.$options.'>';
                    if(count($list)>0){
                        foreach ($list as $key => $option){
                            $selected = '';
                            if($key == $value){
                                $selected = ' selected';
                            }
                            $str .= '<option value="'.$key.'"'.$selected.'>'.$option.'</option>';
                        }
                    }
                    $str .= '</select>';
                    $templateFilds[$k] = $str;

            }
        }
    }

    $template = '';
    $template .= '<div data-num="'.$num.'" class="dynamic-fields-block-wrap">';
    $template .= '<div data-num="'.$num.'" class="dynamic-fields-block">';
    $template .= '<input class="datafield" type="hidden" data-name="item_id" value="'.(isset($item['item_id'])?$item['item_id']:'[!id!]').'">';
    $template .= '<div class="top-wrap">';
    $template .= '<div class="block-num">#'.$num.'</div><div class="block-toggle"><span class="dashicons dashicons-arrow-up"></span></div>';
    $itemTitle = cutString($itemTitle, $count=50, $end='...');
    $template .= $itemTitle?'<span class="itemTitle">'.$itemTitle.'</span>':'';
    $template .= '<div class="remove-block"><span class="dashicons dashicons-no"></span></div>';
    $template .= '</div>';
    $template .= '<div class="content-wrap">';

    $templateStr = '';
    if($patternMod && count($templateFilds)>0){
        $templateStr = $patternMod;
        foreach ($templateFilds as $k => $v){
            $templateStr = str_replace('[!input('.$k.')!]', $v, $templateStr);
        }
    }

    $template .= $templateStr;
    $template .= '<br><br><div data-num="'.$num.'" class="change-pos"><span data-act="up" class="dashicons dashicons-arrow-up-alt2"></span><span data-act="down" class="dashicons dashicons-arrow-down-alt2"></span></div>';
    $template .= '</div>';
    $template .= '</div>';
    $template .= '</div>';

    return $template;
}
//return extracted fields from pattern
function getFieldsFromPattern($pattern){
    if(!$pattern){
        return false;
    }
    $fieldsList = [
        'input' => '<input ',
        'textarea' => '<textarea ',
        'editor' => '<editor ',
        'checkbox' => '<checkbox ',
        'radio' => '<radio ',
        'select' => '<select ',
        'media' => '<media ',
        'imageslist' => '<imageslist ',
    ];
    $fields = [];
    foreach ($fieldsList as $k => $v){
        $tag = $v;
        $startPoint = 0;
        $go = true;
        while($go){
            $patternRes = substr($pattern, $startPoint);
            $start = strpos($patternRes, $tag);
            if($start){
                $input = substr($patternRes, $start);
                $end = strpos($input, ' />')+3;
                $input = substr($patternRes, $start, $end);
                $fields[] = ['field'=>$input,'field_type'=>$k];
                $startPoint = $startPoint+$start+$end;
            }
            else{
                $go = false;
            }
        }
    }
    return $fields;
}
//return arguments from extracted fields
function getArgsFromPattern($v){
    $args = [
        'title' => false,
        'type' => false,
        'name' => false,
        'class' => false,
        'data' => [],
        'value' => false,
        'list' => false
    ];
    if($v){
        //title
        $find = '[title="';
        $start = strpos($v, $find);
        if($start){
            $args['title'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //type
        $find = '[type="';
        $start = strpos($v, $find);
        if($start){
            $args['type'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //name
        $find = '[name="';
        $start = strpos($v, $find);
        if($start){
            $args['name'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //class
        $find = '[class="';
        $start = strpos($v, $find);
        if($start){
            $args['class'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //value
        $find = '[value="';
        $start = strpos($v, $find);
        if($start){
            $args['value'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //data
        $find = '[data="';
        $start = strpos($v, $find);
        if($start){
            $args['data'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //list
        $find = '[list="';
        $start = strpos($v, $find);
        if($start){
            $args['list'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
    }
    return $args;
}
//get dynamic field items by its variable id
function getItemsFromDynamicFields($field_id){
    if($field_id){
        $list = get_option($field_id);
        if($list){
            return json_decode($list, true);
        }
    }
    return false;
}
//get custom posts by its id(name), cat id, number of posts, offset,  sort by, order, status
function getPostsByPostType($postType, $cat = false, $numberposts = -1, $offset = 0, $orderby = 'date', $order = 'DESC', $status = 'publish'){
    $posts = [];
    if($postType){
        $args = array(
            'numberposts' => $numberposts,
            'offset' => $offset,
            'category'    => $cat,
            'orderby'     => $orderby,
            'order'       => $order,
            'include'     => array(),
            'exclude'     => array(),
            'meta_key'    => '',
            'meta_value'  =>'',
            'post_type'   => $postType,
            'suppress_filters' => true,
            'post_status' => $status
        );
        $posts = get_posts( $args );
    }
    return $posts;
}

//add scripts to admin head
function revocanda_custom_admin_head() {
    echo '<script type="text/javascript">';
    echo 'var ddData = {};';
    echo 'var selectedMedia = {};';
    echo 'var mediaLibrary = {};';
    echo '</script>';
}
add_action( 'admin_head', 'revocanda_custom_admin_head' );

function getSoc(){
    return [
        'facebook' => 'Facebook',
        'instagram' => 'Instagram',
        'youtube' => 'Youtube'
    ];
}

function getCountries(){
    $dynamicFields = get_option('field_countries');
    if($dynamicFields){
        $countries = json_decode($dynamicFields, true);
        if(count($countries)>0){
            $resultArray = [];
            foreach ($countries as $key => $country){
                $resultArray[$country['item_id']] = $country['title'];
            }
            return $resultArray;
        }
    }
    return [];
}
function getCities($citiesAndCountries = false){
    $dynamicFields = get_option('field_cities');
    if($dynamicFields){
        $cities = json_decode($dynamicFields, true);
        if(count($cities)>0){
            $resultArray = [];
            $countries = getCountries();
            foreach ($cities as $key => $city){
                if($citiesAndCountries){
                    $resultArray['cities'][$city['item_id']] = ['city'=>$city['city'],'country'=>$city['country']];
                    if(isset($countries[$city['country']])){
                        $country = $countries[$city['country']];
                        $resultArray['countries'][$city['country']]['country'] = $country;
                        if(!isset($resultArray['countries'][$city['country']]['cities'])){
                            $resultArray['countries'][$city['country']]['cities'] = [];
                        }
                        $resultArray['countries'][$city['country']]['cities'] += [$city['item_id']=>$city['city']];
                    }
                }
                else{
                    $resultArray[$city['item_id']] = $city['city'];
                }
            }
            return $resultArray;
        }
    }
    return [];
}
function getCitiesByCountry($country_id){
    $citiesAndCountries = getCities(true);
    if(isset($citiesAndCountries['countries'][$country_id])){
        $country = $citiesAndCountries['countries'][$country_id];
        $cities = $country['cities'];
        if(count($cities)>0){
            return $cities;
        }
    }
    return [];
}

function getContactsIcons(){
    return [
        'phone-box' => 'Телефон',
        'email-box' => 'Email',
        'skype-box' => 'Skype',
        'facebook-box' => 'Facebook',
        'whatsapp-box' => 'Whatsapp',
        'google-plus-box' => 'Google+',
        'twitter-box' => 'Twitter',
        'viber-box' => 'Viber',
        'mode-box' => 'Часы',
        'website-box' => 'Сайт'
    ];
}

function getCenters($select=false){
    //get from custom post type centers
    $args = [
        'posts_per_page'   => -1,
        'offset'           => 0,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'modified',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'centers',
        'genre'            => 'category_centers',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'author'	   => '',
        'author_name'	   => '',
        'post_status'      => 'publish',
        'suppress_filters' => true
    ];
    $posts_array = get_posts( $args );
    if($select && count($posts_array)>0){
        $resultArray = [];
        foreach ($posts_array as $post){
            $resultArray[$post->ID] = $post->post_title;
        }
        return $resultArray;
    }
    else{
        return $posts_array;
    }
}

function getSpecializations(){
    $dynamicFields = get_option('field_specialization');
    if($dynamicFields){
        $specializations = json_decode($dynamicFields, true);
        if(count($specializations)>0){
            $resultArray = [];
            foreach ($specializations as $key => $specialization){
                $resultArray[$specialization['item_id']] = $specialization['specialization'];
            }
            return $resultArray;
        }
    }
    return [];
}

function getInstitutions(){
    $dynamicFields = get_option('field_institution');
    if($dynamicFields){
        $institutions = json_decode($dynamicFields, true);
        if(count($institutions)>0){
            $resultArray = [];
            foreach ($institutions as $institution){
                $resultArray[$institution['item_id']] = $institution['institution'];
            }
            return $resultArray;
        }
    }
    return [];
}

function getEventsTypes(){
    $dynamicFields = get_option('field_event_type');
    if($dynamicFields){
        $event_types = json_decode($dynamicFields, true);
        if(count($event_types)>0){
            $resultArray = [];
            foreach ($event_types as $event_type){
                $resultArray[$event_type['item_id']] = $event_type['event_type'];
            }
            return $resultArray;
        }
    }
    return [];
}

//SEO----------------------------------
add_action('add_meta_boxes', 'getSeoFields', 10, 2);
function getSeoFields($post_type, $post){
    if($post_type == 'contacts' ||
        $post_type == 'testimonials' ||
        $post_type == 'prices' ||
        $post_type == 'licenses' ||
        $post_type == 'feedbacks' ||
        $post_type == 'gallery'){
        return;
    }
    add_meta_box('seo_settings', 'СЕО', 'seo_settings_callback', $post_type, 'normal', 'default');
}
function seo_settings_callback($post){
    wp_nonce_field('seo_settings_save_data', 'seo_settings_meta_box_nonce');//add unique verifying field
    $title = get_post_meta($post->ID, '_seo_settings_title', true);//get custom meta box
    $desc = get_post_meta($post->ID, '_seo_settings_desc', true);//get custom meta box
    $keywords = get_post_meta($post->ID, '_seo_settings_keywords', true);//get custom meta box
    //title
    echo '<p>Заголовок: </p>';
    echo '<p><input id="seo_settings_title" class="widefat datafield" title="Заголовок" type="text" name="seo_settings_title" value="'.$title.'" /></p>';
    //description
    echo '<p>Описание: </p>';
    echo '<p><textarea id="seo_settings_desc" class="widefat datafield" title="Описание" name="seo_settings_desc">'.$desc.'</textarea></p>';
    //keywords
    echo '<p>Ключевые слова: </p>';
    echo '<p><textarea id="seo_settings_keywords" class="widefat datafield" title="Ключевые слова" name="seo_settings_keywords">'.$keywords.'</textarea></p>';
}
function seo_settings_save_data($post_id){
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['seo_settings_meta_box_nonce']) &&
        wp_verify_nonce($_POST['seo_settings_meta_box_nonce'], 'getSeoFields')){//if user doesn't have permission, don't save
        return;
    }
    //title
    if(isset($_POST['seo_settings_title'])){//check is meta box verifying exist
        $my_data = $_POST['seo_settings_title'];
        update_post_meta($post_id, '_seo_settings_title', $my_data);//save data from meta box field
    }
    //desc
    if(isset($_POST['seo_settings_desc'])){//check is meta box verifying exist
        $my_data = $_POST['seo_settings_desc'];
        update_post_meta($post_id, '_seo_settings_desc', $my_data);//save data from meta box field
    }
    //keywoeds
    if(isset($_POST['seo_settings_keywords'])){//check is meta box verifying exist
        $my_data = $_POST['seo_settings_keywords'];
        update_post_meta($post_id, '_seo_settings_keywords', $my_data);//save data from meta box field
    }
}
add_action('save_post', 'seo_settings_save_data');//save custom meta box
//---------------------------------------------------

//Add responsive container to embeds
function revocanda_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'revocanda_embed_html', 10, 3 );

//add new image thumbnails
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'desktop-header', 2000, 1000, true );
    add_image_size( 'mobile-header', 800, 800, true );
    add_image_size( 'desktop-post-gallery', 1200, 1200 );
    add_image_size( 'mobile-post-gallery', 800, 800 );
    add_image_size( 'block-large-image', 800, 800 );
    add_image_size( 'block-medium-image', 600, 600 );
    add_image_size( 'block-small-image', 400, 400 );
    add_image_size( 'licenses-image-hard', 380, 534, true );
    add_image_size( 'licenses-image-soft', 380, 534 );
    add_image_size( 'icons', 120, 120 );
}

//custom gallery output
add_filter( 'post_gallery', 'custom_gallery', 10, 2 );
function custom_gallery($output, $attr) {
    global $isMobile;
    $attrArr = isset($attr['ids'])?explode(',', $attr['ids']):[];
    $str = '';
    $str .= '<div class="slider-one-item">';
    if(count($attrArr)>0){
        foreach ($attrArr as $id) {
            $url = wp_get_attachment_url($id);
            if($isMobile){
                $thumbnail = get_thumbnails_by_id($id, 'mobile-post-gallery');
            }
            else{
                $thumbnail = get_thumbnails_by_id($id, 'desktop-post-gallery');
            }
            $url = $thumbnail?$thumbnail:$url;
            if ($url) {
                $str .= '<div class="item">';
                $str .= '<div class="inner-wrap">';
				$str .= '<img src="'.$url.'" alt="">';
                $str .= '</div>';
				$str .= '</div>';
            }
        }
    }
    $str .= '</div>';
    return $str;
}

//hide updates notifications for inactive plugins
function update_active_plugins($value = '') {
    if ((isset($value->response)) && (count($value->response))) {
        // Get the list cut current active plugins
        $active_plugins = get_option('active_plugins');
        if ($active_plugins) {
            //  Here we start to compare the $value->response
            //  items checking each against the active plugins list.
            foreach($value->response as $plugin_idx => $plugin_item) {
                // If the response item is not an active plugin then remove it.
                // This will prevent WordPress from indicating the plugin needs update actions.
                if (!in_array($plugin_idx, $active_plugins))
                    unset($value->response[$plugin_idx]);
            }
        }
        else {
            // If no active plugins then ignore the inactive out of date ones.
            foreach($value->response as $plugin_idx => $plugin_item) {
                unset($value->response);
            }
        }
    }
    return $value;
}
add_filter('site_transient_update_plugins', 'update_active_plugins');

function custom_format_TinyMCE( $in ) {
    $in['wpautop'] = false;
    return $in;
}
add_filter( 'tiny_mce_before_init', 'custom_format_TinyMCE' );

/**
 * Add custom field to media
 */
add_filter( 'attachment_fields_to_edit', 'wpse256463_attachment_fields', 10, 2 );
function wpse256463_attachment_fields( $fields, $post ) {
    $meta = get_post_meta( $post->ID, 'location', true );
    $fields['location'] = array(
        'label' =>  __( 'Location', 'revocanda' ),
        'input' => 'text',
        'value' => $meta,
        'show_in_edit' => true,
    );
    return $fields;
}

/**
 * Update custom field within media overlay (via ajax)
 */
add_action( 'wp_ajax_save-attachment-compat', 'wpse256463_media_fields', 0, 1 );
function wpse256463_media_fields() {
    $post_id = $_POST['id'];
    $meta = $_POST['attachments'][ $post_id ]['location'];
    update_post_meta( $post_id , 'location', $meta );
    clean_post_cache( $post_id );
}

/**
 * Update media custom field from edit media page (non ajax).
 */
add_action( 'edit_attachment', 'wpse256463_update_attachment_meta', 1 );
function wpse256463_update_attachment_meta( $post_id ) {
    $buy_now = isset( $_POST['attachments'][ $post_id ]['location'] ) ? $_POST['attachments'][ $post_id ]['location'] : false;
    update_post_meta( $post_id, 'location', $buy_now );
    return;
}

//takes arguments from post_type_link filter and return modified correct link with categories hierarchy
function post_link( $post_link, $post, $leavename, $sample, $post_type_category ){
    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, $post_type_category );
        $currcat = isset($terms[0])?$terms[0]:null;
        $parents = null;
        if($currcat){
            $parents = get_ancestors( $currcat->term_id, $post_type_category, 'taxonomy' );
            $parents = array_reverse($parents);
        }
        $terms = get_terms($post_type_category, array('hide_empty' => false));
        $path = '';
        if(count($parents)>0 && count($terms)>0){
            foreach ($parents as $v){
                foreach ($terms as $v2){
                    if($v2->term_id === $v){
                        $path .= $v2->slug.'/';
                    }
                }
            }
            if($path){
                $path .= $currcat->slug;
                $separator = $path?'_':'';
                $str = str_replace( '%cat%/' , $path.'/'.$separator , $post_link );
                return $str;
            }
        }
        elseif(!count($parents)>0 && $currcat){
            $path = $currcat->slug;
            $separator = $path?'_':'';
            $str = str_replace( '%cat%/' , $path.'/'.$separator , $post_link );
            return $str;
        }
        else{
            $separator = $post_link?'_':'';
            $str = str_replace( '%cat%/' , 'uncategorized/'.$separator , $post_link );
            return $str;
        }
    }
    return $post_link;
}

//add hierarchy to custom post
function post_path($wp_query, $wp, $adminka, $thisCustomPost){
    $currentRequest = strpos($wp->request, '/')?substr($wp->request, 0, strpos($wp->request, '/')):$wp->request;
    if($adminka != $currentRequest && $thisCustomPost == $currentRequest){
        $type = null;
        if(isset($wp_query->query['name'])){
            //post
            $type = 'post';
        }
        elseif(count($wp_query->posts)>0 && !isset($wp_query->queried_object)) {
            //category with posts
            $type = 'category';
        }
        elseif(count($wp_query->posts)<=0 && isset($wp_query->queried_object)) {
            //category without posts
            $type = 'category';
        }
        else{
            force_404();
        }
        if($wp->request != $thisCustomPost){
            if(!checkUrlPath($wp->request, 'category_'.$thisCustomPost, $wp_query, $type)){
                force_404();
            }
        }
        else{
            //force_404();
        }
    }
}


//set correct path to post including categories hierarchy
add_action( 'wp', 'post_loading' );
function post_loading(){
    global $wp_query;
    global $wp;
    global $thisCustomPosts;
    if(isset($thisCustomPosts[get_post_type()])){
        $adminka = 'wp-admin';
        $thisCustomPost = get_post_type();
        post_path($wp_query, $wp, $adminka, $thisCustomPost);
    }
}

//return modified link with categories hierarchy
function get_post_link( $post_link, $post, $leavename, $sample ){
    global $thisCustomPosts;
    $post_type_category = null;
    if(isset($thisCustomPosts[get_post_type()])){
        $post_type_category = 'category_'.get_post_type();
    }
    elseif($post->post_type && isset($thisCustomPosts[$post->post_type])){
       $post_type_category = 'category_'.$post->post_type;
    }
    if($post_type_category){
       return post_link( $post_link, $post, $leavename, $sample, $post_type_category);
    }
    return $post_link;
}
add_filter( 'post_type_link', 'get_post_link', 10, 4 );

function getCoordinates($address){
    $address = str_replace(" ", "+", trim(esc_attr($address)));
    $key = get_option('field_mapKey');
    $key = $key?$key:'AIzaSyDchUM7N2SWlrtxSzQ5GwvSQzOUl3vt1ZM';
    $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=$address&key=".$key;
    $response = file_get_contents($url);
    $json = json_decode($response,TRUE);
    if(isset($json['results'][0]['geometry']['location']['lat']) && isset($json['results'][0]['geometry']['location']['lng'])){
        return ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);
    }
    return '';
}

function combineUrlQueries(Array $query=[]){
    $res = '';
    if(count($query)>0){
        foreach ($query as $k => $v){
            if($res){
                $res .= '&';
            }
            $res .= $k.'='.$v;
        }
    }
    return $res;
}
//returns content with offset and variables for pagination. takes: posts array, items amount to display per page, pagination items count
function getPaged($posts_array, $perPage=6, $maxPages=5){
    $res = [];
    if(!is_array($posts_array) || count($posts_array)<1){
        $res['posts_array'] = [];
        return $res;
    }
    $res['perPage'] = $perPage;
    $res['maxPages'] = $maxPages;
    $res['posts_array'] = $posts_array;
    //$res['page'] = isset($_GET['page'])&&$_GET['page']>1?(int)$_GET['page']:1;
    $res['page'] = get_query_var('page')&&get_query_var('page')>1?(int)get_query_var('page'):1;
    $res['offset'] = $res['perPage']*($res['page']-1);
    $res['pagesCount'] = count($res['posts_array']);
    $res['pagesCount'] = ceil($res['pagesCount']/$res['perPage']);
    $res['maxPages'] = $res['maxPages']>$res['pagesCount']?$res['pagesCount']:$res['maxPages'];
    $res['posts_array'] = array_slice($res['posts_array'], $res['offset'], $res['perPage']);
    $res['currUrl'] = "//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $res['currUrlArr'] = parse_url($res['currUrl']);
    $res['$urlQuery'] = @$res['currUrlArr']['query'];
    $res['urlQueryArr'] = [];
    $res['urlQueryStr'] = '';
    if($res['$urlQuery']){
        $_urlQueryArr = explode('&', $res['$urlQuery']);
        $_urlQueryArr = array_filter($_urlQueryArr);
        if(count($_urlQueryArr)>0){
            foreach ($_urlQueryArr as $v){
                $_arr = explode('=', $v);
                $res['urlQueryArr'][$_arr[0]] = $_arr[1];
            }
        }
    }
    if(isset($res['urlQueryArr']['page'])){
        unset($res['urlQueryArr']['page']);
        $res['urlQueryArr'] = array_filter($res['urlQueryArr']);
    }
    $res['urlQueryStr'] = combineUrlQueries($res['urlQueryArr']);
    $res['finUrl'] = @$res['urlQueryArr']['path'].($res['urlQueryStr']?'?'.$res['urlQueryStr'].'&':'?');
    return $res;
}
//returns pagination. takes: getPaged function result
function getPaganation($res){
    if(!$res){
        return;
    }
    ?>
    <!--begin pagination-->
        <?php if(count($res['posts_array'])>0 && $res['pagesCount']>1): ?>
        <div class="col-xs-12">
            <div class="pagination-wrap">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <?php
                        $prev = ($res['page']-1)>1?($res['page']-1):1;
                        echo '<li><a href="'.$res['finUrl'].'page='.$prev.'" aria-label="Previous"><span aria-hidden="true"><</span></a></li>';
                        if($res['page']<=floor($res['maxPages']/2)){
                            $pOffset = 1;
                        }
                        else{
                            $pOffset = $res['page']-(floor($res['maxPages']/2));
                        }
                        if(($pOffset+$res['maxPages']-1)>$res['pagesCount']){
                            $pOffset = $res['pagesCount']-$res['maxPages']+1;
                        }
                        for($i=$pOffset; $i<($res['maxPages']+$pOffset); $i++){
                            $active = '';
                            if((int)$res['page'] == (int)$i){
                                $active = ' class="active"';
                            }
                            echo '<li'.$active.'><a href="'.$res['finUrl'].'page='.$i.'">'.$i.'</a></li>';
                        }
                        $next = ($res['page']+1)>$res['pagesCount']?$res['pagesCount']:($res['page']+1);
                        echo '<li><a href="'.$res['finUrl'].'page='.$next.'" aria-label="Next"><span aria-hidden="true">></span></a></li>';
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
    <?php endif; ?>
    <!--end pagination-->
    <?php
}

/**
 * Add support for the QuickTime (.mov) video format.
 */
add_filter( 'wp_video_extensions',
    function( $exts ) {
        $exts[] = 'mov';
        return $exts;
    }
);

//removing sidebars from front end if haven't widgets inside
add_filter( 'dynamic_sidebar_has_widgets', 'filter_function_name_8921', 10, 2 );
function filter_function_name_8921( $did_one, $index ){
    // filter...
    return $did_one;
}

//removing default widgets
add_action( 'widgets_init', 'cwwp_unregister_default_widgets' );
/**
 * Unregisters the default widgets in WordPress. Tested with 3.5-alpha.
 * Simply uncomment specific widgets to unregister them.
 *
 * @since 1.0.0
 */
function cwwp_unregister_default_widgets() {
    unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_Categories' );
    //unregister_widget( 'WP_Nav_Menu_Widget' );
    unregister_widget( 'WP_Widget_Links' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Pages' );
    unregister_widget( 'WP_Widget_Recent_Comments' );
    unregister_widget( 'WP_Widget_Recent_Posts' );
    unregister_widget( 'WP_Widget_RSS' );
    unregister_widget( 'WP_Widget_Search' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
    //unregister_widget( 'WP_Widget_Text' );
    unregister_widget( 'WP_Widget_Media_Gallery' );
    //unregister_widget( 'WP_Widget_Media_Image' );
    //unregister_widget( 'WP_Widget_Media_Video' );
    //unregister_widget( 'WP_Widget_Media_Audio' );
    //unregister_widget( 'WP_Widget_Custom_HTML' );
}

//remove items from admin main menu
function remove_menus(){
    //remove_menu_page( 'index.php' );                  //Dashboard
    //remove_menu_page( 'jetpack' );                    //Jetpack*
    //remove_menu_page( 'edit.php' );                   //Posts
    //remove_menu_page( 'upload.php' );                 //Media
    //remove_menu_page( 'edit.php?post_type=page' );    //Pages
    remove_menu_page( 'edit-comments.php' );          //Comments
    //remove_menu_page( 'themes.php' );                 //Appearance
    //remove_menu_page( 'plugins.php' );                //Plugins
    //remove_menu_page( 'users.php' );                  //Users
    //remove_menu_page( 'tools.php' );                  //Tools
    //remove_menu_page( 'options-general.php' );        //Settings
}
add_action( 'admin_menu', 'remove_menus' );

//cut string by passed letters amount. can be added ending for cut strings
function cutString($string, $count=20, $end='...'){
    $result = '';
    if(is_string($string) && mb_strlen(trim($string))>0){
        $contentCount = mb_strlen($string);
        $result = mb_substr($string, 0, $count);
        if($contentCount>mb_strlen($result)){
            $result .= $end;
        }
    }
    return $result;
}

function widget($atts) {
    global $wp_widget_factory;
    extract(shortcode_atts(array(
        'widget_class' => FALSE,
        'widget_num' => FALSE
    ), $atts));
    $className = esc_html($widget_class);
    if($className){
    	$widgetInstance = getWidgetFieldByClassName($className);
    	$className = $className;
    	if(is_array($widgetInstance)){
			$widget_num = (int)$widget_num;
			$widgetInstanceName = array_keys($widgetInstance);
		    $widgetInstanceName = $widget_num>=0 && isset($widgetInstanceName[$widget_num])?$widgetInstanceName[$widget_num]:null;
		    if($widgetInstanceName){
			    ob_start();
			    the_widget($className, $widgetInstance[$widgetInstanceName]);
			    $output = ob_get_contents();
			    ob_end_clean();
			    return $output;
		    }
    	}
    }
}
add_shortcode('widget','widget'); 

/*add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );
function sdt_remove_ver_css_js( $src, $handle ) 
{
    $handles_with_version = [ 'style' ]; // <-- Adjust to your needs!
    if ( strpos( $src, 'ver=' ) && ! in_array( $handle, $handles_with_version, true ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}*/