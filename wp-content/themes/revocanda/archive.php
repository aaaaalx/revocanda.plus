<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage revocanda
 * @since 1.0
 * @version 1.0
 */
$post_format = @get_queried_object()->has_archive;
get_header();
get_template_part( 'taxonomy', 'category_'.$post_format );
get_footer();