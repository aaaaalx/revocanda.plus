<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header();

global $isMobile;

//get page header background
$background = get_post_meta($post->ID, '_events_settings_bg_value_key', true);
if($background){
    if($isMobile){
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'mobile-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
    else{
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'desktop-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
}
else{
    if($isMobile){
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg-mobile.png);"';
    }
    else{
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg.png);"';
    }
}

//set up the address
$citiesAndCountries = getCities(true);
$address = get_post_meta($post->ID, '_events_address_value_key', true);
$country = get_post_meta($post->ID, '_events_country_value_key', true);
$country = isset($citiesAndCountries['countries'][$country])?$citiesAndCountries['countries'][$country]['country']:'';
$city = get_post_meta($post->ID, '_events_city_value_key', true);
$city = isset($citiesAndCountries['cities'][$city])?$citiesAndCountries['cities'][$city]['city']:'';
$full_address = '';
if($country){
    $full_address = $country;
}
if($city){
    $full_address .= $full_address?', ':'';
    $full_address .= $city;
}
if($address){
    $full_address .= $full_address?', ':'';
    $full_address .= $address;
}

//get data & time
$date = get_post_meta($post->ID, '_events_date_value_key', true);
$time = get_post_meta($post->ID, '_events_time_value_key', true);

//get short description
$short_desc = get_post_meta($post->ID, '_events_short_desc_value_key', true);

//get setting: is event past
$past_event = get_post_meta($post->ID, '_events_settings_past_event_value_key', true);

//set up video
$isVideoActive = get_post_meta($post->ID, '_events_video_active_value_key', true);
$video_type = get_post_meta($post->ID, '_events_video_type_value_key', true);
$video = '';
if($video_type == 'video'){
    $video = get_post_meta($post->ID, '_events_video_value_key', true);
}
else{
    $video = get_post_meta($post->ID, '_events_video_link_value_key', true);
}
$video_title = $video?get_post_meta($post->ID, '_events_video_title_value_key', true):'';
$video_desc = $video?get_post_meta($post->ID, '_events_video_desc_value_key', true):'';

//get page image
$image = get_the_post_thumbnail_url($post->ID, [480,363]);
?>
<!--begin section-intro-->
<section class="section-poster-intro"<?= $background; ?>>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-box">
                    <div class="section-head-box">
							<span class="section-head-back">
								<span>
									Мероприятия и тренинги
								</span>
							</span>
                        <p><?= get_the_title(); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-intro-->

<!--begin section-item-page-->
<section class="events-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a class="back-button" href="/events" hreflang="ru"><span>Все мероприятия</span></a>
            </div>
            <div class="col-xs-12">
                <div class="event-item-wrap">
                    <div class="row">
                        <?php
                        if(!$image){
                            $image = get_stylesheet_directory_uri().'/img/No-photo_480x363.jpg';
                        }
                        else{
                            $gId = pippin_get_file_id($image);
                            $url = get_thumbnails_by_id($gId, 'block-medium-image');
                            $image = $url?$url:$image;
                        }
                        ?>
                        <div class="col-md-5 col-xs-12">
                            <img src="<?= $image; ?>" alt="">
                        </div>
                        <div class="col-md-7 col-xs-12 relative">
                            <div class="event-caption-box">
                                <h1><?= get_the_title(); ?></h1>
                                <?= $short_desc; ?>
                                <div class="info-box">
                                    <div class="row">
                                        <?php if($full_address): ?>
                                        <div class="col-md-8 col-xs-12">
                                            <div class="address-box">
													<span><?= $full_address; ?></span>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <?php if($date || $time): ?>
                                        <div class="col-md-4 col-xs-12">
                                            <?php if($date): ?>
                                            <div class="date-box">
                                                <span><?= $date; ?></span>
                                            </div>
                                            <?php endif; ?>
                                            <?php if($time): ?>
                                            <div class="time-box">
                                                <span>Начало в <?= $time; ?></span>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php if($past_event): ?>
                                <div class="past-event-label">
                                    <span>событие уже состоялось</span>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="event-program-box">

                                <?php
                                // Start the loop.
                                while ( have_posts() ) : the_post();
                                    ?>
                                    <?php do_shortcode(the_content()); ?>
                                    <?php
                                    // End the loop.
                                endwhile;
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($isVideoActive && $video): ?>
            <div class="col-xs-12">
                <div class="event-program-wrap">
                    <div class="row">
                        <div class="col-md-5 col-xs-12">
                            <div class="video-box">
                                <div class="video-wrap">
                                    <?php
                                    if($video_type == 'embed'){
                                        $videoHtml = wp_oembed_get($video, [
                                            'width'=>480,
                                            'height'=>353
                                        ]);
                                    }
                                    else{
                                        $videoHtml = wp_video_shortcode([
                                            'src'=>$video,
                                            'width'=>480,
                                            'height'=>350
                                        ]);
                                    }
                                    ?>
                                    <?= $videoHtml; ?>
                                    <div class="overlay-box"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-xs-12">
                            <div class="event-program-box">
                                <?php if($video_title): ?>
                                <h2><?= $video_title; ?></h2>
                                <?php endif; ?>
                                <?php if($video_desc): ?>
                                <p><?= $video_desc; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<!--end section-item-page-->

<?php
get_footer();
?>