<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

//get all post type categories
$cats = get_terms( 'category_prices', array(
    'hide_empty' => false,
) );

//get current category
$cat = get_queried_object()?get_queried_object():false;
$args = [
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'prices',
    'genre'            => 'category_prices',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
];
if(isset($cat->term_id)){
    $args['tax_query'] = [
        [
            'taxonomy' => 'category_prices',
            'field' => 'term_id',
            'terms' => $cat->term_id,
            'include_children' => false
        ]
    ];
}
$posts_array = get_posts( $args );
$res = getPaged($posts_array,8);
$posts_array = $res['posts_array'];

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>

    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>Стоимость</span>
							</span>
                            <p>Стоимость</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-price-page-->
    <section class="section-price-page">
        <div class="container">
            <div class="row">

                <?php if(count($cats)>0): ?>
                <!--begin page-nav-wrap-->
                <div class="col-xs-12">
                    <div class="page-nav-wrap">
                        <ul class="slider-page-nav">
                            <?php foreach($cats as $c): ?>
                            <li class="item<?= $c->term_id === $cat->term_id?' active':''; ?>"><a href="<?= esc_url(get_category_link($c->term_id)); ?>"><?= $c->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <!--end page-nav-wrap-->
                <?php endif; ?>

                <?php if(count($posts_array)>0): ?>
                <!--begin price-items-->
                <div class="col-xs-12">
                    <div class="row">

                        <?php foreach($posts_array as $p): ?>

                        <?php
                            $price_str = get_post_field('_prices_price_value_key', $p->ID);
                            $persons_str = get_post_field('_prices_persons_value_key', $p->ID);
                        ?>
                        <div class="col-lg-3 col-sm-6 col-xs-12 relative">
                            <div class="inner-wrap">
                                <div class="price-title">
                                    <h3><?= $p->post_title; ?></h3>
                                </div>
                                <div class="price-info">
                                    <?php if($price_str): ?>
                                    <div class="price"><span><?= $price_str ?></span></div>
                                    <?php endif; ?>
                                    <?php if($persons_str): ?>
                                    <div class="counter"><span><?= $persons_str; ?></span></div>
                                    <?php endif; ?>
                                </div>
                                <div class="price-description">
                                    <?= $p->post_content; ?>
                                </div>
                                <button onclick="ga('send', 'event', 'button5', 'prices')" class="show-more"></button>
                                <button onclick="ga('send', 'event', 'button5', 'prices')" class="more-toggle"><span>Связаться с нами</span></button>
                            </div>
                        </div>
                        <?php endforeach; ?>

                    </div>
                </div>
                <!--end price-items-->
                <?php else: ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="list-item-box-2">
                            Нет материалов для отображения
                        </div>
                    </div>
                <?php endif; ?>

                <?php getPaganation($res); ?>

            </div>
        </div>
    </section>
    <!--end section-price-page-->



<?php get_footer(); ?>