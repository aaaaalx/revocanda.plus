<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header();

global $isMobile;

//get page header background
$background = get_post_meta($post->ID, '_media_settings_bg_value_key', true);
if($background){
    if($isMobile){
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'mobile-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
    else{
        $gId = pippin_get_file_id($background);
        $image = get_thumbnails_by_id($gId, 'desktop-header');
        $background = $image?$image:$background;
        $background = ' style="background-image:url('.$background.');"';
    }
}
else{
    if($isMobile){
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg-mobile.png);"';
    }
    else{
        $background = ' style="background-image:url('.get_stylesheet_directory_uri().'/img/header-bg.png);"';
    }
}
global $wp_embed;
?>

    <!--begin section-intro-->
    <section class="section-poster-intro"<?= $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>СМИ о нас</span>
							</span>
                            <p><?= get_the_title($post->ID); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-item-page-->
    <section class="section-about-us-page">
        <div class="container">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <a class="back-button" href="/media" hreflang="ru"><span>Все статьи</span></a>
                        <div class="col-xs-12">
                            <?= do_shortcode($wp_embed->run_shortcode($post->post_content)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-item-page-->

<?php
get_footer();
?>