<?php
/**
 *
 * @package WordPress
 * @subpackage revocanda
 * @since 1.0
 * @version 1.0
 */

get_header();

global $isMobile;

$background = '/img/header-bg.png';
if($isMobile){
    $background = '/img/header-bg-mobile.png';
}
?>
    <!--begin section-intro-->
    <section class="section-poster-intro" style="background-image: url(<?php echo get_stylesheet_directory_uri().$background; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-box">
                        <div class="section-head-box">
							<span class="section-head-back">
								<span>
									<?= $title; ?>
								</span>
							</span>
                            <p><?= $title; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-intro-->

    <!--begin section-page-->
    <section class="section-about-us-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>
                        Заголовок Сообщения
                    </h1>
                    <p>
                        Текст сообщения
                    </p>
                    <a href="." class="main-button blue-button" hreflang="ru"><span>на главную</span></a>
                </div>
            </div>
        </div>
    </section>
    <!--end section-page-->
<?php get_footer();
